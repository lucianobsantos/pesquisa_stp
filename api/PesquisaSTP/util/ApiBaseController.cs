﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MiniFrameWork.Dados;

using Microsoft.AspNetCore.Authorization;
using MiniFrameWork.Camadas;

namespace PesquisaSTP.util
{

    [Authorize("Bearer")]
    public class ApiBaseController : Controller


    {


        protected IDatabaseMF banco = new MSSQLDatabaseFactory();
        protected IConfiguration _configuration;

        public ApiBaseController(IConfiguration configuration)
        {
            _configuration = configuration;
            banco.CreateDatabaseDynamic(_configuration.GetConnectionString("bancosSQL"), "System.Data.SqlClient");
        }

        protected override void Dispose(bool disposing)
        {
            if (banco.BancoDados != null)
            {
                banco.BancoDados.Close();
            }
            base.Dispose(disposing);
        }


        protected bool isTransacao = false;

        virtual protected void setBanco()
        {
            banco.CreateDatabase("SISTE", "Oracle.DataAccess.Client");
        }


        virtual protected void setBanco(String strconn)
        {
            banco.CreateDatabaseDynamic(strconn, "Oracle.DataAccess.Client");
        }


        public void CloseDatabase()
        {
            if (banco != null)
            {
                banco.CloseDatabase();
                banco = null;
            }

        }





    }
}
