using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Cors;
using MiniFrameWork.Camadas;
using MiniFrameWork.Util;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using MiniFrameWork.Dados;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading.Tasks;
//using SISTE.Util;
//using SISTE.Web.Models;


namespace PesquisaSTP.util
{


    // [EnableCors(headers: "*", methods: "*")]
    public abstract class ApiBASEEntityController<E, N, D> : ApiBaseController
        where E : EntityBase, new()
        where D : DBase<E>, new()
        where N : NBase<E, D>, new()
    {

        protected const string MENSAGEM_PADRAO_SUCESSO = "Operação efetuda com Sucesso !";
        protected const string MENSAGEM_PADRAO_POST = "Registro Cadastrado com Sucesso !";
        protected const string MENSAGEM_PADRAO_PUT = "Registro Alterado com Sucesso !";
        protected const string MENSAGEM_PADRAO_DELETE = "Registro Ativado/Inativado com Sucesso !";

        public N _nNegocio = new N();
        protected string id = "";
        protected string usuarioField = "usua_nr_edicao";

        virtual protected E addPaginacao(E model, IDictionary<String, String> dictionary)
        {
            if (model.CamposAuxiliares == null)
                model.CamposAuxiliares = new Dictionary<string, object>();

            model.CamposAuxiliares.Add("PAG_C", Convert.ToInt64(dictionary["pag_c"]));
            model.CamposAuxiliares.Add("QTD_I", Convert.ToInt64(dictionary["qtd_i"]));

            return model;
        }

        virtual protected void OnPosCommit(string router, E model)
        {


        }

        virtual protected E OnTrataModelAfterRoute(string router, E model, IDictionary<String, String> dictionary)
        {

            return model;
        }
        public ApiBASEEntityController(IConfiguration configuration) : base(configuration)
        {
            _nNegocio.databasemf = this.banco;
        }

        #region
        virtual protected string trataMensagemError(Exception ex, string Tag)
        {
            return ex.Message;
        }
        #endregion


        virtual public IActionResult Inserir(dynamic modelParams)
        {
            E model = null;
            try
            {

                model = JsonConvert.DeserializeObject<E>(JsonConvert.SerializeObject(modelParams, Formatting.Indented));
                Int64? id = _nNegocio.Insert(model);
                Reflector.SetProperty(model, this.id, (Int32?)id);

                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_POST, dados = model });
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("ORA-00001: restrição exclusiva"))
                {

                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });

                }
                else
                {
                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = ex.Message, dados = null });
                }

            }
        }


        virtual public async Task<IActionResult> InserirAsync(dynamic modelParams)
        {
            E model = null;
            try
            {

                model = JsonConvert.DeserializeObject<E>(JsonConvert.SerializeObject(modelParams, Formatting.Indented));

                Int64? id = new Int64?();
                await Task.Run(() =>
                {
                    id = _nNegocio.Insert(model);
                    Reflector.SetProperty(model, this.id, (Int32?)id);
                });

                await Task.Run(() =>
               {
                   OnPosCommit("INSERIR", model);
               });





                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_POST, dados = model });
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("ORA-00001: restrição exclusiva"))
                {

                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });

                }
                else
                {
                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = ex.Message, dados = null });
                }

            }
        }

        virtual public IActionResult Alterar(int id, dynamic modelParams)
        {
            E model = null;
            try
            {
                model = JsonConvert.DeserializeObject<E>(JsonConvert.SerializeObject(modelParams, Formatting.Indented));
                Reflector.SetProperty(model, this.id, id);
                _nNegocio.Update(model);

                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_PUT, dados = model });

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("ORA-00001: restrição exclusiva"))
                {
                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });

                }
                else
                {
                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = ex.Message, dados = null });
                }

            }
        }

        virtual public async Task<IActionResult> AlterarAsync(int id, dynamic modelParams)
        {
            E model = null;
            try
            {
                model = JsonConvert.DeserializeObject<E>(JsonConvert.SerializeObject(modelParams, Formatting.Indented));
                Reflector.SetProperty(model, this.id, id);
                _nNegocio.Update(model);

                await Task.Run(() =>
                {
                    _nNegocio.Update(model);
                });

                await Task.Run(() =>
               {
                   OnPosCommit("ALTERAR", model);
               });


                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_PUT, dados = model });

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("ORA-00001: restrição exclusiva"))
                {
                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });

                }
                else
                {
                    return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = ex.Message, dados = null });
                }

            }
        }



        virtual public IActionResult GetFiltro(string modelParams)
        {
            try
            {
                IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(modelParams).ToDictionary();

                E model = JsonConvert.DeserializeObject<E>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
                model = OnTrataModelAfterRoute("GETFILTRO", model, dictionaryRequest);

                List<E> retornoJson = null;
                retornoJson = _nNegocio.GetListaByFilter(model);


                return Ok(new RetornoREST<E>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = retornoJson });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }


        virtual public async Task<IActionResult> GetFiltroAsync(string modelParams)
        {
            try
            {
                IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(modelParams).ToDictionary();

                E model = JsonConvert.DeserializeObject<E>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
                model = OnTrataModelAfterRoute("GETFILTROASYNC", model, dictionaryRequest);
                model = addPaginacao(model, dictionaryRequest);
                List<E> retornoJson = null;
                Int64 total_registro = 0;
                await Task.Run(() =>
                {
                    retornoJson = _nNegocio.GetListaByFilter(model);
                    if (retornoJson.Count > 0)
                    {
                        total_registro = Convert.ToInt64(retornoJson[0].CamposAuxiliares["qtdtotal"]);
                    }
                });

                return Ok(new RetornoREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_SUCESSO, dados = retornoJson, total_registro = total_registro });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }

        virtual public IActionResult Get(int id)
        {
            try
            {
                E model = new E();
                Reflector.SetProperty(model, this.id, id);

                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = _nNegocio.GetEntidadeByFilter(model) });


            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }

        virtual public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                E model = new E();
                Reflector.SetProperty(model, this.id, id);
                E retornoJson = null;
                await Task.Run(() =>
                {
                    retornoJson = _nNegocio.GetEntidadeByFilter(model);
                });


                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_SUCESSO, dados = retornoJson });


            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }




        virtual public IActionResult Delete(int id)
        {
            try
            {
                E _model = new E();
                Reflector.SetProperty(_model, this.id, id);
                _model = _nNegocio.GetEntidadeByFilter(_model); // temporario
                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_DELETE, dados = _model });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = ex.Message, dados = null });

            }
        }

        virtual public IActionResult Delete(int id, int idUsuario)
        {
            try
            {
                E _model = new E();
                Reflector.SetProperty(_model, this.id, id);
                Reflector.SetProperty(_model, this.usuarioField, idUsuario);

                long? retorno = _nNegocio.Delete(_model);

                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_DELETE, dados = _model });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = ex.Message, dados = null });

            }
        }

        virtual public async Task<IActionResult> DeleteAsync(int id, int idUsuario)
        {
            try
            {
                E _model = new E();
                Reflector.SetProperty(_model, this.id, id);
                Reflector.SetProperty(_model, this.usuarioField, idUsuario);

                long? retorno = null;
                await Task.Run(() =>
                {
                    retorno = _nNegocio.Delete(_model);
                });

                return Ok(new RetornoSingleREST<E>() { error = 0, mensagem = MENSAGEM_PADRAO_DELETE, dados = _model });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<E>() { error = 1, mensagem = ex.Message, dados = null });

            }
        }

    }
}