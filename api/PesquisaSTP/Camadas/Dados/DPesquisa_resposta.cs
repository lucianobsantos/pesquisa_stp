using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using PesquisaSTP.Camadas.Entidades;


namespace PesquisaSTP.Camadas.Dados
{
    public class DPesquisa_resposta : DBase<Pesquisa_resposta>
    {

        public DPesquisa_resposta() : base() { }
        public DPesquisa_resposta(IDatabaseMF _databasemf) : base(_databasemf) { }

    }
}
