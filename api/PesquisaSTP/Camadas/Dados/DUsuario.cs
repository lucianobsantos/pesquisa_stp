using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using PesquisaSTP.Camadas.Entidades;


namespace PesquisaSTP.Camadas.Dados
{
    public class DUsuario : DBase<Usuario>
    {

        public DUsuario() : base() { }
        public DUsuario(IDatabaseMF _databasemf) : base(_databasemf) { }

    }
}
