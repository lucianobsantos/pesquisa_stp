using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using PesquisaSTP.Camadas.Entidades;


namespace PesquisaSTP.Camadas.Dados
{
    public class DPergunta : DBase<Pergunta>
    {

        public DPergunta() : base() { }
        public DPergunta(IDatabaseMF _databasemf) : base(_databasemf) { }

    }
}
