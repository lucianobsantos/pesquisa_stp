using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using PesquisaSTP.Camadas.Entidades;


namespace PesquisaSTP.Camadas.Dados
{
    public class DResposta : DBase<Resposta>
    {

        public DResposta() : base() { }
        public DResposta(IDatabaseMF _databasemf) : base(_databasemf) { }

    }
}
