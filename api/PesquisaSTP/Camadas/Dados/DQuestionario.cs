using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using PesquisaSTP.Camadas.Entidades;


namespace PesquisaSTP.Camadas.Dados
{
    public class DQuestionario : DBase<Questionario>
    {

        public DQuestionario() : base() { }
        public DQuestionario(IDatabaseMF _databasemf) : base(_databasemf) { }

    }
}
