using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Dados;


namespace PesquisaSTP.Camadas.Negocio
{

    public class NPesquisa_resposta : NBase<Pesquisa_resposta, DPesquisa_resposta>
    {

        public NPesquisa_resposta() : base() { }
        public NPesquisa_resposta(IDatabaseMF _databasemf) : base(_databasemf) { }

        public List<Pesquisa_resposta> getDadosGeral(Pesquisa_resposta entidade)
        {

            DPesquisa_resposta _objeto = new DPesquisa_resposta();
            _objeto.databasemf = this.databasemf;
            QueryMF queryMf = _objeto.getQueryMF<Pesquisa_resposta>("GET_RESUMO", (IEntityBase)entidade);
            var retorno = _objeto.GetListaByFilter<Pesquisa_resposta>(queryMf);

            return retorno;
        }

    }
}
