using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Dados;


namespace PesquisaSTP.Camadas.Negocio
{

    public class NResposta : NBase<Resposta, DResposta>
    {

        public NResposta() : base() { }
        public NResposta(IDatabaseMF _databasemf) : base(_databasemf) { }

    }
}
