using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Dados;


namespace PesquisaSTP.Camadas.Negocio
{

    public class NUsuario : NBase<Usuario, DUsuario>
    {

        public NUsuario() : base() { }
        public NUsuario(IDatabaseMF _databasemf) : base(_databasemf) { }


        public Usuario getValidaUsuario(Usuario entidade)
        {
            var dt1 = DateTime.Now;

            DUsuario _objeto = new DUsuario();
            _objeto.databasemf = this.databasemf;
            QueryMF queryMf = _objeto.getQueryMF<Usuario>("VALIDA_USUARIO", (IEntityBase)entidade);
            var retorno = _objeto.GetEntidadeByFilter<Usuario>(queryMf);
            var dt2 = DateTime.Now;
            TimeSpan ts = dt1 - dt2;



            return retorno;
        }

      public Usuario esqueciSenha(Usuario entidade)
        {

            DUsuario _objeto = new DUsuario();
            _objeto.databasemf = this.databasemf;
            QueryMF queryMf = _objeto.getQueryMF<Usuario>("GERATOKENSENHA", (IEntityBase)entidade);
            var retorno = _objeto.GetEntidadeByFilter<Usuario>(queryMf);



            return retorno;
        }

        public Usuario validaToken(Usuario entidade)
        {

            DUsuario _objeto = new DUsuario();
            _objeto.databasemf = this.databasemf;
            QueryMF queryMf = _objeto.getQueryMF<Usuario>("VALIDATOKEN", (IEntityBase)entidade);
            var retorno = _objeto.GetEntidadeByFilter<Usuario>(queryMf);



            return retorno;
        }

        public Usuario alteraSenha(Usuario entidade)
        {

            DUsuario _objeto = new DUsuario();
            _objeto.databasemf = this.databasemf;
            QueryMF queryMf = _objeto.getQueryMF<Usuario>("ALTERASENHA", (IEntityBase)entidade);
            var retorno = _objeto.GetEntidadeByFilter<Usuario>(queryMf);



            return retorno;
        }

            public Usuario InsereUsuarioEmpresa(Usuario entidade)
        {

            DUsuario _objeto = new DUsuario();
            _objeto.databasemf = this.databasemf;
            QueryMF queryMf = _objeto.getQueryMF<Usuario>("CADASTRAUSUARIOEMPRESA", (IEntityBase)entidade);
            var retorno = _objeto.GetEntidadeByFilter<Usuario>(queryMf);

            return retorno;
        }

          public Usuario InsereUsuarioConsultor(Usuario entidade)
        {

            DUsuario _objeto = new DUsuario();
            _objeto.databasemf = this.databasemf;
            QueryMF queryMf = _objeto.getQueryMF<Usuario>("CADASTRAUSUARIOCONSULTOR", (IEntityBase)entidade);
            var retorno = _objeto.GetEntidadeByFilter<Usuario>(queryMf);

            return retorno;
        }

    }
}
