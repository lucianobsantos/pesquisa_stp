using MiniFrameWork.Camadas;
using MiniFrameWork.Dados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Dados;


namespace PesquisaSTP.Camadas.Negocio
{

    public class NQuestionario : NBase<Questionario, DQuestionario>
    {

        public NQuestionario() : base() { }
        public NQuestionario(IDatabaseMF _databasemf) : base(_databasemf) { }

    }
}
