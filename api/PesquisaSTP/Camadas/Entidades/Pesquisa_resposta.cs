using MiniFrameWork.Camadas;
using MiniFrameWork.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PesquisaSTP.Camadas.Entidades
{
    [AtributoTabela(Nome = "Pesquisa_resposta", Esquema = "" , TipoChave = eTipoChave.Automatica)]
    public class Pesquisa_resposta : EntityBase, IEntityBase
    {
       [AtributoCampo(Nome = "pere_nr_sequencia"  , TipoDado = DbType.Int32 , TipoChave = eTipoChave.Automatica   )]
       public Int32? Pere_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "ques_nr_sequencia"  , TipoDado = DbType.Int32   )]
       public Int32? Ques_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "resp_nr_sequencia"  , TipoDado = DbType.Int32   )]
       public Int32? Resp_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "pere_dt_resposta"  , TipoDado = DbType.DateTime   )]
       public DateTime? Pere_dt_resposta { get; set; }

       [AtributoCampo(Nome = "situ_tx_situacao"  , TipoDado = DbType.String   )]
       public String Situ_tx_situacao { get; set; }

       [AtributoCampo(Nome = "usua_nr_cadastro"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_cadastro { get; set; }

       [AtributoCampo(Nome = "usua_nr_edicao"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_edicao { get; set; }

       [AtributoCampo(Nome = "data_dt_cadastro"  , TipoDado = DbType.DateTime   )]
       public DateTime? Data_dt_cadastro { get; set; }

       [AtributoCampo(Nome = "data_dt_edicao"  , TipoDado = DbType.DateTime   )]
       public DateTime? Data_dt_edicao { get; set; }

       [AtributoCampo(Nome = "pere_tx_descricao"  , TipoDado = DbType.String   )]
       public String Pere_tx_descricao { get; set; }

       [AtributoCampo(Nome = "pere_tx_identificador"  , TipoDado = DbType.String   )]
       public String Pere_tx_identificador { get; set; }



  }
}
