using MiniFrameWork.Camadas;
using MiniFrameWork.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PesquisaSTP.Camadas.Entidades
{
    [AtributoTabela(Nome = "Resposta", Esquema = "" , TipoChave = eTipoChave.Automatica)]
    public class Resposta : EntityBase, IEntityBase
    {
       [AtributoCampo(Nome = "resp_nr_sequencia"  , TipoDado = DbType.Int32 , TipoChave = eTipoChave.Automatica   )]
       public Int32? Resp_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "perg_nr_sequencia"  , TipoDado = DbType.Int32   )]
       public Int32? Perg_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "resp_tx_descricao"  , TipoDado = DbType.String   )]
       public String Resp_tx_descricao { get; set; }

       [AtributoCampo(Nome = "resp_nr_ordem"  , TipoDado = DbType.Int32   )]
       public Int32? Resp_nr_ordem { get; set; }

       [AtributoCampo(Nome = "situ_tx_situacao"  , TipoDado = DbType.String   )]
       public String Situ_tx_situacao { get; set; }

       [AtributoCampo(Nome = "usua_nr_cadastro"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_cadastro { get; set; }

       [AtributoCampo(Nome = "usua_nr_edicao"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_edicao { get; set; }

       [AtributoCampo(Nome = "data_dt_cadastro"  , TipoDado = DbType.DateTime   )]
       public DateTime? Data_dt_cadastro { get; set; }

       [AtributoCampo(Nome = "data_dt_edicao"  , TipoDado = DbType.DateTime   )]
       public DateTime? Data_dt_edicao { get; set; }

       [AtributoCampo(Nome = "resp_nr_peso"  , TipoDado = DbType.Decimal   )]
       public Decimal? Resp_nr_peso { get; set; }

       [AtributoCampo(Nome = "resp_tx_imagem"  , TipoDado = DbType.String   )]
       public String Resp_tx_imagem { get; set; }

       [AtributoCampo(Nome = "resp_tx_valor"  , TipoDado = DbType.String   )]
       public String Resp_tx_valor { get; set; }



  }
}
