using MiniFrameWork.Camadas;
using MiniFrameWork.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PesquisaSTP.Camadas.Entidades
{
    [AtributoTabela(Nome = "Usuario", Esquema = "" , TipoChave = eTipoChave.Automatica)]
    public class Usuario : EntityBase, IEntityBase
    {
       [AtributoCampo(Nome = "usua_nr_sequencia"  , TipoDado = DbType.Int32 , TipoChave = eTipoChave.Automatica   )]
       public Int32? Usua_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "usua_tx_nome"  , TipoDado = DbType.String   )]
       public String Usua_tx_nome { get; set; }

       [AtributoCampo(Nome = "usua_tx_apelido"  , TipoDado = DbType.String   )]
       public String Usua_tx_apelido { get; set; }

       [AtributoCampo(Nome = "usua_tx_email"  , TipoDado = DbType.String   )]
       public String Usua_tx_email { get; set; }

       [AtributoCampo(Nome = "situ_tx_situacao"  , TipoDado = DbType.String   )]
       public String situ_tx_situacao { get; set; }

       [AtributoCampo(Nome = "usua_nr_cadastro"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_cadastro { get; set; }

       [AtributoCampo(Nome = "usua_nr_edicao"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_edicao { get; set; }

       [AtributoCampo(Nome = "usua_dt_cadastro"  , TipoDado = DbType.DateTime   )]
       public DateTime? Usua_dt_cadastro { get; set; }

       [AtributoCampo(Nome = "usua_dt_edicao"  , TipoDado = DbType.DateTime   )]
       public DateTime? Usua_dt_edicao { get; set; }

       [AtributoCampo(Nome = "usua_tx_senha"  , TipoDado = DbType.Object   )]
       public Object Usua_tx_senha { get; set; }



  }
}
