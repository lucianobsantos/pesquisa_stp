using MiniFrameWork.Camadas;
using MiniFrameWork.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PesquisaSTP.Camadas.Entidades
{
    [AtributoTabela(Nome = "Pergunta", Esquema = "" , TipoChave = eTipoChave.Automatica)]
    public class Pergunta : EntityBase, IEntityBase
    {
       [AtributoCampo(Nome = "perg_nr_sequencia"  , TipoDado = DbType.Int32 , TipoChave = eTipoChave.Automatica   )]
       public Int32? Perg_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "ques_nr_sequencia"  , TipoDado = DbType.Int32   )]
       public Int32? Ques_nr_sequencia { get; set; }

       [AtributoCampo(Nome = "perg_tx_descricao"  , TipoDado = DbType.String   )]
       public String Perg_tx_descricao { get; set; }

       [AtributoCampo(Nome = "perg_tx_tipo"  , TipoDado = DbType.String   )]
       public String Perg_tx_tipo { get; set; }

       [AtributoCampo(Nome = "perg_nr_ordem"  , TipoDado = DbType.Int32   )]
       public Int32? Perg_nr_ordem { get; set; }

       [AtributoCampo(Nome = "situ_tx_situacao"  , TipoDado = DbType.String   )]
       public String Situ_tx_situacao { get; set; }

       [AtributoCampo(Nome = "usua_nr_cadastro"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_cadastro { get; set; }

       [AtributoCampo(Nome = "usua_nr_edicao"  , TipoDado = DbType.Int32   )]
       public Int32? Usua_nr_edicao { get; set; }

       [AtributoCampo(Nome = "data_dt_cadastro"  , TipoDado = DbType.DateTime   )]
       public DateTime? Data_dt_cadastro { get; set; }

       [AtributoCampo(Nome = "data_dt_edicao"  , TipoDado = DbType.DateTime   )]
       public DateTime? Data_dt_edicao { get; set; }

       [AtributoCampo(Nome = "perg_nr_val_minimo"  , TipoDado = DbType.Int32   )]
       public Int32? Perg_nr_val_minimo { get; set; }

       [AtributoCampo(Nome = "perg_nr_val_maximo"  , TipoDado = DbType.Int32   )]
       public Int32? Perg_nr_val_maximo { get; set; }

       [AtributoCampo(Nome = "perg_tx_desc_minimo"  , TipoDado = DbType.String   )]
       public String Perg_tx_desc_minimo { get; set; }

       [AtributoCampo(Nome = "perg_tx_desc_maximo"  , TipoDado = DbType.String   )]
       public String Perg_tx_desc_maximo { get; set; }



  }
}
