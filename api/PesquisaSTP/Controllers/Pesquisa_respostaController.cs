using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PesquisaSTP.Camadas.Dados;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Negocio;
using PesquisaSTP.util;
using System.Threading.Tasks;



namespace PesquisaSTP.Controllers
{

    public class Pesquisa_respostaController : ApiBASEEntityController<Pesquisa_resposta, NPesquisa_resposta, DPesquisa_resposta>
    {

        public Pesquisa_respostaController(IConfiguration configuration) : base(configuration)
        {

             this.id = "pere_nr_sequencia";                   
        }

        override protected Pesquisa_resposta OnTrataModelAfterRoute(string router, Pesquisa_resposta model, IDictionary<String, String> dictionary)
        {
            return model;
        }

        protected override string trataMensagemError(Exception ex, string Tag)
        {
            return base.trataMensagemError(ex, Tag);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/pesquisa_resposta")]
        public Task<IActionResult> insert([FromBody] Pesquisa_resposta modelParams) => this.InserirAsync(modelParams);

        [HttpPut]
        [AllowAnonymous]
        [Route("api/pesquisa_resposta/{id}")]
        public Task<IActionResult> alterar(int id, [FromBody] Pesquisa_resposta modelParams) => this.AlterarAsync(id, modelParams);


        [HttpGet]
        [AllowAnonymous]
        [Route("api/pesquisa_resposta")]
        public Task<IActionResult> get() => this.GetFiltroAsync(HttpContext.Request.QueryString.Value);

        [HttpGet]
        [AllowAnonymous]
        [Route("api/pesquisa_resposta/{id}")]
        public Task<IActionResult> get(int id) => this.GetAsync(id);

        [HttpDelete]
        [AllowAnonymous]
        [Route("api/pesquisa_resposta/{id}/{idusuario}")]
        public Task<IActionResult> delete(int id, int idusuario) => this.DeleteAsync(id, idusuario);

    }
}

