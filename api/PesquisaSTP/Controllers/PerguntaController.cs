using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PesquisaSTP.Camadas.Dados;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Negocio;
using PesquisaSTP.util;
using System.Threading.Tasks;



namespace PesquisaSTP.Controllers
{

    public class PerguntaController : ApiBASEEntityController<Pergunta, NPergunta, DPergunta>
    {

        public PerguntaController(IConfiguration configuration) : base(configuration)
        {

             this.id = "perg_nr_sequencia";                 
        }

        override protected Pergunta OnTrataModelAfterRoute(string router, Pergunta model, IDictionary<String, String> dictionary)
        {
            return model;
        }

        protected override string trataMensagemError(Exception ex, string Tag)
        {
            return base.trataMensagemError(ex, Tag);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/pergunta")]
        public Task<IActionResult> insert([FromBody] Pergunta modelParams) => this.InserirAsync(modelParams);


        [HttpPut]
        [AllowAnonymous]
        [Route("api/pergunta/{id}")]
        public Task<IActionResult> alterar(int id, [FromBody] Pergunta modelParams) => this.AlterarAsync(id, modelParams);


        [HttpGet]
        [AllowAnonymous]
        [Route("api/pergunta")]
        public Task<IActionResult> get() => this.GetFiltroAsync(HttpContext.Request.QueryString.Value);

        [HttpGet]
        [AllowAnonymous]
        [Route("api/pergunta/{id}")]
        public Task<IActionResult> get(int id) => this.GetAsync(id);

        [HttpDelete]
        [AllowAnonymous]
        [Route("api/pergunta/{id}/{idusuario}")]
        public Task<IActionResult> delete(int id, int idusuario) => this.DeleteAsync(id, idusuario);

    }
}

