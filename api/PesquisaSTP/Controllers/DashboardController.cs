using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PesquisaSTP.Camadas.Dados;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Negocio;
using PesquisaSTP.util;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;


namespace PesquisaSTP.Controllers
{

    public class DashboardController : ApiBASEEntityController<Pesquisa_resposta, NPesquisa_resposta, DPesquisa_resposta>
    {

        public DashboardController(IConfiguration configuration) : base(configuration)
        {

             this.id = "pere_nr_sequencia";                   
        }

        override protected Pesquisa_resposta OnTrataModelAfterRoute(string router, Pesquisa_resposta model, IDictionary<String, String> dictionary)
        {
            return model;
        }

        protected override string trataMensagemError(Exception ex, string Tag)
        {
            return base.trataMensagemError(ex, Tag);
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("api/pesquisa_resposta")]
        public Task<IActionResult> get() => this.GetFiltroAsync(HttpContext.Request.QueryString.Value);


        [HttpGet]
        [AllowAnonymous]
        [Route("api/pesquisa_resposta/{id}")]
        public Task<IActionResult> get(int id) => this.GetAsync(id);


        [HttpGet]
        [AllowAnonymous]
        [Route("api/dashboard/dados_geral")]
        public async Task<IActionResult> getDadosGeral() {
            try 
            {

                NPesquisa_resposta _nNegocio = new NPesquisa_resposta(this.banco);

                // IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(HttpContext.Request.QueryString.Value).ToDictionary();

                Pesquisa_resposta model = new Pesquisa_resposta(); // JsonConvert.DeserializeObject<Pesquisa_resposta>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
        
                // model = OnTrataModelAfterRoute("GET_RELATORIO_INDICADORES", model, dictionaryRequest);
                
                List<Pesquisa_resposta> retornoJson = null;
                
                await Task.Run(() => retornoJson = _nNegocio.getDadosGeral(model) );
                return Ok(new RetornoREST<Pesquisa_resposta>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = retornoJson });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<Pesquisa_resposta>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }




    }
}

