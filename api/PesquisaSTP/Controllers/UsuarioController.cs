using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PesquisaSTP.Camadas.Dados;
using PesquisaSTP.Camadas.Entidades;
using PesquisaSTP.Camadas.Negocio;
using PesquisaSTP.util;
using System.Security.Claims;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace PesquisaSTP.Controllers
{

    public class UsuarioController : ApiBASEEntityController<Usuario, NUsuario, DUsuario>
    {

        public UsuarioController(IConfiguration configuration) : base(configuration)
        {

             this.id = "usua_nr_sequencia";
        }

        override protected Usuario OnTrataModelAfterRoute(string router, Usuario model, IDictionary<String, String> dictionary)
        {
            return model;
        }

        protected override string trataMensagemError(Exception ex, string Tag)
        {
            return base.trataMensagemError(ex, Tag);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/usuario")]
        public Task<IActionResult> insert([FromBody] Usuario modelParams) => this.InserirAsync(modelParams);


        [HttpPut]
        [AllowAnonymous]
        [Route("api/usuario/{id}")]
        public Task<IActionResult> alterar(int id, [FromBody] Usuario modelParams) => this.AlterarAsync(id, modelParams);


        [HttpGet]
        [AllowAnonymous]
        [Route("api/usuario")]
        public Task<IActionResult> get() => this.GetFiltroAsync(HttpContext.Request.QueryString.Value);

        [HttpGet]
        [AllowAnonymous]
        [Route("api/usuario/{id}")]
        public Task<IActionResult> get(int id) => this.GetAsync(id);

        [HttpDelete]
        [AllowAnonymous]
        [Route("api/usuario/{id}/{idusuario}")]
        public Task<IActionResult> delete(int id, int idusuario) => this.DeleteAsync(id, idusuario);


        [AllowAnonymous]
        [Route("api/login")]
        public object Post(
            [FromBody]User usuario,
            [FromServices]SigningConfigurations signingConfigurations,
            [FromServices]TokenConfigurations tokenConfigurations)
        {
            Usuario model = new Usuario();

            bool credenciaisValidas = false;
            if (usuario != null && !String.IsNullOrWhiteSpace(usuario.email))
            {

                NUsuario _negocio = new NUsuario(banco);
                model = _negocio.getValidaUsuario(new Usuario() { Usua_tx_email = usuario.email, Usua_tx_senha = usuario.password });
                credenciaisValidas = (model != null);
            }

            if (credenciaisValidas)
            {
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(usuario.email, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, usuario.email)
                    }
                );

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao +
                    TimeSpan.FromSeconds(tokenConfigurations.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = tokenConfigurations.Issuer,
                    Audience = tokenConfigurations.Audience,
                    SigningCredentials = signingConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);

                return new
                {
                    authenticated = true,
                    created =dataCriacao,
                    expiration = dataExpiracao,
                    accessToken = token,
                    nome = model.Usua_tx_nome,
                    apelido = model.Usua_tx_apelido,
                    email =  model.Usua_tx_email,
                    id_usuario = model.Usua_nr_sequencia,
                    message = "OK"
                };
            }
            else
            {
                return StatusCode(403, new
                {
                    authenticated = false,
                    message = "Falha ao autenticar"
                });
            }


        }

                [HttpGet]
        [AllowAnonymous]
        [Route("api/usuario/esquecisenha")]
        public IActionResult esquecisenha()
        {
            try
            {
                IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(HttpContext.Request.QueryString.Value.Substring(2)).ToDictionary();

               // banco.CreateDatabaseDynamic(String.Format(_configuration.GetConnectionString("bancosSQL"), dictionaryRequest["empresa"]), "System.Data.SqlClient");

                NUsuario _nNegocio = new NUsuario(this.banco);
                String siteenvio = dictionaryRequest["camposAuxiliares.site"];
                Usuario model = JsonConvert.DeserializeObject<Usuario>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
                model = OnTrataModelAfterRoute("ESQUECISENHA", model, dictionaryRequest);

                var retornoModel = _nNegocio.esqueciSenha(model);

                String site = String.Format("{0}/#/authentication/recuperasenha/{1}", siteenvio, retornoModel.CamposAuxiliares["recu_tx_token"]);
                String html = @"
<div style='margin:0px;background-color:#e1e1e1'>
<style type='text/css'>
@font-face {
    font-family: 'My Custom Font';
    src: url(fonts/Visby/Webfont/VisbyCF-Thin-webfont.ttf) format('truetype');
}
@font-face {
    font-family: 'My Custom Font2';
    src: url(fonts/Visby/Webfont/VisbyCF-Bold-webfont.ttf) format('truetype');
}
.customfont {
    font-family: 'My Custom Font', Verdana, Tahoma;
}
.customfontB {
    font-family: 'My Custom Font2', Verdana, Tahoma;
}
</style>
<div style='background-color:#000000;margin:0px;'>
<h1 style='color:white;text-align:center;width:100%;padding-top:20px;padding-bottom:20px;margin:0px;' class='customfont'    >Agente Consultoria</h1>
</div>
<div style='background-color:#f8f8f8;margin:20px; '>
<h1 style='color:#0000;text-align:center;width:100%;padding-top:20px;padding-bottom:5px;margin:0px;' class='customfontB'    >Alteração de senha</h1>
<div style='padding-left:20px;padding-right:20px;color:#e1e1e1'><hr  ></div>
<div style='text-align:center;margin-bottom:20px'  class='customfontB'>Solicitação de troca de senha no Agente Consultoria</div>
<div style='text-align:center;margin-bottom:10px'  class='customfontB'>Acesse o link abaixo</div>
<div style='text-align:center;margin-bottom:40px'  class='customfontB'><a href='" + site + @"'>Link para recuperar senha</a></div>
<div style='text-align:center;margin-bottom:10px'  class='customfontB'>Digite a sua nova senha e confirme clicando em Enviar</div>
<div style='text-align:center;margin-bottom:10px'  class='customfontB'>Em seu proximo acesso, sua nova senha já estará configurada.</div>
<div style='text-align:center;margin-top:30px;margin-bottom:30px;padding-bottom:30px;'  class='customfontB'>Para ajuda e suporte, envie um email para:</p>
suporte@pcode.com.br</div>
</div>
</div>
</div>";

                MiniFrameWork.Util.ConfigEmail configEmail = new MiniFrameWork.Util.ConfigEmail()
                {
                    host = "smtp.gmail.com",
                    port = 587,
                    user = "suporte@pcode.com.br",
                    password = "Papo@852456",
                    from = "suporte@pcode.com.br"
                };
                List<String> enviarPara = new List<String>();
                enviarPara.Add(model.Usua_tx_email);
                MiniFrameWork.Util.SendMail.send(configEmail, "Recuperação de senha", html, enviarPara);
                return Ok(new RetornoSingleREST<Usuario>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = retornoModel });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<Usuario>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/usuario/validatoken")]
        public IActionResult validatoken()
        {
            try
            {
                IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(HttpContext.Request.QueryString.Value.Substring(2)).ToDictionary();

             //   banco.CreateDatabaseDynamic(String.Format(_configuration.GetConnectionString("bancosSQL"), dictionaryRequest["empresa"]), "System.Data.SqlClient");

                NUsuario _nNegocio = new NUsuario(this.banco);

                Usuario model = JsonConvert.DeserializeObject<Usuario>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
                model.CamposAuxiliares = new Dictionary<string, object>();
                model.CamposAuxiliares.Add("token", dictionaryRequest["token"]);
                var retornoModel = _nNegocio.validaToken(model);
                if ( retornoModel.CamposAuxiliares["status"].ToString().Equals("0"))
                   throw new Exception("Token Invalido");

                return Ok(new RetornoSingleREST<Usuario>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = retornoModel });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<Usuario>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/usuario/alterasenha")]
        public IActionResult alterasenha()
        {
            try
            {
                IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(HttpContext.Request.QueryString.Value.Substring(2)).ToDictionary();

            //    banco.CreateDatabaseDynamic(String.Format(_configuration.GetConnectionString("bancosSQL"), dictionaryRequest["empresa"]), "System.Data.SqlClient");

                NUsuario _nNegocio = new NUsuario(this.banco);

                Usuario model = JsonConvert.DeserializeObject<Usuario>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
                model.CamposAuxiliares = new Dictionary<string, object>();
                model.CamposAuxiliares.Add("token", dictionaryRequest["recu_tx_token"]);
                var retornoModel = _nNegocio.alteraSenha(model);

              //  LoginController login = new LoginController(IConfiguration);



               return Ok(new RetornoSingleREST<Usuario>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = retornoModel });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<Usuario>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }

           [HttpPost]
        [AllowAnonymous]
        [Route("api/usuario/insereempresa")]
        public IActionResult insereusuarioempresa([FromBody] Usuario modelParams)
        {
            try
            {
              //  IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(HttpContext.Request.QueryString.Value.Substring(2)).ToDictionary();


                NUsuario _nNegocio = new NUsuario(this.banco);

           //   Usuario model = JsonConvert.DeserializeObject<Usuario>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
               // model.CamposAuxiliares = new Dictionary<string, object>();
              //  model.CamposAuxiliares.Add("token", dictionaryRequest["recu_tx_token"]);
                var retornoModel = _nNegocio.InsereUsuarioEmpresa(modelParams);

              //  LoginController login = new LoginController(IConfiguration);



               return Ok(new RetornoSingleREST<Usuario>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = retornoModel });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<Usuario>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }


      [HttpPost]
        [AllowAnonymous]
        [Route("api/usuario/consultor")]
        public IActionResult insereusuarioconsultor([FromBody] Usuario modelParams)
        {
            try
            {
              //  IDictionary<string, string> dictionaryRequest = HttpUtility.ParseQueryString(HttpContext.Request.QueryString.Value.Substring(2)).ToDictionary();


                NUsuario _nNegocio = new NUsuario(this.banco);

           //   Usuario model = JsonConvert.DeserializeObject<Usuario>(JsonConvert.SerializeObject(dictionaryRequest, Formatting.Indented));
               // model.CamposAuxiliares = new Dictionary<string, object>();
              //  model.CamposAuxiliares.Add("token", dictionaryRequest["recu_tx_token"]);
                var retornoModel = _nNegocio.InsereUsuarioConsultor(modelParams);

              //  LoginController login = new LoginController(IConfiguration);



               return Ok(new RetornoSingleREST<Usuario>() { error = 0, mensagem = "MENSAGEM.SUCESSO", dados = retornoModel });

            }
            catch (Exception ex)
            {
                return BadRequest(new RetornoSingleREST<Usuario>() { error = 1, mensagem = this.trataMensagemError(ex, string.Empty), dados = null });
            }
        }


    }
}

