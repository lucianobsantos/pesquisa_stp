# NgAdminx

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.


create table grupo_modelo ( 
    identificador int IDENTITY not null, 
    descricao     varchar( 100 ) not null,
    situacao      varchar(1)     not null default 'A',
    imagem        varchar(1000)  not null
)

insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'KWID', 'A', 'kwid.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'CAPTUR', 'A', 'captur.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'DUSTER OROCH', 'A', 'oroch.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'DUSTER', 'A', 'duster.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'R.S. 2.0', 'A', 'sanderors.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'STEPWAY', 'A', 'stepway.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'SANDERO', 'A', 'sandero.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'LOGAN', 'A', 'logan.jpg'  )
insert into grupo_modelo ( descricao, situacao, imagem )  values ( 'ZOE', 'A', 'zoe.jpg'  )




## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
