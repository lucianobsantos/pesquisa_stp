import { Component, Input } from "@angular/core"
import { Estoque } from "./../../framework/model/estoque.model"

@Component({
  selector: "app-estoque",
  templateUrl: "./estoque.component.html"
})
export class EstoqueComponent {


  classeIcone = '';
  classCol = '';

  listaEstoque: Estoque[]
  @Input("estoque")
  set estoque(value: Estoque[]) {
    this.listaEstoque = value

  }

  @Input("titulo") titulo: string


  tipoCard: string
  @Input('tipo')
  set tipo(value: string) {
    this.tipoCard = value
    if ( value === 'V' ) {
      this.classeIcone = 'mdi-car';
      this.classCol = 'col-4';
    } else {
      this.classeIcone = 'mdi-car-battery';
      this.classCol = 'col-6';
    }

  }
  getTotal() {
    if (this.listaEstoque.length > 0) {
      return this.listaEstoque[0].valortotal
    } else {
      return 0
    }
  }

    getCobertura() {
      if (this.listaEstoque.length > 0) {
        let dados: any
        dados = this.listaEstoque[0].camposAuxiliares
        return dados["COBERTURA"]
      } else {
        return 0
      }
  }
}
