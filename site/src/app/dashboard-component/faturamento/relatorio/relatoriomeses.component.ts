import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { Principal } from '../../../framework/model/principal.model'
import { FilterService } from '../../../framework/service/filter.service'
import { CardIndicadorService } from '../../../framework/service/card.service'
import { ActivatedRoute } from '@angular/router'

declare const echarts: any

@Component({
  selector: 'app-relatoriomeses',
  templateUrl: './relatoriomeses.component.html'
})
export class RelatorioMesesComponent implements OnInit {
  jaCarregou = false
  id_sequencia: number
  meses: number
  visao: string
  dadosRetornoRelatorio: any = {}
  campos: any = []
  option5: any
  titulo: string
  unidade: string
  item: Principal

  constructor(
    public activatedRoute: ActivatedRoute,
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService,
    private _location: Location
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.id_sequencia = +this.activatedRoute.snapshot.paramMap.get(
        'id_sequencia'
      )
      this.meses = +this.activatedRoute.snapshot.paramMap.get(
        'meses'
      ) - 1
      this.visao = 'R$'
      this.carregaDados()
    })
  }

  ngOnInit(): void {}

  mudaVisao( ) {
    this.visao = ( this.visao === 'R$' ? 'QTD' : 'R$');
  //  let dadostemp = this.dadosRetornoRelatorio

    if ( this.visao === 'R$' ) {
      this.campos = this.dadosRetornoRelatorio.campoVL
   } else {
     this.campos = this.dadosRetornoRelatorio.campoQT
   }



  }

  carregaDados() {
    const model = {
      tipoquery: 'CARD_RELA',
      id: this.id_sequencia,
      meses : this.meses,
      visao : this.visao,
      database: '01/' + this.filterService.getfiltroPadrao().periodo,
      id_empresa: this.filterService.getfiltroPadrao().empresasfiltro.join(),
      id_departamento: this.filterService
        .getfiltroPadrao()
        .departamentosfiltro.join()
    }


    this.cardIndicadorService.getcard_01Relatorio(model).subscribe(dados => {
      this.dadosRetornoRelatorio = dados
      if ( this.visao === 'R$' ) {
         this.campos = this.dadosRetornoRelatorio.campoVL
      } else {
        this.campos = this.dadosRetornoRelatorio.campoQT
      }




    })

  }

  goBack() {
    this._location.back() // <-- go back to previous location on cancel
  }
}
