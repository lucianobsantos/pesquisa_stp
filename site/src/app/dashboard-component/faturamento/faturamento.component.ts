import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core'
import { Principal } from './../../framework/model/principal.model'
import { CardIndicadorService } from 'src/app/framework/service/card.service'
import { FilterService } from 'src/app/framework/service/filter.service'
import { trigger, state, style, animate, transition } from '@angular/animations'
import { LoginService } from 'src/app/framework/service/login.service';

declare const echarts: any

@Component({
  selector: 'app-faturamento',
  templateUrl: './faturamento.component.html',
  styleUrls: ['./faturamento.component.css'],
  animations: [
    trigger('changeState', [
      transition('*=>state1', [
        style({ transform: 'translateX(100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ]),
      transition('*=>state2', [
        style({ transform: 'translateX(-100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ])
    ]),

    trigger('changeState2', [
      transition('*=>state1', [   // :enter is alias to 'void => *'
      style({opacity:0}),
      animate(500, style({opacity:1}))
    ]),
    transition('*=>state2', [   // :leave is alias to '* => void'
      animate(500, style({opacity:0}))
    ])
    ]),



  ]
})

export class FaturamentoComponent implements OnInit {

  diaMais = false;

  subtituloGrafico: string
  titulografico: Array<any> = []
  valorgrafico: Array<any> = []
  currentState = ''
  currentState2 = ''
  tabela = 'vendedor'
  dados = {
    Titulo: '',
    valor: 0,
    valor_ant: 0,
    valor_diferenca: 0,
    percentual_diferenca: 0,
    mes_anterior: 0,
    meta: 0,
    meta_diaria: 0,
    projecao: 0,
    ticket: 0,
    valor_diario: 0,
    qtd_diario: 0,
    ticket_diario: 0,
    percentual_meta: 0,
    cor_meta: '',
    percentual_projecao_meta: 0,
    cor_projecao_meta: ''
  }
  itemDiario: Principal
  tipoView = ''
  chavepesquisa: string
  listaGrafico: Array<Principal> = []
  opcoesGrafico2 = {}
  valor = 0
  carregouGrafico = false
  corCard: string
  dataStyle = {}

  @Input() ehdetalhe = false;

  item: Principal
  @Input('principal')
  set principal(value: Principal) {
    this.item = value
    if (value) {
      // this.itemDiario = {... this.item }
      this.itemDiario =  <Principal> JSON.parse(JSON.stringify(this.item))
      this.mudaVisao()
    }
  }

  // tslint:disable-next-line:no-output-on-prefix
  @Output('onStatusChange')
  onStatusChange = new EventEmitter<object>()
  mudaCardPadrao(principal: Principal, tipo: string) {
    this.onStatusChange.emit({ model: principal, tipo: tipo })
  }




  mudaVisao() {
    this.tipoView = this.tipoView === 'R$' ? 'QTD' : 'R$'
    this.currentState = this.currentState === 'state1' ? 'state2' : 'state1'
    this.dados = {
      Titulo: this.tipoView === 'R$' ? 'Faturamento' : 'Vendas',
      valor: this.tipoView === 'R$' ? this.item.valor : this.item.qtd ,
      valor_ant:
        this.tipoView === 'R$' ? this.item.valor_ant_dia :this.item.camposAuxiliares['qtd_ant_dia'],
      valor_diferenca:
        this.tipoView === 'R$'
          ? this.item.valor - this.item.valor_ant_dia
          : this.item.qtd - this.item.camposAuxiliares['qtd_ant_dia'],
      percentual_diferenca:
        this.tipoView === 'R$'
          ? ((this.item.valor / this.item.valor_ant_dia) * 100 - 100) / 100
          : ((this.item.qtd / this.item.camposAuxiliares['qtd_ant_dia']) * 100 -
              100) /
            100,
       mes_anterior:
        this.tipoView === 'R$' ? this.item.valor_ant : this.item.qtd_ant,
      meta:
        this.tipoView === 'R$'
          ? this.item.camposAuxiliares['valor_meta']
          : this.item.camposAuxiliares['qtd_meta'],
      meta_diaria:
        this.tipoView === 'R$'
          ? this.item.camposAuxiliares['valor_meta_diaria']
          : this.item.camposAuxiliares['qtd_meta_diaria'],

      projecao:
        this.tipoView === 'R$'
          ? this.item.projecao_valor
          : this.item.projecao_qtd,
      ticket: this.item.ticket,
      valor_diario: this.item.camposAuxiliares['valor_diario'],
      qtd_diario: this.item.camposAuxiliares['qtd_diario'],
      ticket_diario: this.item.camposAuxiliares['ticket_diario'],
      percentual_meta:
      this.tipoView === 'R$'
        ? Math.round((( this.item.valor * 100) / this.item.camposAuxiliares['valor_meta']))
        : Math.round((( this.item.qtd * 100) / this.item.camposAuxiliares['qtd_meta'])) ,
       cor_meta : '',
       percentual_projecao_meta:
       this.tipoView === 'R$'
       ? Math.round((( (( this.item.projecao_valor ? this.item.projecao_valor : 0 ) + this.item.valor) * 100) / this.item.camposAuxiliares['valor_meta']))
       : Math.round((( (( this.item.projecao_qtd ? this.item.projecao_qtd : 0) + this.item.qtd) * 100) / this.item.camposAuxiliares['qtd_meta'])) ,

       cor_projecao_meta: ''
    }


   // console.log( this.dados.projecao )
    if ( !this.dados.projecao  ) {
      this.dados.projecao =  0
    }

    if (this.dados.percentual_meta <= 50) {
      this.dados.cor_meta = 'danger'
    } else if (this.dados.percentual_meta > 50 && this.dados.percentual_meta <= 90) {
      this.dados.cor_meta = 'warning'
    } else if ( this.dados.percentual_meta > 90 && this.dados.percentual_meta <= 100) {
      this.dados.cor_meta = 'info'
    } else {
      this.dados.cor_meta = 'success'
    }

    if (this.dados.percentual_projecao_meta <= 50) {
      this.dados.cor_projecao_meta = 'danger'
    } else if (this.dados.percentual_projecao_meta > 50 && this.dados.percentual_projecao_meta <= 90) {
      this.dados.cor_projecao_meta = 'warning'
    } else if ( this.dados.percentual_projecao_meta > 90 && this.dados.percentual_projecao_meta <= 100) {
      this.dados.cor_projecao_meta = 'info'
    } else {
      this.dados.cor_projecao_meta = 'success'
    }

	  if ( !this.ehdetalhe ) {
    this.getcard_01_grafico_diario({
      id_consulta: this.item.camposAuxiliares['id_consulta'],
      visao: this.tipoView,
      id_empresa: this.item.camposAuxiliares['id_empresa'],
      id_departamento: this.item.camposAuxiliares['id_departamento']
    })
  }
   this.itemDiario.camposAuxiliares['DADOS'] = { qtd : this.dados.qtd_diario , valor: this.dados.valor_diario }





  }

  getcard_01_grafico_diario(model) {
    this.cardIndicadorService
      .getcard_01_grafico_diario(model)
      .subscribe(dadosretorno => {
        this.titulografico = []
        this.valorgrafico = []

        for (let i = 0, len = dadosretorno.dados.length; i < len; i++) {
          this.subtituloGrafico =
            dadosretorno.dados[i].camposAuxiliares['dateinicio'] +
            ' até ' +
            dadosretorno.dados[i].camposAuxiliares['datefinal']
          this.titulografico.push(
            dadosretorno.dados[i].camposAuxiliares['dia'] +
              '/' +
              dadosretorno.dados[i].camposAuxiliares['mes']
          )
          this.valorgrafico.push(dadosretorno.dados[i].valor)
        }

        this.opcoesGrafico2 = {
          title: {
            text: this.dados.Titulo + ' Diario',
            subtext: this.subtituloGrafico,
            show: false
          },
          tooltip: {
            trigger: 'axis'
          },
          legend: {
            data: [this.subtituloGrafico]
          },
          toolbox: {
            show: true,
            feature: {
              magicType: {
                type: ['line', 'bar'],
                title: { line: 'Linha', bar: 'Barra' }
              },

              saveAsImage: { title: 'Salva' }
            }
          },
          dataZoom: {
            show: true,
            start: 50,
            end: 100
          },
          xAxis: {
            type: 'category',
            boundaryGap: false,
            data: this.titulografico
          },
          yAxis: {
            type: 'value',
            axisLabel: {
              formatter: '{value} '
            }
          },
          series: [
            {
              name: '08/2018',
              type: 'line',
              data: this.valorgrafico,
              markPoint: {
                data: [{ type: 'max', name: 'Maximo' }]
              },
              markLine: {
                data: [{ type: 'average', name: 'Media' }]
              }
            }
          ]
        }
      })
  }


  constructor(
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService,
    public loginService: LoginService
  ) {

  }

  ngOnInit(): void {
    this.carregouGrafico = true
  }


  animEnd(event) {
    if (   this.currentState2 === 'state2'  ) {
      this.diaMais = false
    }
  }

  diaMostraMais() {
    if (   !this.diaMais  ) {
      this.diaMais = true
    }
    this.currentState2 = (this.currentState2 === 'state1' ? 'state2' : 'state1')
  }
}
