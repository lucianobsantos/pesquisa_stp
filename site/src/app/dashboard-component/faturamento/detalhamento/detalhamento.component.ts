import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core'
import { Principal } from '.././../../framework/model/principal.model'
import { CardIndicadorService } from 'src/app/framework/service/card.service'
import { FormBuilder, FormGroup } from '@angular/forms'
import { FilterService } from 'src/app/framework/service/filter.service'
import { trigger, state, style, animate, transition } from '@angular/animations'

declare const echarts: any

@Component({
  selector: 'app-faturamento-detalhamento',
  templateUrl: './detalhamento.component.html',
  styleUrls: ['./detalhamento.component.css'],
  animations: [
    trigger('changeState', [
      transition('*=>state1', [
        style({ transform: 'translateX(100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ]),
      transition('*=>state2', [
        style({ transform: 'translateX(-100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ])
    ])
  ]
})
export class FaturamentoDetalhamentoComponent implements OnInit {
  titulo = 'Produto'
  filterRank: FormGroup
  tabela = 'produto'

  datarankTitulo: Array<any> = []
  datarank: Array<any> = []
  total: number

  listaRank: Array<Principal> = []
  listaGrafico: Array<Principal> = []
  listaTipo: Array<any> = [
    { tipo: 'V', tx_titulo: 'Vendedor', tx_tipo: 'Vendedor' },
    { tipo: 'D', tx_titulo: 'Departamento', tx_tipo: 'Departamento' },
    { tipo: 'E', tx_titulo: 'Empresa', tx_tipo: 'Empresa' },
    { tipo: 'G', tx_titulo: 'Grupo de Produto', tx_tipo: 'Grupo_produto' },
    { tipo: 'B', tx_titulo: 'Financeiro', tx_tipo: 'banco' },
    { tipo: 'P', tx_titulo: 'Produto', tx_tipo: 'Produto' },
    { tipo: 'F', tx_titulo: 'Forma Pagamento', tx_tipo: 'Forma_pagto' }
  ]
  listaCores: Array<any> = [
    '#0277B4',
    '#72A7CF',
    '#EC9938',
    '#259878',
    '#BCED5C',
    '#AD91DA'
  ]
  carregouGrafico = false
  opcoesGrafico = {}

  @Input() grupoDepartamento: string
  @Input() subtitulo: string

  @Input() datainicio: string
  @Input() datafinal: string




  tipoView: string;
  @Input()
  set setTipoView(value: string) {
    console.log(111)
    this.tipoView = value;
    if ( this.item) {
      const model = {
        id_consulta: this.item.camposAuxiliares['id_consulta'],
        tabela: this.tabela,
        mes: this.filterService.getfiltroPadrao().periodo.split('/')[0],
        todos: 'N',
        visao: this.tipoView,
        id_empresa: this.item.camposAuxiliares['id_empresa'],
        id_departamento: this.item.camposAuxiliares['id_departamento'],
        dtinicio : this.datainicio ,
        dtfinal : this.datafinal
      }

      this.mudaRank(model)
    }
}

  item: Principal
  @Input('principal')
  set principal(value: Principal) {
    this.item = value

    if (value) {

      const model = {
        id_consulta: this.item.camposAuxiliares['id_consulta'],
        tabela: this.tabela,
        mes: this.filterService.getfiltroPadrao().periodo.split('/')[0],
        todos: 'N',
        visao: this.tipoView,
        id_empresa: this.item.camposAuxiliares['id_empresa'],
        id_departamento: this.item.camposAuxiliares['id_departamento'],
        dtinicio : this.datainicio ,
        dtfinal : this.datafinal
      }

      this.mudaRank(model)

    }
  }

  // tslint:disable-next-line:no-output-on-prefix
  @Output('onStatusChange')
  onStatusChange = new EventEmitter<object>()
  mudaCardPadrao(principal: Principal, tipo: string) {
    this.onStatusChange.emit({ model: principal, tipo: tipo })
  }

  constructor(
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService,
    private fb: FormBuilder
  ) {
    this.filterRank = this.fb.group({
      tiporank: this.fb.control('V', [])
    })

    if ( this.filterService.getBiGrupo().nome_Banco === 'souzajr') {
      this.listaTipo.push( { tipo: 'X', tx_titulo: 'Tipo Venda', tx_tipo: 'tipovenda' } );
      this.listaTipo.push( { tipo: 'Y', tx_titulo: 'Gerente', tx_tipo: 'gerente' } );
      this.listaTipo.push( { tipo: 'Z', tx_titulo: 'Supervisor', tx_tipo: 'supervisor' } );
    }

  }

  ngOnInit(): void {
    this.carregouGrafico = true
  }

  onChangeRank($event) {
    this.tabela = $event.tx_tipo
    const model = {
      id_consulta: this.item.camposAuxiliares['id_consulta'],
      tabela: this.tabela,
      mes: this.filterService.getfiltroPadrao().periodo.split('/')[0],
      todos: 'N',
      visao: this.tipoView,
      id_empresa: this.item.camposAuxiliares['id_empresa'],
      id_departamento: this.item.camposAuxiliares['id_departamento'],
      dtinicio : this.datainicio ,
      dtfinal : this.datafinal
    }

    this.mudaRank(model)
    this.titulo = $event.tx_titulo
  }

  mudaRank(model) {
    this.cardIndicadorService.getRank(model).subscribe(dadosretorno => {
      this.listaRank = dadosretorno.dados
      this.datarank = []
      this.datarankTitulo = []

      for (let i = 0, len = this.listaRank.length; i < len; i++) {
        this.datarank.push({
          value: this.listaRank[i].valor,
          name: this.listaRank[i].camposAuxiliares['descricao']
        })
        this.datarankTitulo.push(
          this.listaRank[i].camposAuxiliares['descricao']
        )
      }


      if ( !this.item.camposAuxiliares['DADOS']) {
        this.total = ( this.tipoView === 'R$' ? this.item.valor : this.item.qtd )
      } else {
        this.total = ( this.tipoView === 'R$' ? this.item.camposAuxiliares['DADOS']['valor'] : this.item.camposAuxiliares['DADOS']['qtd'] )
      }

      this.opcoesGrafico = {
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        legend: {
          orient: 'vertical',
          x: 'left',
          show: false,
          data: this.datarankTitulo
        },
        series: [
          {
            name: this.titulo,
            type: 'pie',
            radius: ['50%', '70%'],
            color: [
              '#0277B4',
              '#72A7CF',
              '#EC9938',
              '#259878',
              '#BCED5C',
              '#AD91DA'
            ],
            avoidLabelOverlap: false,
            label: {
              normal: {
                show: false,
                position: 'center'
              },
              emphasis: {
                show: true,
                textStyle: {
                  fontSize: '12',
                  fontWeight: 'bold'
                }
              }
            },
            labelLine: {
              normal: {
                show: false
              }
            },
            data: this.datarank
          }
        ]
      }
    })
  }
}
