import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core'
import { Principal } from '.././../../framework/model/principal.model'
import { CardIndicadorService } from 'src/app/framework/service/card.service'
import { FormBuilder, FormGroup } from '@angular/forms'
import { FilterService } from 'src/app/framework/service/filter.service'
import { trigger, state, style, animate, transition } from '@angular/animations'

declare const echarts: any

@Component({
  selector: 'app-faturamento-acumulado',
  templateUrl: './acumulado.component.html',
  animations: [
    trigger('changeState', [
      transition('*=>state1', [
        style({ transform: 'translateX(100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ]),
      transition('*=>state2', [
        style({ transform: 'translateX(-100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ])
    ])
  ]
})
export class AcumuladoComponent implements OnInit {

  currentState = ''
  dados = {
    Titulo: '',
    valor: 0,
    valor_ant: 0,
    valor_diferenca: 0,
    percentual_diferenca: 0,
    mes_anterior: 0,
    meta: 0,
    meta_diaria: 0,
    projecao: 0,
    ticket: 0,
    valor_diario: 0,
    qtd_diario: 0,
    ticket_diario: 0
  }
  tipoView = ''
  chavepesquisa: string
  datarankTitulo: Array<any> = []
  datarank: Array<any> = []
  titulo = 'Vendedor'
  filterRank: FormGroup
  listaRank: Array<Principal> = []
  listaGrafico: Array<Principal> = []
  listaTipo: Array<any> = [
    { tipo: 'V', tx_titulo: 'Vendedor', tx_tipo: 'Vendedor' },
    { tipo: 'D', tx_titulo: 'Departamento', tx_tipo: 'Departamento' },
    { tipo: 'E', tx_titulo: 'Empresa', tx_tipo: 'Empresa' },
    { tipo: 'G', tx_titulo: 'Grupo de Produto', tx_tipo: 'Grupo_produto' },
    { tipo: 'B', tx_titulo: 'Financeiro', tx_tipo: 'banco' },
    { tipo: 'P', tx_titulo: 'Produto', tx_tipo: 'Produto' },
    { tipo: 'F', tx_titulo: 'Forma Pagamento', tx_tipo: 'Forma_pagto' }
  ]


  opcoesGrafico2 = {}

  valor = 0
  carregouGrafico = false
  opcoesGrafico = {}

  corCard: string
  dataStyle = {}

  item: Principal
  @Input('principal')
  set principal(value: Principal) {
    this.item = value

    if (value) {
      this.mudaVisao()
    }
  }

  // tslint:disable-next-line:no-output-on-prefix
  @Output('onStatusChange')
  onStatusChange = new EventEmitter<object>()
  mudaCardPadrao(principal: Principal, tipo: string) {
    this.onStatusChange.emit({ model: principal, tipo: tipo })
  }

  mudaVisao() {
    this.tipoView = this.tipoView === 'R$' ? 'QTD' : 'R$'
    this.currentState = this.currentState === 'state1' ? 'state2' : 'state1'
    this.dados = {
      Titulo: this.tipoView === 'R$' ? 'Faturamento' : 'Vendas',
      valor: this.tipoView === 'R$' ? this.item.valor : this.item.qtd,
      valor_ant:
        this.tipoView === 'R$' ? this.item.valor_ant_dia : this.item.camposAuxiliares['qtd_ant_dia'],
      valor_diferenca:
        this.tipoView === 'R$'
          ? this.item.valor - this.item.valor_ant_dia
          : this.item.qtd - this.item.camposAuxiliares['qtd_ant_dia'],
      percentual_diferenca:
        this.tipoView === 'R$'
          ? ((this.item.valor / this.item.valor_ant_dia) * 100 - 100) / 100
          : ((this.item.qtd / this.item.camposAuxiliares['qtd_ant_dia']) * 100 -
              100) /
            100,
      mes_anterior:
        this.tipoView === 'R$' ? this.item.valor_ant : this.item.qtd_ant,
      meta:
        this.tipoView === 'R$'
          ? this.item.camposAuxiliares['valor_meta']
          : this.item.camposAuxiliares['qtd_meta'],
      meta_diaria:
        this.tipoView === 'R$'
          ? this.item.camposAuxiliares['valor_meta_diaria']
          : this.item.camposAuxiliares['qtd_meta_diaria'],

      projecao:
        this.tipoView === 'R$'
          ? this.item.projecao_valor
          : this.item.projecao_qtd,
      ticket: this.item.ticket,
      valor_diario: this.item.camposAuxiliares['valor_diario'],
      qtd_diario: this.item.camposAuxiliares['qtd_diario'],
      ticket_diario: this.item.camposAuxiliares['ticket_diario']
    }


  }


  constructor(
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService
  ) {

    if ( this.filterService.getBiGrupo().nome_Banco === 'souzajr') {
      this.listaTipo.push( { tipo: 'X', tx_titulo: 'Tipo Venda', tx_tipo: 'tipovenda' } );
      this.listaTipo.push( { tipo: 'Y', tx_titulo: 'Gerente', tx_tipo: 'gerente' } );
      this.listaTipo.push( { tipo: 'Z', tx_titulo: 'Supervisor', tx_tipo: 'supervisor' } );
    }

  }


  ngOnInit(): void {
    this.carregouGrafico = true
  }
}
