import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { Principal } from '../../../framework/model/principal.model'
import { ItemVenda } from '../../../framework/model/itemvenda.model'
import { FilterService } from '../../../framework/service/filter.service'
import { CardIndicadorService } from '../../../framework/service/card.service'
import { ActivatedRoute } from '@angular/router'

declare const echarts: any

@Component({
  selector: 'app-relatorioanalitico',
  templateUrl: './relatorioanalitico.component.html'
})
export class RelatorioAnaliticoComponent implements OnInit {
  jaCarregou = false
  id_sequencia: number

  dadosRetornoRelatorio: {}
  campos: any = []
  id_consulta: string

  titulo: string
  unidade: string
  item: Principal

  constructor(
    public activatedRoute: ActivatedRoute,
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService,
    private _location: Location
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.id_consulta =  this.activatedRoute.snapshot.queryParamMap.get(
        'id_consulta'
      )
      this.carregaDados()
    })
  }

  ngOnInit(): void {}

  carregaDados() {
    const model = {
      id_consulta: this.id_consulta,
      database: '01/' + this.filterService.getfiltroPadrao().periodo,
      id_empresa: this.filterService.getfiltroPadrao().empresasfiltro.join(),
      id_departamento: this.filterService
        .getfiltroPadrao()
        .departamentosfiltro.join()
    }

    this.cardIndicadorService.getAnalitico(model).subscribe(dados => {


      this.dadosRetornoRelatorio  = dados.dados.reduce((previous, current) => {
      //  previous[current['data_hora_venda']] = [{ data_hora_venda : current['data_hora_venda'] }] ;
      //  previous[current['data_hora_venda']].push(current);


        if (!previous[current['data_hora_venda']]) {
            previous[current['data_hora_venda']] = []; //[{ data_hora_venda : current['data_hora_venda'] }];
        }

        if (!previous[current['data_hora_venda']][current['num_venda']]) {
          previous[current['data_hora_venda']][current['num_venda']] = [current];
        } else {
          previous[current['data_hora_venda']][current['num_venda']].push(current);
        }




        return previous;
    }, {});


console.log(this.dadosRetornoRelatorio );
    })

  }

  goBack() {
    this._location.back() // <-- go back to previous location on cancel
  }
}
