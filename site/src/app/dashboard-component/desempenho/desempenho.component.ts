import { Component, Input } from '@angular/core';
import { Desempenho } from './../../framework/model/desempenho.model'

@Component({
  selector: 'app-desempenho',
  templateUrl: './desempenho.component.html',

})
export class DesempenhoComponent {

  item: Desempenho;
  @Input('desempenho')
  set desempenho(value: Desempenho) {
    this.item = value;
  }

}
