import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core'
import { Principal } from './../../framework/model/principal.model'

declare const echarts: any

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html'
})
export class PrincipalComponent implements OnInit {
  valor = 0
  carregouGrafico = false
  opcoesGrafico: any
  placeHolderStyle = {
    normal: {
      label: {
        show: false
      },
      labelLine: {
        show: false
      },
      color: 'rgba(0,0,0,0)',
      borderWidth: 0
    },
    emphasis: {
      color: 'rgba(0,0,0,0)',
      borderWidth: 0
    }
  }

  corCard: string
  dataStyle = {}

  item: Principal
  @Input('principal')
  set principal(value: Principal) {
    this.item = value
  }

  @Output('onStatusChange')
  onStatusChange = new EventEmitter<object>()
  mudaCardPadrao(  principal: Principal, tipo: string ) {
    this.onStatusChange.emit({ model: principal, tipo: tipo } );
  }




  ngOnInit(): void {


    let meta = this.item.valor
    if (this.item.meta) {
      meta = this.item.meta
    }
    if ( meta === 0 ) {
      this.valor  = 0
    } else {
    this.valor = Math.round((this.item.valor * 100) / meta)
    }


    if (this.valor <= 50) {
      this.corCard = 'bg-danger'
    } else if (this.valor > 50 && this.valor <= 90) {
      this.corCard = 'bg-warning'
    } else if (this.valor > 90 && this.valor <= 100) {
      this.corCard = 'bg-info'
    } else {
      this.corCard = 'bg-success'
    }

    this.dataStyle = {
      normal: {
        formatter: this.valor + '%',
        position: 'center',
        top: '1%',
        show: true,
        textStyle: {
          fontSize: '25',
          fontWeight: 'normal',
          color: '#fff'
        }
      }
    }
console.log( this.valor)
    this.opcoesGrafico = {
      series: [
        {
          type: 'pie',
          hoverAnimation: false,
          radius: ['70%', '95%'],
          center: ['50%', '50%'],
          startAngle: 90,
          labelLine: {
            normal: {
              show: false
            }
          },
          label: {
            normal: {
              position: 'center'
            }
          },
          data: [
            {
              value: 100,
              itemStyle: {
                normal: {
                  color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                    {
                      offset: 0,
                      color: '#ffffff'
                    },
                    {
                      offset: 1,
                      color: '#ffffff'
                    }
                  ])
                }
              }
            },
            {
              value: 0,
              itemStyle: this.placeHolderStyle
            }
          ]
        },
        {
          type: 'pie',
          hoverAnimation: false,
          radius: ['70%', '95%'],
          center: ['50%', '50%'],
          startAngle: 90,
          labelLine: {
            normal: {
              show: false
            }
          },
          label: {
            normal: {
              position: 'center'
            }
          },
          data: [
            {
              value: this.valor > 100 ? 100 : this.valor,
              itemStyle: {
                normal: {
                  color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                    {
                      offset: 0,
                      color: '#DAE2DB'
                    },
                    {
                      offset: 1,
                      color: '#534650'
                    }
                  ])
                }
              },
              label: this.dataStyle
            },
            {
              value: 100 - (this.valor > 100 ? 100 : this.valor),
              itemStyle: this.placeHolderStyle
            }
          ]
        }
      ]
    }
    this.carregouGrafico = true
  }
}
