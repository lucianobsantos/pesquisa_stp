import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { NgbProgressbarConfig } from '@ng-bootstrap/ng-bootstrap';


import { Principal } from './../../framework/model/principal.model'
import { FilterService } from '../../framework/service/filter.service'
import { CardIndicadorService } from '../../framework/service/card.service'
import { BIEMPRESAService } from '../../framework/service/biempresa.service'
import { BIDEPARTAMENTOService } from '../../framework/service/bidepartamento.service'
import { ActivatedRoute, Router } from '@angular/router'
import { LoginService } from 'src/app/framework/service/login.service';

declare const echarts: any

@Component({
  selector: 'app-detalhamento',
  templateUrl: './detalhamento.component.html',
  providers: [NgbProgressbarConfig]
})
export class DetalhamentoComponent implements OnInit {

  modelPrincipal: {}
  listaFaturamento: Array<Principal> = []
  jaCarregou = false
  id_consulta: string
  id_sequencia: number
  id_empresa: number
  id_departamento: number
  dadosRetornoRelatorio: any = {}
  option5: any
  titulo: string
  unidade: string
  item: Principal
  total: number
  tipoquery = 'DETALHAMENTO_EMPRESA'
  camposhow = 'empresa'
  camposhowTitulo = 'Empresa'

  arrayEmpresa = []
  arrayDepartamento = []
  tipo = 'X'

  constructor(
    public activatedRoute: ActivatedRoute,
    private _router: Router ,
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService,
    private _location: Location,
    private biempresaService: BIEMPRESAService,
    private bidepartamentoService: BIDEPARTAMENTOService,
    private loginService: LoginService
  ) {

    this.arrayEmpresa =  this.filterService.getfiltroPadrao().empresasfiltro
    this.arrayDepartamento =  this.filterService.getfiltroPadrao().departamentosfiltro

    if ( this._router.url.split('/')[2] === 'empresa' ) {
      this.tipo = 'E'
      this.tipoquery = 'DETALHAMENTO_EMPRESA'
      this.camposhow = 'empresa'
      this.camposhowTitulo = 'Detalhamento por Empresa'

     /* this.activatedRoute.params.subscribe(params => {
        this.id_empresa = + this.activatedRoute.snapshot.paramMap.get(
          'id_empresa'
        )
      })*/
      this.activatedRoute.params.subscribe(params => {
        this.id_consulta =  this.activatedRoute.snapshot.queryParamMap.get(
          'id_consulta'
        )
      })

    } else if ( this._router.url.split('/')[2] === 'departamento' ) {
      this.tipo = 'D'
      this.tipoquery = 'DETALHAMENTO_DEPARTAMENTO'
      this.camposhow = 'departamento'
      this.camposhowTitulo = 'Detalhamento por Departamento'
     /* this.activatedRoute.params.subscribe(params => {
        this.id_departamento = + this.activatedRoute.snapshot.paramMap.get(
          'id_departamento'
        )
      })*/
      this.activatedRoute.params.subscribe(params => {
        this.id_consulta =  this.activatedRoute.snapshot.queryParamMap.get(
          'id_consulta'
        )
      })
    } else if ( this._router.url.split('/')[2] === 'empresadepartamento' ) {
      this.tipoquery = 'DETALHAMENTO_DEPARTAMENTO'
      this.camposhow = 'departamento'
      this.camposhowTitulo = 'Detalhamento por departamento da empresa : '
      this.activatedRoute.params.subscribe(params => {
        this.id_empresa = + this.activatedRoute.snapshot.paramMap.get(
          'id_empresa'
        )
        this.arrayEmpresa = []
        this.arrayEmpresa.push( this.id_empresa )
       this.biempresaService.get( this.id_empresa ).subscribe(dados => {
         if (dados) {
          this.camposhowTitulo = 'Detalhamento por departamento da empresa : ' + dados.dados.nome
         }
      })

      })
    } else if ( this._router.url.split('/')[2] === 'departamentoempresa' ) {
      this.tipoquery = 'DETALHAMENTO_EMPRESA'
      this.camposhow = 'empresa'
      this.camposhowTitulo = 'Detalhamento por empresa do departamento : '
      this.activatedRoute.params.subscribe(params => {
        this.id_departamento = +this.activatedRoute.snapshot.paramMap.get(
          'id_departamento'
        )

        this.bidepartamentoService.get( this.id_departamento ).subscribe(dados => {
          if (dados) {
           this.camposhowTitulo = 'Detalhamento por empresa do departamento : ' + dados.dados.nome
          }
       })


        this.arrayDepartamento = []
        this.arrayDepartamento.push( this.id_departamento )
        console.log( this.arrayDepartamento )
      })
    }
    this.activatedRoute.params.subscribe(params => {
      this.id_sequencia = +this.activatedRoute.snapshot.paramMap.get(
        'id_sequencia'
      )
     console.log(this.arrayEmpresa.join())
      this.carregaDados()
    })


  }

  ngOnInit(): void {}

  carregaDados() {
    const model = {
      tipoquery: this.tipoquery ,
      id: this.id_sequencia,
      database: '01/' + this.filterService.getfiltroPadrao().periodo,
      id_empresa:  this.arrayEmpresa.join(),
      id_departamento: this.arrayDepartamento.join()
    }
    if ( this.tipoquery === 'DETALHAMENTO_EMPRESA') {
      this.modelPrincipal = { 'tipoquery' : 'card_empresa' , id: null, id_usuario : this.loginService.user.id_usuario,
        id_consulta : this.id_consulta  };
    } else {
      this.modelPrincipal = { 'tipoquery' : 'card_departamento' , id: null, id_usuario : this.loginService.user.id_usuario,
        id_consulta : this.id_consulta  };
    }

    this.cardIndicadorService.getIndicadorFaturamento(this.modelPrincipal).subscribe(resultado => {
      this.listaFaturamento = resultado.dados
      console.log( this.listaFaturamento )
    })


/*
    this.cardIndicadorService.getRelatorioCustom(model).subscribe(dados => {
      console.log( dados )
      this.dadosRetornoRelatorio = dados
       if (dados) {
        this.titulo = dados.dados[0].titulo
        this.unidade = dados.dados[0].unidade
      }
     this.total =   dados.dados.map(item => item.camposAuxiliares[dados.campo[0]]  ).reduce((prev, next) => prev + next);
     console.log( '==========================')
console.log( dados )
console.log( '==========================')
      this.jaCarregou = true
    })
*/

  }


  mudaIndicadorPrincipal(principal: object) {

  }


  getPercent( valor, total ) {
    return Math.floor((valor * 100) / total)
  }


  goBack() {
    this._location.back() // <-- go back to previous location on cancel
  }


  detalha( item: any ) {

    if ( this.tipo === 'E' ) {

      this._router.navigateByUrl(
        '/starter/empresadepartamento/' +  this.id_sequencia + '/' + item['camposAuxiliares']['id_empresa']
      )
    } else  if ( this.tipo === 'D' ) {
      this._router.navigateByUrl(
        '/starter/departamentoempresa/' +  this.id_sequencia + '/' +  item['camposAuxiliares']['id_departamento']
      )
    }

    console.log( this.tipo )

  }
}
