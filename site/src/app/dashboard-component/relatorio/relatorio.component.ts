import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { Principal } from './../../framework/model/principal.model'
import { FilterService } from '../../framework/service/filter.service'
import { CardIndicadorService } from '../../framework/service/card.service'
import { ActivatedRoute } from '@angular/router'

declare const echarts: any

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html'
})
export class RelatorioComponent implements OnInit {
  jaCarregou = false
  id_sequencia: number
  dadosRetornoRelatorio: any = {}
  option5: any
  titulo: string
  unidade: string
  item: Principal

  constructor(
    public activatedRoute: ActivatedRoute,
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService,
    private _location: Location
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.id_sequencia = +this.activatedRoute.snapshot.paramMap.get(
        'id_sequencia'
      )
      this.carregaDados()
    })
  }

  ngOnInit(): void {}

  carregaDados() {
    const model = {
      tipoquery: 'GRAFICO_ALL',
      id: this.id_sequencia,
      database: '01/' + this.filterService.getfiltroPadrao().periodo,
      id_empresa: this.filterService.getfiltroPadrao().empresasfiltro.join(),
      id_departamento: this.filterService
        .getfiltroPadrao()
        .departamentosfiltro.join()
    }
    this.cardIndicadorService.getRelatorio(model).subscribe(dados => {
      this.dadosRetornoRelatorio = dados
      if (dados) {
        this.titulo = dados.dados[0].titulo
        this.unidade = dados.dados[0].unidade
      }
      this.jaCarregou = true
    })

    model.tipoquery = 'GRAFICO'
    this.cardIndicadorService.getGrafico(model).subscribe(dados => {
      this.setaGrafico(dados)
    })
  }

  setaGrafico(dadosGrafico: any) {
    this.option5 = {
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: [this.titulo]
      },
      toolbox: {
        show: true,
        feature: {
          mark: { show: false },
          dataView: { show: false, readOnly: false },
          magicType: { show: true, type: ['line', 'bar'], title: {line:'Linha', bar:'Barra' } },
          restore: { show: false },
          saveAsImage: { show: false }
        }
      },
      calculable: true,
      xAxis: [
        {
          type: 'category',
          boundaryGap: false,
          data: dadosGrafico.label
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            formatter: '{value}'
          }
        }
      ],
      series: [
        {
          name: this.titulo,
          type: 'line',
          data: dadosGrafico.dados,
          markPoint: {
            data: [
              {
                name: 'Mes Atual',
                value: dadosGrafico.dados[dadosGrafico.dados.length - 1],
                xAxis: 1,
                yAxis: -1.5
              }
            ]
          },
          markLine: {
            data: [{ type: 'average', name: 'Media' }]
          }
        }
      ]
    }
  }

  goBack() {
    this._location.back() // <-- go back to previous location on cancel
  }
}
