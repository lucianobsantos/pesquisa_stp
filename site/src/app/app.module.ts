import * as $ from 'jquery';
import {registerLocaleData} from '@angular/common';
import br from '@angular/common/locales/br';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  CommonModule,
  LocationStrategy,
  HashLocationStrategy
} from '@angular/common';
import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';



import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';

import { NavigationComponent } from './shared/header-navigation/navigation.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';

import { Approutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpinnerComponent } from './shared/spinner.component';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';


import { ApplicationErrorHandler } from './framework/service/ApplicationErrorHandler'
import { AuthInterceptor } from './framework/security/auth.interceptor'
import { LoginService } from './framework/service/login.service'
import { UsuarioService } from './framework/service/usuario.service'
import { AuthGuard } from './framework/security/auth-guard.service'
import { FilterService } from './framework/service/filter.service';
import { BIEMPRESAService } from './framework/service/biempresa.service';
import { BIGRUPOService } from './framework/service/bigrupo.service';
import { BIDEPARTAMENTOService } from './framework/service/bidepartamento.service';

import { TabelaGridInlineComponent } from './framework/component/tabela-gridinline/tabela-gridinline.component';
import { TabelaGridFilhoInlineComponent } from './framework/component/tabela-gridfilhoinline/tabela-gridfilhoinline.component';
import { TabelaGridComponent } from './framework/component/tabela-grid/tabela-grid.component'
import { TelaPadraoComponent } from './framework/component/telapadrao/telapadrao.component'



const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 2,
  wheelPropagation: true,
  minScrollbarLength: 20
};
registerLocaleData(br, 'pt-BR');
@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    FullComponent,
    BlankComponent,
    NavigationComponent,
    BreadcrumbComponent,
    SidebarComponent,
    TabelaGridInlineComponent,
    TabelaGridFilhoInlineComponent,
    TabelaGridComponent,
    TelaPadraoComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot({  enableHtml: true, preventDuplicates: true, closeButton: true , progressBar: true}),
    RouterModule.forRoot(Approutes, { useHash: false }),
    PerfectScrollbarModule,
    ReactiveFormsModule
  ],
  providers: [

    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: ErrorHandler, useClass: ApplicationErrorHandler },
    LoginService, UsuarioService,
    AuthGuard, FilterService,
    BIEMPRESAService, BIDEPARTAMENTOService,
    BIGRUPOService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
