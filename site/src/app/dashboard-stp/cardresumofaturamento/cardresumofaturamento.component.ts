import { Component, Input } from '@angular/core';
import { Principal } from '../../framework/model/principal.model';

@Component({
  selector: 'app-resumofaturamento',
  templateUrl: './cardresumofaturamento.component.html'
})
export class ResumoFaturamentoComponent {
  currentState = '';
  currentState2 = '';

  @Input() grupo = '';

  item: Principal = null;
  @Input('item')
  set principal(value: Principal) {
    this.item = value;
    if (value) {

      // this.itemDiario = {... this.item }
      // this.itemDiario =  <Principal> JSON.parse(JSON.stringify(this.item))
      this.mudaVisao();
    }
  }

  _tipoView: string = '';
  @Input('tipoView')
  set settipoView(value: string) {
    if (value) {

      this._tipoView = value;

      this.mudaVisao();
    }
  }

  dados = {
    Titulo: '',
    valor: 0,
    valor_ant: 0,
    valor_diferenca: 0,
    percentual_diferenca: 0,
    mes_anterior: 0,
    meta: 0,
    meta_diaria: 0,
    projecao: 0,
    ticket: 0,
    valor_diario: 0,
    qtd_diario: 0,
    ticket_diario: 0,
    percentual_meta: 0,
    cor_meta: '',
    percentual_projecao_meta: 0,
    cor_projecao_meta: ''
  };

  mudaVisao() {

    this.currentState = this.currentState === 'state1' ? 'state2' : 'state1';
    (this._tipoView = this._tipoView.length < 2 ? 'R$' : this._tipoView),

    this.dados = {
      Titulo: this._tipoView === 'R$' ? 'Faturamento' : 'Vendas',
      valor: this._tipoView === 'R$' ? this.item.valor : this.item.qtd,
      valor_ant:
        this._tipoView === 'R$'
          ? this.item.valor_ant_dia
          : this.item.camposAuxiliares['qtd_ant_dia'],
      valor_diferenca:
        this._tipoView === 'R$'
          ? this.item.valor - this.item.valor_ant_dia
          : this.item.qtd - this.item.camposAuxiliares['qtd_ant_dia'],
      percentual_diferenca:
        this._tipoView === 'R$'
          ? ((this.item.valor / this.item.valor_ant_dia) * 100 - 100) / 100
          : ((this.item.qtd / this.item.camposAuxiliares['qtd_ant_dia']) * 100 -
              100) /
            100,
      mes_anterior:
        this._tipoView === 'R$' ? this.item.valor_ant : this.item.qtd_ant,
      meta:
        this._tipoView === 'R$'
          ? this.item.camposAuxiliares['valor_meta']
          : this.item.camposAuxiliares['qtd_meta'],
      meta_diaria:
        this._tipoView === 'R$'
          ? this.item.camposAuxiliares['valor_meta_diaria']
          : this.item.camposAuxiliares['qtd_meta_diaria'],

      projecao:
        this._tipoView === 'R$'
          ? this.item.projecao_valor
          : this.item.projecao_qtd,
      ticket: this.item.ticket,
      valor_diario: this.item.camposAuxiliares['valor_diario'],
      qtd_diario: this.item.camposAuxiliares['qtd_diario'],
      ticket_diario: this.item.camposAuxiliares['ticket_diario'],
      percentual_meta:
        this._tipoView === 'R$'
          ? Math.round(
              (this.item.valor * 100) / this.item.camposAuxiliares['valor_meta']
            )
          : Math.round(
              (this.item.qtd * 100) / this.item.camposAuxiliares['qtd_meta']
            ),
      cor_meta: '',
      percentual_projecao_meta:
        this._tipoView === 'R$'
          ? Math.round(
              (((this.item.projecao_valor ? this.item.projecao_valor : 0) +
                this.item.valor) *
                100) /
                this.item.camposAuxiliares['valor_meta']
            )
          : Math.round(
              (((this.item.projecao_qtd ? this.item.projecao_qtd : 0) +
                this.item.qtd) *
                100) /
                this.item.camposAuxiliares['qtd_meta']
            ),

      cor_projecao_meta: ''
    };

    if (!this.dados.projecao) {
      this.dados.projecao = 0;
    }

    if (this.dados.percentual_meta <= 50) {
      this.dados.cor_meta = 'danger';
    } else if (
      this.dados.percentual_meta > 50 &&
      this.dados.percentual_meta <= 90
    ) {
      this.dados.cor_meta = 'warning';
    } else if (
      this.dados.percentual_meta > 90 &&
      this.dados.percentual_meta <= 100
    ) {
      this.dados.cor_meta = 'info';
    } else {
      this.dados.cor_meta = 'success';
    }

    if (this.dados.percentual_projecao_meta <= 50) {
      this.dados.cor_projecao_meta = 'danger';
    } else if (
      this.dados.percentual_projecao_meta > 50 &&
      this.dados.percentual_projecao_meta <= 90
    ) {
      this.dados.cor_projecao_meta = 'warning';
    } else if (
      this.dados.percentual_projecao_meta > 90 &&
      this.dados.percentual_projecao_meta <= 100
    ) {
      this.dados.cor_projecao_meta = 'info';
    } else {
      this.dados.cor_projecao_meta = 'success';
    }
  }
}
