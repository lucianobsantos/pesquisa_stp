import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

import { IBaseService } from '../../../framework/service/IBaseService'
import { BaseService } from '../../../framework/service/base.service'
import { LoginService } from '../../../framework/service/login.service'
import { User } from '../../../framework/model/user.model'

@Component({
  selector: 'app-tabelagrid',
  templateUrl: './tabela-grid.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})
export class TabelaGridComponent<T> implements OnInit {
  public listaSituacao: Array<any> = [
    { situacao: '', tx_situacao: 'Todos' },
    { situacao: 'A', tx_situacao: 'Ativo' },
    { situacao: 'I', tx_situacao: 'Inativo' }
  ]

  public user: User
  public position = 'top-right'
  public lista: T[] = []
  public modelEdit: T = {} as T
  public paginacao = {
    totalItens: 0,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }
  public callDadosInit = true;
  public acao = 'CONSULTAR'

  public tituloPagina: string
  filterForm: FormGroup

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastr: ToastrService,
    public loginService: LoginService,
    public service: BaseService<T>
  ) { }

  ngOnInit() {
    this.user = this.loginService.user

    if ( this.callDadosInit ) {
      this.getDados()
    }
  }

  getPage() {
    this.getDados()
  }

  pesquisaDados() {
    this.paginacao.paginaCorrente = 0;
    this.getDados();
  }

  getDados() {
    (this.service as IBaseService)
      .getFilter(this.filterForm.value, this.paginacao.paginaCorrente, this.paginacao.itensPorPagina)
      .subscribe(retorno => {
        this.lista = retorno.dados
        if (retorno.total_registro) {
          this.paginacao.totalItens = retorno.total_registro
        }
      })
  }


  ativaInativa(item) {

    const titulo = `Deseja ${item.situacao === 'A' ? 'Inativar' : 'Ativar'} o Registro ?`
    swal({
      title: titulo,
      text: '',
      icon: 'warning',
      buttons: {
        nao: {
          text: 'Não',
          value: false
        },
        ok: {
          text: 'Sim',
          value: true,
          closeModal: false
        }
      },
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.service.delete(this.getId(item), this.user.id_usuario).subscribe(
            retorno => {
              this.toastr.success(retorno.mensagem, 'Sucesso!')
              this.setCancel();
              swal.stopLoading()
              swal.close('cancel')
            },
            response => {
              this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
            }
          )
        }
      });
  }

  setCancel() {
    this.acao = 'CONSULTAR'
    this.modelEdit = {} as T
    this.getDados()
  }

  getId(model: T): number {
    return -1
  }

  gotoBack() {
    window.history.back()
  }
}
