import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  Input
} from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

import { IBaseService } from '../../../framework/service/IBaseService'
import { BaseService } from '../../../framework/service/base.service'
import { LoginService } from '../../../framework/service/login.service'
import { User } from '../../../framework/model/user.model'

@Component({
  selector: 'app-tabelagridfilhoinline',
  templateUrl: './tabela-gridfilhoinline.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})
export class TabelaGridFilhoInlineComponent<T> implements OnInit {
  @ViewChild('areaValidacao') areaValidacao: ElementRef
  @Input() modelPai: T = {} as T
  public listaSituacao: Array<any> = [
    { situ_tx_situacao: '', tx_situacao: 'Todos' },
    { situ_tx_situacao: 'A', tx_situacao: 'Ativo' },
    { situ_tx_situacao: 'I', tx_situacao: 'Inativo' }
  ]
  public user: User
  public position = 'top-right'
  public lista: T[] = []
  public modelEdit: T = {} as T
  public paginacao = {
    totalItens: 30,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }
  public acao = 'LISTAR'
  public novaAcao = 'LISTAR'

  public tituloPagina: string

  filterEdicao: FormGroup

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastr: ToastrService,
    public loginService: LoginService,
    public service: BaseService<T>
  ) {}

  ngOnInit() {
    this.filterEdicao.disable()
    this.user = this.loginService.user
    this.getDados(0)
  }

  setInsert() {
    this.modelEdit = {} as T
  }

  setEdit(model: T) {
    this.getModel(model)
    this.novaAcao = 'UPDATE'
  }

  setConsulta(model: T) {
    this.getModel(model)
    this.novaAcao = 'CONSULTAR'
  }

  setCancel() {
    this.filterEdicao.disable()
    this.acao = 'LISTAR'
    this.modelEdit = {} as T
    this.getDados(this.paginacao.paginaCorrente)
  }

  getPage(page: number) {
    this.getDados(page)
  }

  getDados(pagina) {
    (this.service as IBaseService)
      .getFilter(this.modelPai, 1 , 200)
      .subscribe(retorno => {
        this.lista = retorno.dados
      })
  }

  saveDados() {
    if (!this.filterEdicao.valid) {
      this.toastr.error(this.areaValidacao.nativeElement.innerHTML, 'Erro!')
      return false
    }

    const modelSend: T = this.setFormGroupTOModel(this.modelEdit)
    if (this.acao !== 'INCLUIR') {
      this.service.put(this.getId(modelSend), modelSend).subscribe(
        retorno => {
          this.setCancel()
          this.toastr.success(retorno.mensagem, 'Sucesso!')
        },
        response => {
          this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
        }
      )
    } else {
      this.service.post(modelSend).subscribe(
        retorno => {
          this.setCancel()
          this.toastr.success(retorno.mensagem, 'Sucesso!')
        },
        response => {
          this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
        }
      )
    }
  }

  onTratarModelEditPosGet() {}

  getId(model: T): number {
    return -1
  }

  getModel(model: T) {
    this.service.get(this.getId(model)).subscribe(retorno => {
      this.modelEdit = retorno.dados
      this.onTratarModelEditPosGet()
      this.acao = this.novaAcao
      this.setFormGroup(this.modelEdit)
    })
  }

  gotoBack() {
    window.history.back()
  }

  setFormGroup(model: T) {
    for (let key in this.filterEdicao.controls) {
      this.filterEdicao.controls[key].setValue(model[key])
    }
    if (this.acao !== 'CONSULTAR') {
      this.filterEdicao.enable()
    }
  }

  setFormGroupTOModel(model: T): T {
    for (let key in this.filterEdicao.controls) {
      model[key] = this.filterEdicao.controls[key].value
    }
    return model
  }
}
