import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef
} from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import swal from 'sweetalert'

import { IBaseService } from '../../../framework/service/IBaseService'
import { BaseService } from '../../../framework/service/base.service'
import { LoginService } from '../../../framework/service/login.service'
import { User } from '../../../framework/model/user.model'

@Component({
  selector: 'app-tabelagridinline',
  templateUrl: './tabela-gridinline.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})
export class TabelaGridInlineComponent<T> implements OnInit {
  @ViewChild('areaValidacao') areaValidacao: ElementRef

  public listaSituacao: Array<any> = [
    { situ_tx_situacao: '', tx_situacao: 'Todos' },
    { situ_tx_situacao: 'A', tx_situacao: 'Ativo' },
    { situ_tx_situacao: 'I', tx_situacao: 'Inativo' }
  ]
  public user: User
  public position = 'top-right'
  public lista: T[] = []
  public modelEdit: T = {} as T
  public paginacao = {
    totalItens: 0,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }
  public acao = 'CONSULTAR'

  public tituloPagina: string
  filterForm: FormGroup
  filterEdicao: FormGroup

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastr: ToastrService,
    public loginService: LoginService,
    public service: BaseService<T>
  ) {}

  ngOnInit() {
    this.user = this.loginService.user
    this.getDados(0)
  }

  setInsert() {
    this.modelEdit = {} as T
  }

  setEdit(model: T) {
    this.getModel(model)
  }

  setCancel() {
    this.acao = 'CONSULTAR'
    this.modelEdit = {} as T
    this.getDados(this.paginacao.paginaCorrente)
  }

  getPage() {
    this.getDados(this.paginacao.paginaCorrente)
  }

  pesquisaDados() {
    this.paginacao.paginaCorrente = 0;
  }

  getDados(pagina) {
    (this.service as IBaseService)
      .getFilter(this.filterForm.value, this.paginacao.paginaCorrente, this.paginacao.itensPorPagina)
      .subscribe(retorno => {
        this.lista = retorno.dados
        if ( retorno.total_registro ) {
          this.paginacao.totalItens = retorno.total_registro
        }

      })
  }


  ativaInativa(item) {

    const titulo = `Deseja ${item.situ_tx_situacao === 'A' ? 'Inativar' : 'Ativar' } o Registro ?`
    swal({
      title: titulo,
      text: '',
      icon: 'warning',
      buttons: {
        nao: {
          text: 'Não',
          value: false
        },
        ok: {
          text: 'Sim',
          value: true,
          closeModal: false
        }
      },
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this.service.delete(this.getId(item), this.user.id_usuario).subscribe(
          retorno => {
            this.toastr.success(retorno.mensagem, 'Sucesso!')
            this.setCancel();
            swal.stopLoading()

            swal.close('cancel')
          },
          response => {
            this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
          }
        )
      }
    });
  }

  saveDados() {
    // [disabled]='!filterEdicao.valid'

    if (!this.filterEdicao.valid) {
      this.toastr.error(this.areaValidacao.nativeElement.innerHTML, 'Erro!')
      return false
    }

    const modelSend: T = this.setFormGroupTOModel(this.modelEdit)
    if (this.acao !== 'INCLUIR') {
      this.service.put(this.getId(modelSend), modelSend).subscribe(
        retorno => {
          this.setCancel()
          this.toastr.success(retorno.mensagem, 'Sucesso!')
        },
        response => {
          this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
        }
      )
    } else {
      this.service.post(modelSend).subscribe(
        retorno => {
          this.setCancel()
          this.toastr.success(retorno.mensagem, 'Sucesso!')
        },
        response => {
          this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
        }
      )
    }
  }

  onTratarModelEditPosGet() {}

  getId(model: T): number {
    return -1
  }

  getModel(model: T) {
    this.service.get(this.getId(model)).subscribe(retorno => {
      this.modelEdit = retorno.dados
      this.onTratarModelEditPosGet()
      this.setFormGroup(this.modelEdit)
    })
  }

  gotoBack() {
    window.history.back()
  }

  setFormGroup(model: T) {
    for (let key in this.filterEdicao.controls) {
      this.filterEdicao.controls[key].setValue(model[key])
    }
  }

  setFormGroupTOModel(model: T): T {
    for (let key in this.filterEdicao.controls) {
      model[key] = this.filterEdicao.controls[key].value
    }
    return model
  }
}
