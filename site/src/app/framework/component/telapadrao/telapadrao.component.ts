import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef
} from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

import { IBaseService } from '../../../framework/service/IBaseService'
import { BaseService } from '../../../framework/service/base.service'
import { LoginService } from '../../../framework/service/login.service'
import { User } from '../../../framework/model/user.model'

@Component({
  selector: 'app-telapadrao',
  templateUrl: './telapadrao.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})
export class TelaPadraoComponent<T> implements OnInit {
  @ViewChild('areaValidacao') areaValidacao: ElementRef

  public listaSituacao: Array<any> = [
    { situacao: '', tx_situacao: 'Todos' },
    { situacao: 'A', tx_situacao: 'Ativo' },
    { situacao: 'I', tx_situacao: 'Inativo' }
  ]

  public nameRouter: String = ''
  public user: User
  public position = 'top-right'
  public lista: T[] = []
  public modelEdit: T = {} as T
  public paginacao = {
    totalItens: 30,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }
  public acao = 'CONSULTAR'

  public tituloPagina: string
  filterForm: FormGroup
  filterEdicao: FormGroup

  constructor(
    public _router: Router,
    public activatedRoute: ActivatedRoute,
    public http: HttpClient,
    private toastr: ToastrService,
    public loginService: LoginService,
    public service: BaseService<T>
  ) {
    this.nameRouter = activatedRoute.snapshot.firstChild.routeConfig.path
  }

  setNovo() {}

  ngOnInit() {
    this.filterEdicao.disable()
    this.user = this.loginService.user
    if (this.nameRouter.toLowerCase() === 'new') {
      this.acao = 'INCLUIR'
      this.setNovo()
      this.filterEdicao.enable()
    } else {
      if (this.nameRouter.toLowerCase() === 'edit') {
        this.acao = 'ALTERAR'
      }
      this.getModel(this.modelEdit)
    }

    // if ( this.acao !== 'CONSULTAR') {
    //  this.filterEdicao.enable()
    // }
  }

  setInsert() {
    this.modelEdit = {} as T
  }

  setEdit(model: T) {
    this.getModel(model)
  }

  setCancel() {
    this.acao = 'CONSULTAR'
    this.modelEdit = {} as T
    this.gotoBack()
  }

  getPage(page: number) {
    this.getDados(page)
  }

  getDados(pagina) {
    (this.service as IBaseService)
      .getFilter(this.filterForm.value, 1, 10)
      .subscribe(retorno => {
        this.lista = retorno.dados
      })
  }

  saveDados() {
    if (!this.filterEdicao.valid) {
      this.toastr.error(this.areaValidacao.nativeElement.innerHTML, 'Erro!')
      return false
    }

    const modelSend: T = this.setFormGroupTOModel(this.modelEdit)
    if (this.acao !== 'INCLUIR') {
      this.service.put(this.getId(modelSend), modelSend).subscribe(
        retorno => {
          this.setCancel()
          this.toastr.success(retorno.mensagem, 'Sucesso!')
        },
        response => {
          console.log(response)
          this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
        }
      )
    } else {
      this.service.post(modelSend).subscribe(
        retorno => {
          this.setCancel()
          this.toastr.success(retorno.mensagem, 'Sucesso!')
        },
        response => {
          console.log(response)
          this.toastr.error(JSON.parse(response.error).mensagem, 'Erro!')
        }
      )
    }
  }

  onTratarModelEditPosGet() {}

  getId(model: T): number {
    return -1
  }

  getModel(model: T) {
    this.service.get(this.getId(model)).subscribe(retorno => {
      this.modelEdit = retorno.dados
      this.onTratarModelEditPosGet()
      this.setFormGroup(this.modelEdit)
      if (this.acao !== 'CONSULTAR') {
        this.filterEdicao.enable()
      }
    })
  }

  gotoBack() {
    window.history.back()
  }

  setFormGroup(model: T) {
    for (let key in this.filterEdicao.controls) {
      this.filterEdicao.controls[key].setValue(model[key])
    }
  }

  setFormGroupTOModel(model: T): T {
    for (let key in this.filterEdicao.controls) {
      model[key] = this.filterEdicao.controls[key].value
    }
    return model
  }
}
