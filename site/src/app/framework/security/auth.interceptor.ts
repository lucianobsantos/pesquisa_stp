import { Injectable, Injector } from '@angular/core'
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
  HttpHandler,
  HttpEvent
} from '@angular/common/http'
import { Observable } from 'rxjs/Observable'

import { LoginService } from './../service/login.service'
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private injector: Injector,  private toastr: ToastrService ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const loginService = this.injector.get(LoginService)
    if (loginService.isLoggedIn()) {
      const authRequest = request.clone({
        setHeaders: { Authorization: `Bearer ${loginService.user.accessToken}` }
      })
      return next.handle(authRequest)
    } else {
      return next.handle(request).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {

          // do stuff with response if you want
        }
      }, (err: any) => {
        this.toastr.error(  err.error.mensagem, 'Erro!')
        if (err instanceof HttpErrorResponse ) {

          if (err.status === 401) {
            // redirect to the login route
            // or show a modal
          }
        }
      });
    }
  }
}
