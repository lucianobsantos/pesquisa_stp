import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Router, NavigationEnd} from '@angular/router'

import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/filter'

import {PCODE_API} from '../app.api'
import {LOCSTORE_U} from '../app.api'
import {User} from './../model/user.model'

@Injectable()
export class LoginService {

  user: User
  lastUrl: string

  constructor(private http: HttpClient, private router: Router) {
    this.router.events.filter(e => e instanceof NavigationEnd)
                      .subscribe( (e: NavigationEnd) => this.lastUrl = e.url)
  }

  isLoggedIn(): boolean {

    let token: any = null
    if ( this.user === undefined ) {
         token  = JSON.parse(localStorage.getItem( LOCSTORE_U))
        if ( token !== null ) {
           this.user = token
        }
    }

    return this.user !== undefined
  }

  login(empresa: string, email: string, password: string): Observable<User> {
    /*
    return this.http.post<User>(`${PCODE_API}/login`,
                          {email: email, password: password, empresa: empresa})
                    .do(user =>  this.user = user)*/

    // login sem empresa
    return this.http.post<User>(`${PCODE_API}/login`,
                    {email: email, password: password})
              .do(user =>  this.user = user)

  }

  logout() {
     this.user = {} as User
     localStorage.clear()
    this.router.navigate(['authentication/login'])
   }

  handleLogin(path: string = this.lastUrl) {
    localStorage.clear()
    this.router.navigate(['authentication/login'])
  }

}
