import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Area } from '../model/area.model'

@Injectable()
export class AreaService extends BaseService<Area>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'area';
  }
}
