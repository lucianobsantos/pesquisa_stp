import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Pesquisaresposta } from '../model/pesquisaresposta.model'

@Injectable()
export class PesquisarespostaService extends BaseService<Pesquisaresposta>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'pesquisa_resposta';
  }
}
