import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Graduacao } from '../model/graduacao.model'

@Injectable()
export class GraduacaoService extends BaseService<Graduacao>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'graduacao';
  }
}
