import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/filter'
import { PCODE_API } from '../app.api'

import { FilterService } from './../service/filter.service'
import { Desempenho } from '../model/desempenho.model'
import { Principal } from '../model/principal.model'
import { Estoque } from '../model/estoque.model'
import { Retorno } from '../model/retorno.model'
import { ItemVenda } from '../model/itemvenda.model'
import { LoginService } from './login.service'
import { ResumoVenda } from '../model/resumovendas.model';

@Injectable()
export class CardIndicadorService {
  constructor(
    private http: HttpClient,
    public filterService: FilterService,
    private loginService: LoginService
  ) {}

  getTop3(): Observable<Retorno<Desempenho[]>> {
    const parametros = this.filterService.getfiltroPadrao()
    let params: URLSearchParams = new URLSearchParams()
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)

    return this.http.get<Retorno<Desempenho[]>>(
      `${PCODE_API}/indicador/top3?${params.toString()}`,
      {}
    )
  }

  getIndicadores(): Observable<Retorno<Principal[]>> {
    var model = {
      tipoquery: 'card',
      id: null,
      id_usuario: this.loginService.user.id_usuario
    }
    return this.http.get<Retorno<Principal[]>>(
      `${PCODE_API}/indicador/cardprincipal?${this.montaQuery(model)}`,
      {}
    )
  }

  getIndicadorFaturamento(model): Observable<Retorno<Principal[]>> {
    let params: URLSearchParams = new URLSearchParams()
    const parametros = this.filterService.getfiltroPadrao()
    params.set('id', model.id)
    params.set('tipoquery', model.tipoquery)
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('id_empresa', parametros.empresasfiltro.join())
    params.set('id_departamento', parametros.departamentosfiltro.join())
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)
    params.set('qtd_meses', '1')
    params.set('id_consulta', model.id_consulta)


    return this.http.get<Retorno<Principal[]>>(
      `${PCODE_API}/indicador/CardFaturamento?${params.toString()}`,
      {}
    )
  }

  getRank(model): Observable<Retorno<Principal[]>> {
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_consulta', model.id_consulta)
    params.set('tabela', model.tabela)
    params.set('mes', model.mes.toString())
    params.set('todos', model.todos)
    params.set('visao', model.visao)
    params.set('id_empresa', model.id_empresa)
    params.set('id_departamento', model.id_departamento)
    if ( model.dtinicio ) {
      params.set('dtinicio',  model.dtinicio)
    }

       if ( model.dtfinal ) {
        params.set('dtfinal', model.dtfinal)
       }


    return this.http.get<Retorno<Principal[]>>(
      `${PCODE_API}/indicador/card01rank?${params.toString()}`,
      {}
    )
  }


  getResumo(model): Observable<Retorno<ResumoVenda[]>> {
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_consulta', model.id_consulta)
    params.set('mes', model.mes.toString())
    return this.http.get<Retorno<ResumoVenda[]>>(
      `${PCODE_API}/indicador/resumoVendas?${params.toString()}`,
      {}
    )
  }

  resumoVendasProdServ(model): Observable<Retorno<ResumoVenda[]>> {
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_consulta', model.id_consulta)
    params.set('mes', model.mes.toString())
    if ( model.vendedor ) {
      params.set('vendedor', model.vendedor.toString())
    }
    if ( model.cnpj_empresa ) {
      params.set('cnpj_empresa', model.cnpj_empresa.toString())
    }

    params.set('tipo_item', model.tipo_item.toString())
    return this.http.get<Retorno<ResumoVenda[]>>(
      `${PCODE_API}/indicador/resumoVendasProdServ?${params.toString()}`,
      {}
    )
  }

  desempenho(model): Observable<Retorno<ResumoVenda[]>> {
    let params: URLSearchParams = new URLSearchParams()

    if ( model.vendedor ) {
      params.set('vendedor', model.vendedor.toString())
    }
    if ( model.cnpj_empresa ) {
      params.set('cnpj_empresa', model.cnpj_empresa.toString())
    }

    params.set('tipo_item', model.tipo_item.toString())
    return this.http.get<Retorno<ResumoVenda[]>>(
      `${PCODE_API}/indicador/desempenho?${params.toString()}`,
      {}
    )
  }

  desempenhoGeral(model): Observable<Retorno<ResumoVenda[]>> {
    let params: URLSearchParams = new URLSearchParams()

    //if ( model.vendedor ) {
   //   params.set('vendedor', model.vendedor.toString())
   // }
    if ( model.cnpj_empresa ) {
      params.set('cnpj_empresa', model.cnpj_empresa.toString())
    }

    params.set('tipo_item', model.tipo_item.toString())
    return this.http.get<Retorno<ResumoVenda[]>>(
      `${PCODE_API}/indicador/desempenhogeral?${params.toString()}`,
      {}
    )
  }



  getcard_01_grafico_diario(model): Observable<Retorno<Principal[]>> {
    const parametros = this.filterService.getfiltroPadrao()
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_consulta', model.id_consulta)
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('visao', model.visao)

    return this.http.get<Retorno<Principal[]>>(
      `${PCODE_API}/indicador/card_01_grafico_diario?${params.toString()}`,
      {}
    )
  }

  getcard_01Relatorio(model): Observable<any> {
    let params: URLSearchParams = new URLSearchParams()
    const parametros = this.filterService.getfiltroPadrao()
    params.set('id', model.id)
    params.set('tipoquery', model.tipoquery)
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('id_empresa', parametros.empresasfiltro.join())
    params.set('id_departamento', parametros.departamentosfiltro.join())
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)
    params.set('qtd_meses', model.meses)
  //  params.set('id_consulta', model.id_consulta)
   console.log(model )
    return this.http.get<any>(
      `${PCODE_API}/indicador/card_01_Relatorio/?${params.toString()}`,
      {}
    )
  }

  getIndicadorEstoqueVeiculo(): Observable<Retorno<Estoque[]>> {
    const parametros = this.filterService.getfiltroPadrao()
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_empresa', parametros.empresasfiltro.join())
    params.set('id_departamento', parametros.departamentosfiltro.join())
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)

    return this.http.get<Retorno<Estoque[]>>(
      `${PCODE_API}/indicador/estoqueveiculo/?${params.toString()}`,
      {}
    )
  }

  getIndicadorEstoquePecas(): Observable<Retorno<Estoque[]>> {
    const parametros = this.filterService.getfiltroPadrao()
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_empresa', parametros.empresasfiltro.join())
    params.set('id_departamento', parametros.departamentosfiltro.join())
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)

    return this.http.get<Retorno<Estoque[]>>(
      `${PCODE_API}/indicador/estoquepecas/?${params.toString()}`,
      {}
    )
  }

  montaQuery(model): string {
    const parametros = this.filterService.getfiltroPadrao()
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_empresa', parametros.empresasfiltro.join())
    params.set('id_departamento', parametros.departamentosfiltro.join())
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('id', model.id)
    params.set('tipoquery', model.tipoquery)
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)
    return params.toString()
  }

  montaQueryCustom(model): string {
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_empresa', model.id_empresa)
    params.set('id_departamento', model.id_departamento)
    params.set('database', model.database)
    params.set('id', model.id)
    params.set('tipoquery', model.tipoquery)
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)
    return params.toString()
  }
  getRelatorio(model): Observable<any> {
    return this.http.get<any>(
      `${PCODE_API}/indicador/Relatorio/?${this.montaQuery(model)}`,
      {}
    )
  }

  getRelatorioCustom(model): Observable<any> {
    return this.http.get<any>(
      `${PCODE_API}/indicador/Relatorio/?${this.montaQueryCustom(model)}`,
      {}
    )
  }

  getGrafico(model): Observable<any> {
    return this.http.get<any>(
      `${PCODE_API}/indicador/Grafico/?${this.montaQuery(model)}`,
      {}
    )
  }


  getAnalitico(model): Observable<Retorno<ItemVenda[]>> {
    const parametros = this.filterService.getfiltroPadrao()
    let params: URLSearchParams = new URLSearchParams()
    params.set('id_consulta', model.id_consulta)
    params.set('database', `${'01/' + parametros.periodo}`)
    params.set('id_usuario', `${this.loginService.user.id_usuario}`)

    console.log(   `${PCODE_API}/indicador/analitico/?${params.toString()}` )
    return this.http.get<Retorno<ItemVenda[]>>(
      `${PCODE_API}/indicador/analitico/?${params.toString()}`,
      {}
    )
  }

}
