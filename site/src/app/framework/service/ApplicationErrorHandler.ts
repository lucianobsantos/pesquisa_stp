import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core'
import { HttpErrorResponse } from '@angular/common/http'
import 'rxjs/add/observable/throw'

import { LoginService } from './../service/login.service'

@Injectable()
export class ApplicationErrorHandler extends ErrorHandler {
  constructor(private injector: Injector, private zone: NgZone) {
    super()
  }

  handleError(errorResponse: HttpErrorResponse | any) {

    if (errorResponse instanceof HttpErrorResponse) {

        const message = errorResponse.message
      this.zone.run(() => {
        switch (errorResponse.status) {
          case 401:
              this.injector.get(LoginService).handleLogin()
            break
          case 403:
          this.injector.get(LoginService).handleLogin()
             console.log(message || 'Não autorizado.')
            break
          case 404:
            console.log(
              message ||
                'Recurso não encontrado. Verifique o console para mais detalhes'
            )
            break
        }
      })
    }
    super.handleError(errorResponse)
  }
}
