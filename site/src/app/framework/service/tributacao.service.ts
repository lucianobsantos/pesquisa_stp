import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Tributacao } from '../model/tributacao.model'

@Injectable()
export class TributacaoService extends BaseService<Tributacao>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'tributacao';
  }
}
