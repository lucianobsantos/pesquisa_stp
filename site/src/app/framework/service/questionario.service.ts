import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Questionario } from '../model/questionario.model'

@Injectable()
export class QuestionarioService extends BaseService<Questionario>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'questionario';
  }
}
