import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Retorno } from '../model/retorno.model'
import { BIEMPRESA } from '../model/biempresa.model'
import { Observable } from 'rxjs';

import { PCODE_API } from '../app.api'

@Injectable()
export class BIEMPRESAService extends BaseService<BIEMPRESA>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'bi_empresa';
  }

  getFilter(model: any, paginaCorrente: number, qtdItens: number): Observable<Retorno<any[]>> {
    return this.http.get<Retorno<any[]>>(
      `${PCODE_API}/bi_empresa/combo/?pag_c=1&qtd_i=100&id_usuario=${model.id_usuario}`,
      {}
    )
  }
}
