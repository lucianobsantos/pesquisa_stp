import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Resposta } from '../model/resposta.model'

@Injectable()
export class RespostaService extends BaseService<Resposta>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'resposta';
  }
}
