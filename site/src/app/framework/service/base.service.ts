import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
// import { Observable } from 'rxjs/Observable'
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/filter'

import { PCODE_API } from '../app.api'
import { Retorno } from '../model/retorno.model'
import { IBaseService } from './IBaseService'

@Injectable()
export class BaseService<T> implements IBaseService {
  public urlApi = ''
  constructor(public http: HttpClient) {}

  get(id: number): Observable<Retorno<T>> {
    return this.http.get<Retorno<T>>(`${PCODE_API}/${this.urlApi}/${id}`)
  }

  getFilter(model: any, paginaCorrente: number, qtdItens: number): Observable<Retorno<T[]>> {
    return this.http.get<Retorno<T[]>>(
      `${PCODE_API}/${this.urlApi}/?pag_c=${paginaCorrente}&qtd_i=${qtdItens}${this.montaQuery(model)}`,
      {}
    )
  }

  put(id: any, model: any): Observable<Retorno<T>> {
    return this.http
      .put<Retorno<T>>(`${PCODE_API}/${this.urlApi}/${id}`, model)
      .catch((err: Response) => {
        return Observable.throw(err)
      })
  }

  post(model: any): Observable<Retorno<T>> {
    return this.http.post<Retorno<T>>(`${PCODE_API}/${this.urlApi}/`, model)
  }

  delete(id: number, idUsuario: number): Observable<Retorno<T>> {
    return this.http.delete<Retorno<T>>(`${PCODE_API}/${this.urlApi}/${id}/${idUsuario}`)
  }

  montaQuery(model): string {
    let _queryString = '&'

    for (const key in model) {
      if (model[key] !== '') {
        if (model[key]) {
          if (typeof model[key].getMonth === 'function') {
            _queryString +=
              key + '=' + encodeURIComponent(model[key].toISOString()) + '&'
          } else {
            _queryString += key + '=' + encodeURIComponent(model[key]) + '&'
          }
        }
      }
    }
    _queryString = _queryString.substring(0, _queryString.length - 1)

    return _queryString
  }
}
