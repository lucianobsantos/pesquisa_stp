import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Usuario } from '../model/usuario.model'
import { Observable } from 'rxjs';
import { Retorno } from '../model/retorno.model';
import { UsuarioAcesso } from '../model/usuarioacesso.model';
import { PCODE_API } from '../app.api';

@Injectable()
export class UsuarioService extends BaseService<Usuario>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'usuario';
  }


  getUsuarioAcesso(id: number, tipo: string): Observable<Retorno<UsuarioAcesso[]>> {
    return this.http.get<Retorno<UsuarioAcesso[]>>(`${PCODE_API}/usuario/${id}/${tipo}`)
  }


  esqueciSenha(model: any): Observable<Retorno<Usuario[]>> {
    return this.http.get<Retorno<Usuario[]>>(
      `${PCODE_API}/usuario/esquecisenha/?${this.montaQuery(model)}`,
      {}
    )
  }

  validatoken(model: any): Observable<Retorno<Usuario>> {
    return this.http.get<Retorno<Usuario>>(
      `${PCODE_API}/usuario/validatoken/?${this.montaQuery(model)}`,
      {}
    )
  }


  alterasenha(model: any): Observable<Retorno<Usuario>> {
    return this.http.get<Retorno<Usuario>>(
      `${PCODE_API}/usuario/alterasenha/?${this.montaQuery(model)}`,
      {}
    )
  }

}
