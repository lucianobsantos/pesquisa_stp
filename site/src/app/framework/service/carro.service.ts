import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Carro } from '../model/carro.model';
import { Observable } from 'rxjs';
import { Retorno } from '../model/retorno.model';
import { Modelo } from '../model/modelo.model';
import { PCODE_API } from '../app.api';

@Injectable()
export class CarroService extends BaseService<Carro>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'carro';
  }


  getModelo(id_carro: number, paginaCorrente: number, qtdItens: number): Observable<Retorno<Modelo[]>> {
    return this.http.get<Retorno<Modelo[]>>(
      `${PCODE_API}/${this.urlApi}/${id_carro}/modelo?pag_c=${paginaCorrente}&qtd_i=${qtdItens}`,
      {}
    )
  }

  getRevisao(id_carro: number, id_modelo: number,  paginaCorrente: number, qtdItens: number): Observable<Retorno<Modelo[]>> {
    return this.http.get<Retorno<Modelo[]>>(
      `${PCODE_API}/${this.urlApi}/${id_carro}/modelo/${id_modelo}/revisao?pag_c=${paginaCorrente}&qtd_i=${qtdItens}`,
      {}
    )
  }
}
