import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Consultorespecialidade } from '../model/consultorespecialidade.model'

@Injectable()
export class ConsultorespecialidadeService extends BaseService<Consultorespecialidade>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'consultor_especialidade';
  }
}
