import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
// import { Usuario } from '../model/usuario.model'
import { Observable } from 'rxjs/Rx'

import { PCODE_API } from '../app.api'
import { Retorno } from '../model/retorno.model'
import { Dashboardgeral } from '../model/dashboardgeral.model'

@Injectable()
export class DashboardGeralService extends BaseService<Dashboardgeral> {
  constructor(http: HttpClient) {
    super(http)
    this.urlApi = 'dashboard'
  }

  getDadosGeral(model: any): Observable<Retorno<Dashboardgeral[]>> {
    // console.log(  model )
    return this.http.get<Retorno<Dashboardgeral[]>>(
      `${PCODE_API}/${this.urlApi}/dados_geral`,
      {}
    )
  }

/*
  getGeralPMDE(model: any): Observable<Retorno<Dashboardgeral>> {
    // console.log(  model )
    return this.http.get<Retorno<Dashboardgeral>>(
      `${PCODE_API}/${this.urlApi}/pmde/?${this.montaQuery(model)}`,
      {}
    )
  }

  getDetalhePMDE(model: any): Observable<Retorno<Dashboardgeral[]>> {
    return this.http.get<Retorno<Dashboardgeral[]>>(
      `${PCODE_API}/${this.urlApi}/detalhepmde/?${this.montaQuery(model)}`,
      {}
    )
  }
  */

}
