import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { BIDEPARTAMENTO } from '../model/bidepartamento.model'
import { Observable } from 'rxjs';
import { Retorno } from '../model/retorno.model';
import { PCODE_API } from '../app.api';

@Injectable()
export class BIDEPARTAMENTOService extends BaseService<BIDEPARTAMENTO>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'bi_departamento';
  }

  getFilter(model: any, paginaCorrente: number, qtdItens: number): Observable<Retorno<any[]>> {
    return this.http.get<Retorno<any[]>>(
      `${PCODE_API}/bi_departamento/combo/?pag_c=1&qtd_i=100&id_usuario=${model.id_usuario}`,
      {}
    )
  }
}
