import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Observable } from 'rxjs';
import { Retorno } from '../model/retorno.model';
import { Modelo } from '../model/modelo.model';
import { PCODE_API } from '../app.api';
import { PCODE_API_DEALER } from '../app.api';
import { Grupo_Modelo } from '../model/grupo_modelo';

@Injectable()
export class Grupo_ModeloService extends BaseService<Grupo_Modelo>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'grupo_modelo';
  }


  getGrupoModelo(idgrupomodelo: number, paginaCorrente: number, qtdItens: number): Observable<Retorno<Modelo[]>> {
    return this.http.get<Retorno<Modelo[]>>(
      `${PCODE_API}/${this.urlApi}/${idgrupomodelo}/modelo?pag_c=${paginaCorrente}&qtd_i=${qtdItens}`,
      {}
    )
  }
 
  getRevisao(  idModelo: string, cnpj_empresa: string,   paginaCorrente: number, qtdItens: number): Observable<Retorno<Modelo[]>> {
    return this.http.get<Retorno<Modelo[]>>(
      `${PCODE_API}/modelo/${idModelo}/revisao/?cnpj_empresa=${cnpj_empresa}&pag_c=${paginaCorrente}&qtd_i=${qtdItens}`,
      {}
    )
  } 

  getItensRevisao(  idModelo: string, cnpj_empresa: string,   paginaCorrente: number, qtdItens: number): Observable<Retorno<Modelo[]>> {
    return this.http.get<Retorno<Modelo[]>>(
      `${PCODE_API}/revisao/${idModelo}/itens/?cnpj_empresa=${cnpj_empresa}&pag_c=${paginaCorrente}&qtd_i=${qtdItens}`,
      {}
    )
  } 
}
