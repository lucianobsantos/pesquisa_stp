import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Consultorgraduacao } from '../model/consultorgraduacao.model'

@Injectable()
export class ConsultorgraduacaoService extends BaseService<Consultorgraduacao>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'consultor_graduacao';
  }
}
