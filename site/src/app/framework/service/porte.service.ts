import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Porte } from '../model/porte.model'

@Injectable()
export class PorteService extends BaseService<Porte>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'porte';
  }
}
