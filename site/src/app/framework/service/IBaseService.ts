export interface IBaseService {
  get(id: number): any

  getFilter(model: any, paginaCorrente: number, qtdItens: number): any

  put(id: any, model: any): any
  post(   model: any): any
}
