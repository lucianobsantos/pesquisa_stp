import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Retorno } from '../model/retorno.model'
import { BIGRUPO } from '../model/bigrupo.model'
import { Observable } from 'rxjs';

import { PCODE_API } from '../app.api'

@Injectable()
export class BIGRUPOService extends BaseService<BIGRUPO>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'bi_grupo';
  }


  getGrupo(nome: string): Observable<Retorno<BIGRUPO>> {
    return this.http.get<Retorno<BIGRUPO>>(`${PCODE_API}/${this.urlApi}/${nome}`)
  }


}
