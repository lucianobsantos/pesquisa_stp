import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Consultor } from '../model/consultor.model'

@Injectable()
export class ConsultorService extends BaseService<Consultor>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'consultor';
  }
}
