import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Observable } from 'rxjs';
import { Retorno } from '../model/retorno.model';
import { Modelo } from '../model/modelo.model';
import { PCODE_API } from '../app.api';
import { Grupo_Modelo } from '../model/grupo_modelo';
import { Revisao_cliente } from '../model/revisao_cliente';

@Injectable()
export class Revisao_clienteService extends BaseService<Revisao_cliente>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'revisao_cliente';
  }
 
}
