import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Consultorconsultoria } from '../model/consultorconsultoria.model'

@Injectable()
export class ConsultorconsultoriaService extends BaseService<Consultorconsultoria>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'consultor_consultoria';
  }
}
