import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { Subject } from 'rxjs/Subject'
import { FiltroPadrao } from './../model/filtropadrao.model'
import { BIGRUPO } from './../model/bigrupo.model'
import { LOCSTORE_F } from './../app.api'
import { BIEMPRESA } from '../model/biempresa.model';

@Injectable()
export class FilterService {

  private subject = new Subject<FiltroPadrao>()

  dadosGrupo: any
  filtroPadrao: FiltroPadrao
  biGrupo: BIGRUPO
  empresaSelecionado: BIEMPRESA;

  constructor() {

    this.dadosGrupo = JSON.parse(localStorage.getItem(LOCSTORE_F)) as {}
    if (this.dadosGrupo) {
      this.biGrupo = this.dadosGrupo.biGrupo
      this.filtroPadrao = this.dadosGrupo.filtroPadrao
      this.empresaSelecionado = this.dadosGrupo.empresaSelecionado;
    } else  {

      const data = new Date()
      const periodo =
        this.padLeft((data.getMonth() + 1).toString(), '0', 2) +
        '/' +
        data.getFullYear().toString()
      this.filtroPadrao = {
        periodo: periodo,
        departamentosfiltro: [],
        empresasfiltro: []
      }
      this.biGrupo = {
        nome: '',
        nome_Banco: '',
        descricao: ''
      }
      this.empresaSelecionado = {}
    }
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr(size * -1, size)
  }

  setBiEmpresaSelecionada(val: BIEMPRESA) {
    this.empresaSelecionado = val;
    localStorage.setItem(
      LOCSTORE_F,
      JSON.stringify({ filtroPadrao: this.filtroPadrao, biGrupo: this.biGrupo, empresaSelecionado: this.empresaSelecionado })
    )
    
  }
  
  getBiEmpresaSelecionada() {
    return this.empresaSelecionado; 
  }

  setBiGrupo(val: BIGRUPO) {
    this.biGrupo = val
    localStorage.setItem(
      LOCSTORE_F,
      JSON.stringify({ filtroPadrao: this.filtroPadrao, biGrupo: this.biGrupo, empresaSelecionado: this.empresaSelecionado })
    )
  }

  getBiGrupo() {
    return this.biGrupo
  }

  setfiltroPadrao(val: FiltroPadrao) {
    this.filtroPadrao = val
    localStorage.setItem(
      LOCSTORE_F,
      JSON.stringify({ filtroPadrao: this.filtroPadrao, biGrupo: this.biGrupo, empresaSelecionado: this.empresaSelecionado })
    )
  }

  getfiltroPadrao() {
    return this.filtroPadrao
  }

  sendMessage(message: FiltroPadrao) {
    this.filtroPadrao = message
    localStorage.setItem(
      LOCSTORE_F,
      JSON.stringify({ filtroPadrao: this.filtroPadrao, biGrupo: this.biGrupo,empresaSelecionado: this.empresaSelecionado })
    )

    this.subject.next(message)
  }

  clearMessage() {
    localStorage.setItem(LOCSTORE_F, JSON.stringify({}))

    this.subject.next()
  }

  getMessage(): Observable<FiltroPadrao> {
    return this.subject.asObservable()
  }
}
