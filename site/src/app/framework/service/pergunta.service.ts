import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BaseService } from './base.service'
import { Pergunta } from '../model/pergunta.model'

@Injectable()
export class PerguntaService extends BaseService<Pergunta>  {
  constructor(http: HttpClient) {
    super(http )
    this.urlApi = 'pergunta';
  }
}
