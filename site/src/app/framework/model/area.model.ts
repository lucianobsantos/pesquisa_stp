export interface Area {
  area_nr_sequencia?: number
  area_tx_descricao?: string
  situ_tx_situacao?: string
  area_dt_cadastro?: Date
  area_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  camposAuxiliares?: any
}