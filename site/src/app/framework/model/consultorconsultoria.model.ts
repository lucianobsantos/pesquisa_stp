export interface Consultorconsultoria {
  ccon_nr_sequencia?: number
  cons_nr_sequencia?: number
  area_nr_sequencia?: number
  ccon_tx_titulo?: string
  ccon_tx_descricao?: string
  ccon_nr_valor?: number
  situ_tx_situacao?: string
  ccon_dt_inicio?: Date
  ccon_dt_fim?: Date
  ccon_tx_palavrachave?: string
  ccon_tx_observacao?: string
  ccon_dt_cadastro?: Date
  ccon_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  camposAuxiliares?: any
}