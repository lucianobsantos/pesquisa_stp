export interface Graduacao {
  grad_nr_sequencia?: number
  grad_tx_descricao?: string
  situ_tx_situacao?: string
  grad_dt_cadastro?: Date
  grad_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  camposAuxiliares?: any
}