export interface Carro {
  identificador?: number
  descricao?: string
  situacao?: string
  camposAuxiliares?: any
}