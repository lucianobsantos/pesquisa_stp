export interface Tributacao {
  trib_nr_sequencia?: number
  trib_tx_descricao?: string
  situ_tx_situacao?: string
  trib_dt_cadastro?: Date
  trib_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  camposAuxiliares?: any
}