export interface Porte {
  port_nr_sequencia?: number
  port_tx_descricao?: string
  situ_tx_situacao?: string
  port_dt_cadastro?: Date
  port_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  camposAuxiliares?: any
}