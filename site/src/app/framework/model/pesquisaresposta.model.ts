export interface Pesquisaresposta {
  pere_nr_sequencia?: number
  ques_nr_sequencia?: number
  resp_nr_sequencia?: number
  pere_dt_resposta?: Date
  situ_tx_situacao?: string
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  data_dt_cadastro?: Date
  data_dt_edicao?: Date
  pere_tx_descricao?: string
  pere_tx_identificador?: string
  camposAuxiliares?: any
}