export interface Consultorespecialidade {
  coes_nr_sequencia?: number
  cons_nr_sequencia?: number
  area_nr_sequencia?: number
  coes_nr_pontuacao?: number
  situ_tx_situacao?: string
  cgra_dt_cadastro?: Date
  cgra_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  camposAuxiliares?: any
}