export interface Revisao_cliente {
    identificador?: number
    cnpj_empresa?: string
    placa?: string
    situacao?: string
    statusos?: string
    data?: string
    data_prevista?: string

    camposAuxiliares?: any
  }

 
  