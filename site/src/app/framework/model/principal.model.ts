export interface Principal {
  titulo: string,
  empresa: string,
  departamento: string,
  valor: number,
  valor_ant: number,
  valor_ant_dia: number,
  qtd: number,
  qtd_ant: number,
  meta: number,
  metadiaria: number,
  sentido: string,
  unidade: string,
  id_sequencia: number,
  ticket: number,
  ticket_ant: number,

  projecao_qtd: number,
  projecao_valor: number,

  camposAuxiliares: any[]
}
