export interface User {
    nome: string,
    apelido: string,
    email: string,
    id_usuario: number,
    accessToken: string,
    created: Date,
    expiration: Date
}
