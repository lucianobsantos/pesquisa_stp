export interface Pergunta {
  perg_nr_sequencia?: number
  ques_nr_sequencia?: number
  perg_tx_descricao?: string
  perg_tx_tipo?: string
  perg_nr_ordem?: number
  situ_tx_situacao?: string
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  data_dt_cadastro?: Date
  data_dt_edicao?: Date
  perg_nr_val_minimo?: number
  perg_nr_val_maximo?: number
  perg_tx_desc_minimo?: string
  perg_tx_desc_maximo?: string
  camposAuxiliares?: any
}