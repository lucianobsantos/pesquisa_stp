export interface ResumoVenda {
  ehtotal?: boolean
  expand?: boolean
  faturamento?: number
  nome?: string
  cnpj_empresa?: string
  passagens?: number
  quantidade_prod?: number
  quantidade_serv?: number
  ticket?: number
  tipo?: number
  valor_prod?: number
  valor_serv?: number
  vendedor?: string
  vendas?: ResumoVenda[]
  }
