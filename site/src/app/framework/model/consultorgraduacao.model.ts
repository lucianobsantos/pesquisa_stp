export interface Consultorgraduacao {
  cgra_nr_sequencia?: number
  cons_nr_sequencia?: number
  grad_nr_sequencia?: number
  cgra_tx_instituicao?: string
  situ_tx_situacao?: string
  cgra_dt_cadastro?: Date
  cgra_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  camposAuxiliares?: any
}