export interface Grupo_Modelo {
    identificador?: number
    descricao?: string
    situacao?: string
    imagem?: string
    camposAuxiliares?: any
  }