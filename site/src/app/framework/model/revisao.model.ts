export interface Revisao {
  identificador?: number
  descricao?: string
  situacao?: string
  id_carro?: number
  id_modelo?: number
  camposAuxiliares?: any
}