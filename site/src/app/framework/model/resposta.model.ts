export interface Resposta {
  resp_nr_sequencia?: number
  perg_nr_sequencia?: number
  resp_tx_descricao?: string
  resp_nr_ordem?: number
  situ_tx_situacao?: string
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  data_dt_cadastro?: Date
  data_dt_edicao?: Date
  resp_nr_peso?: number
  resp_tx_imagem?: string
  resp_tx_valor?: string
  camposAuxiliares?: any
}