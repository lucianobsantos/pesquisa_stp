export interface BIDEPARTAMENTO {
  id_departamento?: number
  nome?: string
  id_departamento_pai?: number
  camposAuxiliares?: any
}