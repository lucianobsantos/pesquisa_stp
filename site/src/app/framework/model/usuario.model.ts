export interface Usuario {
  id_usuario?: number
  nome?: string
  apelido?: string
  email?: string
  situacao?: string
  id_usuario_cadastro?: number
  id_usuario_edicao?: number
  dt_cadastro?: Date
  dt_edicao?: Date
  senha?: string
  camposAuxiliares?: any
}
