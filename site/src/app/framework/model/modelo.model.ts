export interface Modelo {
  id_sync_modelo?: number
  mod_cd?: string
  descricao_modelo?: string
  pad_cd?: number
  camposAuxiliares?: any
}