export interface Desempenho {
  title?: string
  subtitle?: string
  titletop1?: string
  valuetop1?: number
  titletop2?: string
  valuetop2?: number
  titletop3?: string
  valuetop3?: number
  color?: string
  icone?: string
}
