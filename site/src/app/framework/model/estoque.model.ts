export interface Estoque {
  titulo ?: string
  percentual ?: number
  valor ?: number
  valortotal ?: number
  meta ?: number
  camposAuxiliares ?: any[]
}
