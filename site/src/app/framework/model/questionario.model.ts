export interface Questionario {
  ques_nr_sequencia?: number
  ques_tx_descricao?: string
  ques_tx_cnpj_empresa?: string
  ques_tx_logo_empresa?: string
  ques_tx_texto_inicio?: string
  ques_tx_texto_fim?: string
  ques_tx_situacao?: string
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  data_dt_cadastro?: Date
  data_dt_edicao?: Date
  camposAuxiliares?: any
}