export interface BIEMPRESA {
  id_empresa?: number
  nome?: string
  razao_social?: string
  cnpj?: string
  insc_estadual?: string
  insc_municipal?: string
  endereco?: string
  endereco_nr?: string
  endereco_complemento?: string
  bairro?: string
  cidade?: string
  uf?: string
  cep?: string
  telefone?: string
  email?: string
  data_atualizacao?: Date
  latitude?: string
  longitude?: string
  data_inclusao?: Date
  sistema?: string
  servidor?: string
  banco_dados?: string
  usuario?: string
  senha?: string
  matriz?: string
  camposAuxiliares?: any
}