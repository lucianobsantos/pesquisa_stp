export interface ItemVenda {
  Data_hora_venda: Date;
  Departamento: string;
  Num_venda: string;
  Cliente: string;
  Vendedor: string;
  Grupo_produto: string;
  Produto: string;
  Forma_pagto: string;
  Banco: string;
  Valor_pago: number;
  Valor: number;
  Quantidade: number;
  Valor_unitario: number;
  Desconto_item: number;
  Acrescimo_item: number;
  Valor_total_item: number;
  Valor_impostos_item: number;
  Mes: number;

  camposAuxiliares: any[];
}
