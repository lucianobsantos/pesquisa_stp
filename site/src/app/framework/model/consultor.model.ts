export interface Consultor {
  cons_nr_sequencia?: number
  cons_tx_nome?: string
  cons_tx_apelido?: string
  cons_tx_cpf?: string
  cons_tx_rg?: string
  cons_tx_orgao_rg?: string
  cons_tx_sexo?: string
  cons_tx_telefone1?: string
  cons_tx_telefone2?: string
  cons_tx_celular?: string
  cons_tx_email?: string
  cons_tx_cep?: string
  cons_tx_logradouro?: string
  cons_tx_numero?: string
  cons_tx_bairro?: string
  cons_tx_complemento?: string
  muni_nr_sequencia?: number
  situ_tx_situacao?: string
  cons_dt_cadastro?: Date
  cons_dt_edicao?: Date
  usua_nr_cadastro?: number
  usua_nr_edicao?: number
  cons_tx_curriculo?: string
  camposAuxiliares?: any
}