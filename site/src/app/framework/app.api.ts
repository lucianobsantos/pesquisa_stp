import {environment} from '../../environments/environment';
export const PCODE_API = environment.api;
export const PCODE_API_DEALER = environment.api_dealernet;
export const LOCSTORE_U = 'U_BILINKIE' + btoa( window.location.hostname.split('.')[0] );
export const LOCSTORE_F = 'F_BILINKIE' + btoa( window.location.hostname.split('.')[0] );
