import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './framework/security/auth-guard.service'

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';

export const Approutes: Routes = [
  {
    canActivate :  [AuthGuard],
    path: '',
    component: FullComponent,
    children: [
      { path: '', redirectTo: '/questionario', pathMatch: 'full' },
      {
        path: 'starter',
        loadChildren: './starter/starter.module#StarterModule',
        canActivate  :  [AuthGuard]
      },
      {
        path: 'cadastro',
        loadChildren: './cadastro/cadastro.module#CadastroModule'
      },
      {
        path: 'seguranca',
        loadChildren: './seguranca/seguranca.module#SegurancaModule'
      },
      {
        path: 'questionario',
        loadChildren: './questionario/questionario.module#QuestionarioModule'
      },
      {
        path: 'dasboardpesquisa',
        loadChildren: './dashboard-pesquisa/dashboard_pesquisa.module#DashboardpesquisaModule'
      },
      {
        path: 'revisao',
        loadChildren: './revisao/revisao.module#RevisaoModule'
      },
      {
        path: 'colaborador',
        loadChildren: './colaborador/colaborador.module#ColaboradorModule'
      },
      {
        path: 'component',
        loadChildren: './component/component.module#ComponentsModule'
      }
    ]
  },
  {
    path: '',
    component: BlankComponent,
    children: [
      {
        path: 'authentication',
        loadChildren:
          './authentication/authentication.module#AuthenticationModule'
      }
    ]
  },
 {
    path: '**',
    redirectTo: '/starter'
  }
];
