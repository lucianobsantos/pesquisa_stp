import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { LoginService } from '../../framework/service/login.service'
import { BIGRUPOService } from '../../framework/service/bigrupo.service'
import { User } from '../../framework/model/user.model'
import { BIGRUPO } from '../../framework/model/bigrupo.model'
import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from '../../framework/service/usuario.service';
import { FilterService } from '../../framework/service/filter.service';
import { PCODE_API } from '../../framework/app.api';
import { LOCSTORE_U } from '../../framework/app.api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  empresa: string
  empresadesc: string
  site: string
  loginForm: FormGroup
  navigateTo: string
  bIGrupo: BIGRUPO

  isLogin = true;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private bigrupoService: BIGRUPOService,
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router,
    private filterService: FilterService
  ) {

    const empresaTemp = window.location.hostname.split('.')[0]

    this.empresa = ''
    this.empresadesc = ''

    this.site =  window.location.origin

    /*
    this.bigrupoService.getGrupo(empresaTemp).subscribe(retorno => {
      this.bIGrupo = retorno.dados
      this.empresa = this.bIGrupo.nome_Banco
      this.empresadesc = this.bIGrupo.descricao
      this.loginForm.controls['empresa'].setValue( this.empresa )
      this.loginForm.controls['empresadesc'].setValue( this.empresadesc )
      this.filterService.setBiGrupo( {nome: empresaTemp, nome_Banco: this.empresa, descricao: this.empresadesc} )
    })*/


  }

  ngOnInit() {

    this.loginForm = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      // empresadesc: new FormControl({value: this.empresadesc, disabled: true},  [ Validators.required] ),
      // empresa: new FormControl({value: this.empresa},  [ Validators.required] ),
      password: this.fb.control(null, [Validators.required])
    })
    this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/')
  }

  login() {
    this.loginService
      .login(null, this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(
        user => {
          localStorage.setItem(LOCSTORE_U, JSON.stringify(user))
          this.toastr.success(`Bem vindo, ${user.nome}`, 'Sucesso!');
        },
        response => {
          if ( response.error.message ) {
            this.toastr.error( response.error.message, 'Erro!')
          } else {
             this.toastr.error( response.message, 'Erro!')
          }
          localStorage.clear()
        },
        () => {
          this.router.navigate([atob(this.navigateTo)])
        }
      )
  }

  recuperarSenha() {
    this.isLogin = !this.isLogin
    if ( !this.isLogin ) {
      this.loginForm.controls['password'].setValidators([])
      this.loginForm.reset()
      // this.loginForm.controls['empresa'].setValue( this.empresa )
      // this.loginForm.controls['empresadesc'].setValue( this.empresadesc )
    }  else {
      this.loginForm.controls['password'].setValidators([Validators.required])
      this.loginForm.reset()
      // this.loginForm.controls['empresa'].setValue( this.empresa )
      // this.loginForm.controls['empresadesc'].setValue( this.empresadesc )
    }
  }


  enviarRecuperarSenha() {

    this.usuarioService
    .esqueciSenha({email : this.loginForm.value.email, 'camposAuxiliares.site' : this.site, empresa: this.empresa } )
    .subscribe(retorno => {
      this.isLogin = true
      this.router.navigate(['authentication/login'])

    },
      () => {
        this.isLogin = true
        this.router.navigate(['authentication/login'])
      }
    )
  }



}
