import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RecuperaComponent } from './recupera/recupera.component';


export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'recuperasenha/:token',
        component: RecuperaComponent
      }
    ]
  }
];
