import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { LoginService } from '../../framework/service/login.service'
import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from '../../framework/service/usuario.service';
import { Usuario } from '../../framework/model/usuario.model';
import { PCODE_API } from '../../framework/app.api';
import { LOCSTORE_U } from '../../framework/app.api';
import { PasswordValidation } from '../../framework/validator/PasswordValidation';
import { HttpErrorResponse } from '@angular/common/http';
import { BIGRUPOService } from '../../framework/service/bigrupo.service';
import { BIGRUPO } from '../../framework/model/bigrupo.model';
import { FilterService } from '../../framework/service/filter.service';

@Component({
  selector: 'app-recupera',
  templateUrl: './recupera.component.html',
  styleUrls: ['./recupera.component.css']
})
export class RecuperaComponent implements OnInit {

  empresa: string
  empresadesc: string
  token: string
  email: string
  site: string
  loginForm: FormGroup
  navigateTo: string
  bIGrupo: BIGRUPO


  isLogin = true;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private bigrupoService: BIGRUPOService,
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router,
    private filterService: FilterService

  ) {

    this.activatedRoute.params.subscribe(params => {
      this.token =  this.activatedRoute.snapshot.paramMap.get('token')
   });

   const empresaTemp = window.location.hostname.split('.')[0]
   this.empresa = ''
   this.empresadesc = ''

   this.site =  window.location.origin


   this.bigrupoService.getGrupo(empresaTemp).subscribe(retorno => {
     this.bIGrupo = retorno.dados
     this.empresa = this.bIGrupo.nome_Banco
     this.empresadesc = this.bIGrupo.descricao
     this.loginForm.controls['empresa'].setValue( this.empresa )
     this.loginForm.controls['empresadesc'].setValue( this.empresadesc )
     this.filterService.setBiGrupo( {nome: empresaTemp, nome_Banco: this.empresa, descricao: this.empresadesc} )

     this.usuarioService.validatoken({empresa: this.empresa, token: this.token})
     .subscribe(
       response => {
          this.email = response.dados.email
       },
       (retornoValida: any) => {
         if (retornoValida instanceof HttpErrorResponse ) {
         this.router.navigate(['authentication/login'])
         }
       }
     )

   })

 }

  ngOnInit() {





    this.loginForm = this.fb.group({
      empresadesc: new FormControl({value: this.empresadesc, disabled: true},  [ Validators.required] ),
      empresa: new FormControl({value: this.empresa},  [ Validators.required] ),
      password: this.fb.control(null, [Validators.required]),
      confirmPassword: this.fb.control(null, [Validators.required])
    }, {
      validator: PasswordValidation.MatchPassword // your validation method
    })
    this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/')
  }


  login() {
    this.usuarioService.alterasenha({ token: this.token, empresa: this.empresa,email: this.email, senha: this.loginForm.value.password })
      .subscribe(
       retorno => {
            if ( retorno.dados.camposAuxiliares.status === 1 ) {
              this.toastr.success(`Senha Alterada com Sucesso`, 'Sucesso!');
              this.loginService
              .login(this.empresa, this.email, this.loginForm.value.password)
              .subscribe(
                user => {
                  localStorage.setItem(LOCSTORE_U, JSON.stringify(user))
                  this.toastr.success(`Bem vindo, ${user.nome}`, 'Sucesso!');
                },
                response => {
                  if ( response.error.message ) {
                    this.toastr.error( response.error.message, 'Erro!')
                  } else {
                     this.toastr.error( response.message, 'Erro!')
                  }
                  localStorage.clear()
                },
                () => {
                  this.router.navigate([atob(this.navigateTo)])
                }
              )




            } else {
              this.toastr.success(`Erro na troca de senha `, 'Erro!');
            }

        },
        response => {
          console.log( response )
        },
        () => {

        //  this.router.navigate(['authentication/login'])
         // this.router.navigate([atob(this.navigateTo)])
        }
      )
  }



}
