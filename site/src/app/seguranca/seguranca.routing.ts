import { Routes } from '@angular/router';

import { UsuariolistaComponent } from './usuario/usuariolista.component';
import { UsuarioTelaComponent } from './usuario/usuariotela.component';

export const SegurancaRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'usuariolista',
        component: UsuariolistaComponent
      },
      {
        path: 'usuario/:id',
        component: UsuarioTelaComponent,
        children: [
          { path: '', redirectTo: 'detail', pathMatch: 'full' },
          { path: 'detail', component: UsuarioTelaComponent },
          { path: 'edit', component: UsuarioTelaComponent },
          { path: 'new', component: UsuarioTelaComponent }
        ]
      }
    ]
  }
];
