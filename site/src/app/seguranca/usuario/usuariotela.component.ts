import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

import { TelaPadraoComponent } from '../../framework/component/telapadrao/telapadrao.component'
import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'
import { LoginService } from '../../framework/service/login.service'
import { UsuarioAcesso } from '../../framework/model/usuarioacesso.model';


@Component({
  selector: 'app-usuariotela',
  templateUrl: './usuariotela.component.html',
  styleUrls: ['./usuariotela.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UsuarioTelaComponent extends TelaPadraoComponent<Usuario>
  implements OnInit   {

    listaEmpresas: UsuarioAcesso[]
    carregouEmpresa: Boolean = false
    listaDepartamentos: UsuarioAcesso[]
    carregouDepartamento: Boolean = false


  ngOnInit() {
    this.user = this.loginService.user
    if ( this.user.id_usuario !== 3 ) {
      this._router.navigate(['starter'])
   }

    super.ngOnInit()
    this.tituloPagina = 'Cadastro - Usuário'
  }

  setNovo() {
    this.modelEdit =  {
      id_usuario : -1,
      nome : null,
      apelido : null,
      email : null,
      senha : null,
      situacao : 'A',
      camposAuxiliares : {'tx_situacao':  'ATIVA'},
      id_usuario_cadastro : this.user.id_usuario,
      id_usuario_edicao   : this.user.id_usuario }

    this.setFormGroup(this.modelEdit)
    this.acao = 'INCLUIR'
    this.carregaEmpresa()
    this.carregaDepartamento()
  }

  onTratarModelEditPosGet() {
    this.modelEdit.id_usuario_edicao = this.user.id_usuario
    this.carregaEmpresa()
    this.carregaDepartamento()

  }

  carregaEmpresa() {
    this.service.getUsuarioAcesso(this.modelEdit.id_usuario, 'empresa')
    .subscribe(retorno => {
      this.listaEmpresas = retorno.dados
      for (const acesso of this.listaEmpresas) {
        this.filterEdicao.addControl(
          'id_empresa' + acesso.id_sequencia,
          new FormControl(acesso.marcado === 'S', [])
        )
      }

      this.carregouEmpresa = true
    })
  }

  carregaDepartamento() {
    this.service.getUsuarioAcesso(this.modelEdit.id_usuario, 'departamento')
    .subscribe(retorno => {
      this.listaDepartamentos = retorno.dados
      for (const acesso of this.listaDepartamentos) {
        this.filterEdicao.addControl(
          'id_departamento' + acesso.id_sequencia,
          new FormControl(acesso.marcado === 'S', [])
        )
      }

      this.carregouDepartamento = true
    })
  }


  constructor(
      public _router: Router,
    public activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public toastyService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    private fb: FormBuilder
  ) {
    super(_router, activatedRoute, http, toastyService , loginService, service)
    this.activatedRoute.params.subscribe(params => {
      this.modelEdit.id_usuario = +this.activatedRoute.snapshot.paramMap.get('id')
   });

    this.filterEdicao = this.fb.group({
      usua_nr_sequencia: this.fb.control('', [  ]),
      nome: this.fb.control('', [Validators.required, Validators.maxLength(100) ]),
      apelido: this.fb.control('', [Validators.required, Validators.maxLength(20) ]),
      email: this.fb.control('', [Validators.required, Validators.maxLength(200) ]),
      senha: this.fb.control('', []),
      situacao: this.fb.control('A', []),
    })
  }

  getId( model: Usuario ): number {
     return model.id_usuario
  }

  setFormGroupTOModel(model: Usuario): Usuario {
    let empresas = []
    let departamento = []
    // tslint:disable-next-line:forin
    for (const key in this.filterEdicao.controls) {
      if ((key.indexOf('id_empresa') < 0) && (key.indexOf('id_departamento') < 0) ) {
        model[key] = this.filterEdicao.controls[key].value
      } else {
        if (this.filterEdicao.controls[key].value) {
          if (key.indexOf('id_empresa') >= 0)  {
             empresas.push(key.split('id_empresa')[1])
          } else {
            departamento.push(key.split('id_departamento')[1])
          }
        }
      }
    }
    model.camposAuxiliares = { empresas: empresas ,  departamentos: departamento   }
    return model
  }



}
