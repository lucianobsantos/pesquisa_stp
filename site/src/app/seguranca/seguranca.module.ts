import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

import { SegurancaRoutes } from './seguranca.routing';

import { UsuariolistaComponent } from './usuario/usuariolista.component';
import { UsuarioTelaComponent } from './usuario/usuariotela.component';

// import { UsuarioService } from '../framework/service/usuario.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SegurancaRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule
  ],
  declarations: [
    UsuariolistaComponent,
    UsuarioTelaComponent
  ]
})
export class SegurancaModule {}
