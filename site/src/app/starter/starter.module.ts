import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {NgxEchartsModule} from 'ngx-echarts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgSelectModule } from '@ng-select/ng-select';

import { CardIndicadorService } from './../framework/service/card.service'
import { DesempenhoComponent} from '../dashboard-component'
import { PrincipalComponent} from '../dashboard-component'
import { EstoqueComponent} from '../dashboard-component'
import { RelatorioComponent} from '../dashboard-component'
import { DetalhamentoComponent } from '../dashboard-component'
import { FaturamentoComponent} from '../dashboard-component'
import { AcumuladoComponent} from '../dashboard-component'
import { FaturamentoDetalhamentoComponent } from '../dashboard-component'
import { RelatorioMesesComponent } from '../dashboard-component'
import { RelatorioAnaliticoComponent } from '../dashboard-component'
import { StarterComponent } from './starter.component';
import { ResumoFaturamentoComponent } from '../dashboard-stp'
import { FaturamentoStpComponent } from '../dashboard-stp'

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Starter Page',
      urls: [
        { title: 'Dashboard', url: '/dashboard' },
        { title: 'Starter Page' }
      ]
    },
    component: StarterComponent,
    children: [
      { path: 'relatorio/:meses/:id_sequencia', component: RelatorioMesesComponent },
      { path: 'relatorioanalitico', component: RelatorioAnaliticoComponent },
      { path: 'empresa/:id_sequencia', component: DetalhamentoComponent },
      { path: 'departamento/:id_sequencia', component: DetalhamentoComponent },
      { path: 'empresadepartamento/:id_sequencia/:id_empresa', component: DetalhamentoComponent },
      { path: 'departamentoempresa/:id_sequencia/:id_departamento', component: DetalhamentoComponent },
    ]
  }
];

@NgModule({
  imports: [NgxEchartsModule, FormsModule, CommonModule, RouterModule.forChild(routes), NgbModule,
    ReactiveFormsModule, NgSelectModule],
  declarations: [ StarterComponent, DesempenhoComponent, PrincipalComponent, EstoqueComponent,
    RelatorioComponent, DetalhamentoComponent, FaturamentoComponent, AcumuladoComponent, FaturamentoDetalhamentoComponent,
    RelatorioMesesComponent, RelatorioAnaliticoComponent, FaturamentoStpComponent,ResumoFaturamentoComponent],
  providers: [ CardIndicadorService ]
})
export class StarterModule { }


