import { Component, AfterViewInit, OnInit } from '@angular/core'
import { FilterService } from './../framework/service/filter.service'
import { CardIndicadorService } from './../framework/service/card.service'
import { Desempenho } from './../framework/model/desempenho.model'
import { Principal } from './../framework/model/principal.model'
import { Estoque } from './../framework/model/estoque.model'
import { Router, NavigationEnd } from '@angular/router'
import { LoginService } from '../framework/service/login.service';


@Component({
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.css']
})
export class StarterComponent implements AfterViewInit, OnInit {
  showDetalhe = false
  posicao = 0
  nomeGrupo: string
  periodoStr: string

  listaDesempenho: Array<Desempenho> = []
  listaPrincipal: Array<Principal> = []
  listaFaturamento: Array<Principal> = []
  listaEstoqueVeiculos: Array<Estoque> = []
  listaEstoquePecas: Array<Estoque> = []
  listaRank: Array<Principal> = []

  subtitle: string

  principalSelecionado = null
  mudaIndicadorPrincipal(principal: object) {

    if (!this.showDetalhe) {
      this.posicao = window.scrollY
      this.principalSelecionado = principal['model']
    } else {
    }

    if (principal['tipo'] === '6') {
      this.router.navigateByUrl(
        '/starter/relatorio/6/' + this.principalSelecionado.id_sequencia
      )
    } else if (principal['tipo'] === '12') {
      this.router.navigateByUrl(
        '/starter/relatorio/12/' + this.principalSelecionado.id_sequencia
      )
    } else if (principal['tipo'] === 'A') {

      this.router.navigate( ['/starter/relatorioanalitico/' ]  ,
      { queryParams: {id_consulta : this.listaFaturamento[0].camposAuxiliares['id_consulta'] }})
    } else if (principal['tipo'] === 'E') {
      this.router.navigate( ['/starter/empresa/' + this.listaFaturamento[0].id_sequencia + '/' ]  ,
      { queryParams: {id_consulta : this.listaFaturamento[0].camposAuxiliares['id_consulta'] }})
    } else if (principal['tipo'] === 'D') {
      this.router.navigate( ['/starter/departamento/' + this.listaFaturamento[0].id_sequencia + '/' ] ,
      { queryParams: {id_consulta : this.listaFaturamento[0].camposAuxiliares['id_consulta'] }})
    }
    this.showDetalhe = !this.showDetalhe
  }

  constructor(
    public filterService: FilterService,
    public cardIndicadorService: CardIndicadorService,
    public router: Router,
    private loginService: LoginService
  ) {
    this.router.events.subscribe(event => {
      let enderecoRota = ''
      if (event instanceof NavigationEnd) {
        enderecoRota = event.url
        if (
          enderecoRota !== '/starter' &&
          event.urlAfterRedirects !== '/starter'
        ) {
          this.showDetalhe = true
        } else {
          this.showDetalhe = false
          setTimeout(() => {
            window.scroll(0, this.posicao)
          }, 100)
        }
      }
    })
  }

  ngOnInit() {
    this.nomeGrupo = this.filterService.getBiGrupo().descricao
    this.periodoStr = this.filterService.getfiltroPadrao().periodo
  }

  ngAfterViewInit() {
    /*  this.cardIndicadorService.getTop3().subscribe(
      resultado => {
        this.listaDesempenho = resultado.dados
      },
      response => {
        console.log(response)
      }
    )*/




    if ( !this.showDetalhe ) {
  /*    this.cardIndicadorService.getIndicadores().subscribe(resultado => {
        this.listaPrincipal = resultado.dados
     })
*/
    const model = { 'tipoquery' : 'card' , id: null, id_usuario : this.loginService.user.id_usuario };
    this.cardIndicadorService.getIndicadorFaturamento(model).subscribe(resultado => {
      this.listaFaturamento = resultado.dados

    })

    this.cardIndicadorService
      .getIndicadorEstoqueVeiculo()
      .subscribe(resultado => {
        this.listaEstoqueVeiculos = resultado.dados

      })

    this.cardIndicadorService
      .getIndicadorEstoquePecas()
      .subscribe(resultado => {
        this.listaEstoquePecas = resultado.dados
      })
    }
  }
}
