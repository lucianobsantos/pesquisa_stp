import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { NgSelectModule } from '@ng-select/ng-select'

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar'

import { NgxChartsModule } from '@swimlane/ngx-charts'

import { DashboardpesquisaRoutes } from './dashboard_pesquisa.routing'

import { DashboardprincipalComponent } from './principal/dashboard_principal.component'

import { DashboardGeralService } from '../framework/service/dashboard.service '

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardpesquisaRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
    PerfectScrollbarModule,
    NgxChartsModule
  ],
  declarations: [
    DashboardprincipalComponent
  ],
  providers: [
    DashboardGeralService
  ]
})
export class DashboardpesquisaModule { }
