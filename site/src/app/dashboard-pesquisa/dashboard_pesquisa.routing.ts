import { Routes } from '@angular/router'

// import { CarroListaComponent } from './carro/carrolista.component';
// import { QuestionariolistaComponent } from './lista/questionariolista.component'
import { DashboardprincipalComponent } from './principal/dashboard_principal.component'

export const DashboardpesquisaRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: DashboardprincipalComponent
      },/*,
      {
        path: 'perguntaquestionario/:id',
        component: PerguntalistaComponent
      },
      {
        path: 'questionario/:id',
        component: QuestionariolistaComponent
      }*/
    ]
  }
];
