import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// import { TabelaGridComponent } from '../../../framework/component/tabela-grid/tabela-grid.component'
import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'

import { DashboardGeralService } from '../../framework/service/dashboard.service '
import { Dashboardgeral } from '../../framework/model/dashboardgeral.model'
import { LoginService } from '../../framework/service/login.service'

import * as c3 from 'c3';
/*
import * as shape from 'd3-shape';
import * as d3 from 'd3'; */
import { single } from './data';
// import { colorSets } from '@swimlane/ngx-charts/release/utils/color-sets';

@Component({
  selector: 'app-principal',
  templateUrl: './dashboard_principal.component.html',
  styleUrls: ['./dashboard_principal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardprincipalComponent // extends TabelaGridComponent<Questionario>
  implements OnInit   {

    public config: PerfectScrollbarConfigInterface = {}

    // gráfico 2
    qtdPesquisa: number
    single: any[];
    dateData: any[];
    dateDataWithRange: any[];
    range = false;
    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = false;
    showXAxisLabel = true;
    tooltipDisabled = false;
    xAxisLabel = 'Meses';
    showYAxisLabel = true;
    yAxisLabel = 'Quantidade';
    showGridLines = true;
    innerPadding = 0;
    autoScale = true;
    timeline = false;
    barPadding = 5;
    groupPadding = 0;
    roundDomains = false;
    maxRadius = 10;
    minRadius = 3;
    view = '';
    showLabels = true;
    explodeSlices = false;
    doughnut = false;
    arcWidth = 0.25;
    rangeFillOpacity = 0.15;

    colorScheme = {
      domain: ['#4fc3f7', '#fb8c00', '#7460ee', '#f62d51', '#20c997', '#2962FF']
    };
    schemeType = 'ordinal';

  user: Usuario = {}
  tituloPagina: String = ''
  lista: Dashboardgeral[] = []
  paginacao = {
    totalItens: 0,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }

  ngOnInit() {
    // super.ngOnInit()
    this.getDadosGeral()

    this.tituloPagina = 'Dashboard geral'
    const chart2 = c3.generate({
      bindto: '#placeholder',
      data: {
        columns: [
          ['Pesquisas', 5, 6, 3, 7, 9, 10, 14, 12, 11, 9, 8, 7, 10, 6, 12, 10, 8],
          ['Nota média', 1, 2, 8, 3, 4, 5, 7, 6, 5, 6, 4, 3, 3, 12, 5, 6, 3]
        ],
        type: 'spline'
      },
      axis: {
        y: {
          show: true,
          tick: {
            count: 0,
            outer: false
          }
        },
        x: {
          show: true
        }
      },
      padding: {
        top: 40,
        right: 10,
        bottom: 40,
        left: 20
      },
      point: {
        r: 0
      },
      legend: {
        hide: false
      },
      color: {
        pattern: ['#2961ff', '#ff821c', '#ff821c', '#7e74fb']
      }
    });

  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastrService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    public serviceDashboard: DashboardGeralService,
    private fb: FormBuilder
  ) {
    /* super(_router, http, toastrService , loginService, service)
    this.filterForm = this.fb.group({
      ques_tx_descricao: this.fb.control('', [  ]),
      ques_tx_cnpj_empresa: this.fb.control('', [  ]),
      ques_tx_logo_empresa: this.fb.control('', [  ]),
      ques_tx_texto_inicio: this.fb.control('', [  ]),
      ques_tx_texto_fim: this.fb.control('', [  ]),
      situ_tx_situacao: this.fb.control('A', [])
    }) */
    this.single = single

  }

  getDadosGeral() {
    const listaaux: Dashboardgeral[] = []
    this.serviceDashboard
       .getDadosGeral({})
       .subscribe(retorno => {
          this.lista = retorno.dados
          this.qtdPesquisa = this.lista[0].camposAuxiliares.qtd_pesquisa
          for (let i = 1; i < this.lista.length; i++) {
            listaaux.push(this.lista[i])
          }
          this.lista = listaaux

         // console.log(this.lista)
         if (retorno.total_registro) {
           this.paginacao.totalItens = retorno.total_registro
         }
       })
   }

}
