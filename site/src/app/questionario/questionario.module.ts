import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { NgSelectModule } from '@ng-select/ng-select'

import { QuestionarioRoutes } from './questionario.routing'

import { QuestionariolistaComponent } from './lista/questionariolista.component'
import { PerguntalistaComponent } from './pergunta/perguntalista.component'

import { QuestionarioService } from '../framework/service/questionario.service'
import { PerguntaService } from '../framework/service/pergunta.service'
import { RespostaService } from '../framework/service/resposta.service'
import { PesquisarespostaService } from '../framework/service/pesquisaresposta.service'

import {RespostaComponent } from './resposta/resposta.component'
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(QuestionarioRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule
  ],
  declarations: [
    QuestionariolistaComponent, PerguntalistaComponent,
    RespostaComponent
  ],
  providers: [
    QuestionarioService, PerguntaService, RespostaService, PesquisarespostaService
  ]
})
export class QuestionarioModule { }
