import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

// import { TabelaGridComponent } from '../../../framework/component/tabela-grid/tabela-grid.component'
import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'

import { QuestionarioService } from '../../framework/service/questionario.service'
import { Questionario } from '../../framework/model/questionario.model'
import { LoginService } from '../../framework/service/login.service'

@Component({
  selector: 'app-questionariolista',
  templateUrl: './questionariolista.component.html',
  encapsulation: ViewEncapsulation.None
})
export class QuestionariolistaComponent // extends TabelaGridComponent<Questionario>
  implements OnInit   {

  user: Usuario = {}
  tituloPagina: String = ''
  lista: Questionario[] = []
  paginacao = {
    totalItens: 0,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }

  ngOnInit() {
    // super.ngOnInit()
    this.getDados()
    this.tituloPagina = 'Questionários'
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastrService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    public serviceQuestionario: QuestionarioService,
    private fb: FormBuilder
  ) {
    /* super(_router, http, toastrService , loginService, service)
    this.filterForm = this.fb.group({
      ques_tx_descricao: this.fb.control('', [  ]),
      ques_tx_cnpj_empresa: this.fb.control('', [  ]),
      ques_tx_logo_empresa: this.fb.control('', [  ]),
      ques_tx_texto_inicio: this.fb.control('', [  ]),
      ques_tx_texto_fim: this.fb.control('', [  ]),
      situ_tx_situacao: this.fb.control('A', [])
    }) */
  }

  getDados() {
    this.serviceQuestionario
       .getFilter({situ_tx_situacao: 'A'}, 1, 1000)
       .subscribe(retorno => {
         this.lista = retorno.dados
         // console.log(retorno.dados)
         if (retorno.total_registro) {
           this.paginacao.totalItens = retorno.total_registro
         }
       })
   }

  getId( model: Questionario ): number {
     return model.ques_nr_sequencia
  }
}
