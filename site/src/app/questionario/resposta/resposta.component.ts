import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core'
import { Router, NavigationEnd } from '@angular/router';

import { RespostaService } from '../../framework/service/resposta.service'
import { Resposta } from '../../framework/model/resposta.model'

@Component({
  selector: 'app-resposta',
  templateUrl: './resposta.component.html',
  encapsulation: ViewEncapsulation.None
  })
export class RespostaComponent
  implements OnInit {

    /*
    TIPOS:

      'RADIO'
      'CHECKBOX'
      'NUMERO'
      'TEXTO_PEQUENO'
      'TEXTO_GRANDE'
      'LINEAR'
      'AVALIACAO'
    */


    listRespostas: Resposta[] = null
    id_pergunta: number

    @Input() tipo = ''
    @Input() index = 0

    paginacao = {
      totalItens: 0,
      itensPorPagina: 5,
      paginaCorrente: 1,
      maximoPaginas: 10
    }

    // tslint:disable-next-line:no-output-on-prefix
    @Output('onResposta')
    onResposta = new EventEmitter<object>()
    getResposta(id_resposta: number, tx_resposta: string) {
      // console.log({resposta_individual : {ques_nr_sequencia: -1, id_pergunta: this.id_pergunta, id_resposta: id_resposta, descricao: tx_resposta }})
      this.onResposta.emit({perg_nr_sequencia: this.id_pergunta, resp_nr_sequencia: id_resposta, pere_tx_descricao: tx_resposta })
    }

  getDados() {
    this.serviceResposta
       .getFilter({situ_tx_situacao: 'A', perg_nr_sequencia: this.id_pergunta }, 1, 1000)
       .subscribe(retorno => {
         this.listRespostas =  retorno.dados

         //console.log(retorno.dados)

          if (retorno.total_registro) {
            // this.ques_tx_logo_empresa = this.lista[0].camposAuxiliares.ques_tx_logo_empresa
            this.paginacao.totalItens = retorno.total_registro
          }
       })
   }

  @Input('perguntaId')
  set perguntaId(value: number) {
    this.id_pergunta = value

     this.getDados()

/*
    // vai no servico de resposta e pega as respostas possiveis 
    this.listRespostas = [{
      resp_nr_sequenca: 1, resp_tx_descricao: 'respota 1 ',
      resp_tx_valor: '1', resp_tx_imagem: 'assets/images/users/1.png'
    },
    {
      resp_nr_sequenca: 2, resp_tx_descricao: 'respota 2 ',
      resp_tx_valor: '2', resp_tx_imagem: 'assets/images/users/2.png'
    },
    {
      resp_nr_sequenca: 3, resp_tx_descricao: 'respota 3 ',
      resp_tx_valor: '3', resp_tx_imagem: 'assets/images/users/3.png'
    },
    {
      resp_nr_sequenca: 4, resp_tx_descricao: 'respota 4',
      resp_tx_valor: '4', resp_tx_imagem: 'assets/images/users/1.png'
    },
    {
      resp_nr_sequenca: 5, resp_tx_descricao: 'respota 5 ',
      resp_tx_valor: '5', resp_tx_imagem: 'assets/images/users/2.png'
    }
    ];
    if (( value === 2 ) || ( value === 11 ) || ( value === 8 ) || ( value === 10 ) ) {
      this.listRespostas = [{
        resp_nr_sequenca: 1, resp_tx_descricao: 'Sim',
        resp_tx_valor: '1', resp_tx_imagem: 'assets/images/sim.png'
      },
      {
        resp_nr_sequenca: 2, resp_tx_descricao: 'Não',
        resp_tx_valor: '2', resp_tx_imagem: 'assets/images/nao.png'
      }
    ]
    }
    if (( value === 6 )   ) {
      this.listRespostas = [{
        resp_nr_sequenca: 1, resp_tx_descricao: '9 a 10 Satisfeito ',
        resp_tx_valor: '1', resp_tx_imagem: 'assets/images/sim.png'
      },
      {
        resp_nr_sequenca: 2, resp_tx_descricao: '1 a 8 Insatisfeito ',
        resp_tx_valor: '2', resp_tx_imagem: 'assets/images/nao.png'
      }
    ]
    }

    if ( value === 3 ){
      this.listRespostas = [{
        resp_nr_sequenca: 1, resp_tx_descricao: '1 ',
        resp_tx_valor: '1', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 2, resp_tx_descricao: '2 ',
        resp_tx_valor: '2', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 3, resp_tx_descricao: '3 ',
        resp_tx_valor: '3', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 4, resp_tx_descricao: '4 ',
        resp_tx_valor: '4', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 5, resp_tx_descricao: '5 ',
        resp_tx_valor: '5', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 1, resp_tx_descricao: '6 ',
        resp_tx_valor: '1', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 2, resp_tx_descricao: '7 ',
        resp_tx_valor: '2', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 3, resp_tx_descricao: '8 ',
        resp_tx_valor: '3', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 4, resp_tx_descricao: '9 ',
        resp_tx_valor: '4', resp_tx_imagem: ''
      },
      {
        resp_nr_sequenca: 5, resp_tx_descricao: '10 ',
        resp_tx_valor: '5', resp_tx_imagem: ''
      }
      ];
    }
*/
  }

  ngOnInit() {
    // console.log( 'id_perg ' + @Input('perguntaId'))
  }

  onClick(index) {
    const x = document.querySelector('#passo_' + index)
    if (x) {
        x.scrollIntoView();
    }
  }

  constructor(
    public router: Router,
    public serviceResposta: RespostaService) {

  }


}
