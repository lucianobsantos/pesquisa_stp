import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router, ActivatedRoute, NavigationEnd, RouterEvent } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

// import { TabelaGridComponent } from '../../../framework/component/tabela-grid/tabela-grid.component'
import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'

import { PerguntaService } from '../../framework/service/pergunta.service'
import { PesquisarespostaService } from '../../framework/service/pesquisaresposta.service'
import { Pergunta } from '../../framework/model/pergunta.model'
import { Pesquisaresposta } from '../../framework/model/pesquisaresposta.model'
import { LoginService } from '../../framework/service/login.service'

@Component({
  selector: 'app-perguntalista',
  templateUrl: './perguntalista.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PerguntalistaComponent // extends TabelaGridComponent<Questionario>
  implements OnInit   {

    listaRespostas: Array<Pesquisaresposta> = []

    model: Pesquisaresposta = {}

    id_questionario:   string
    guid:              string
    textoQuestionario: any

    ques_tx_logo_empresa: string = null
    user: Usuario = {}
    tituloPagina: String = ''
    lista: Pergunta[] = []

    paginacao = {
      totalItens: 0,
      itensPorPagina: 5,
      paginaCorrente: 1,
      maximoPaginas: 10
    }

    newGuid() {
      return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          const r = Math.random() * 16 | 0, v = c === 'x' ? r : ( r & 0x3 | 0x8 );
          return v.toString(16);
      });

    }

  ngOnInit() {
    // super.ngOnInit()
    this.getDados()
    this.tituloPagina = 'Perguntas'
    this.guid = this.newGuid()

  }

  setarRespostas(resposta: any) {

    this.model = resposta
    this.model.ques_nr_sequencia = Number( this.id_questionario)
    this.model.pere_tx_identificador = this.guid

    let repetido = false

    // Substitui uma resposta
    this.listaRespostas.forEach( (item, index) => {
      if (this.model['perg_nr_sequencia'] === item['perg_nr_sequencia']) {
        this.listaRespostas[index] = this.model
        console.log('substituido resp ' + item['resp_nr_sequencia'] + ' por ' + this.model['resp_nr_sequencia'])
        repetido = true
        return
      }
    })

    if (!repetido) {
      this.listaRespostas.push( this.model )
    }
    console.log(this.model)

    // console.log('Lista de respostas >>>> ', this.listaRespostas)
  }

  confimar() {
    console.log('Lista de respostas para salvar >>>> ', this.listaRespostas)

    this.listaRespostas.forEach( (item) => {
      this.salvar(item)
    })

    if (this.listaRespostas.length > 0) {
      this.toastr.success('Pesquisa finalizada com sucesso, obrigado!');
    } else {
      this.toastr.warning('Favor responder o questionário antes de confirmar!');
      document.querySelector('#passo_0').scrollIntoView()
    }

    this.listaRespostas = []

    // location.reload()
  }

  salvar(model: Pesquisaresposta) {
    // console.log('Lista de respostas para salvar >>>> ', model)

    this.servicePesquisaResposta
      .post( model)
      .subscribe(
        user => {
          // console.log(model.resp_nr_sequencia +  ' - OK')
          // this.toastr.success('Pesquisa gravada com sucesso, obrigado!');
        },
        response => {
          if ( response.error.message ) {
            this.toastr.error( response.error.message, 'Erro!')
          } else {
             this.toastr.error( response.message, 'Erro!')
          }
        }
      )

    }

  constructor(
    public _router: Router,
    public activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public toastrService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    public servicePergunta: PerguntaService,
    private toastr: ToastrService,
    public servicePesquisaResposta: PesquisarespostaService,
    private fb: FormBuilder
  ) {
    /* super(_router, http, toastrService , loginService, service)
    this.filterForm = this.fb.group({
      ques_tx_descricao: this.fb.control('', [  ]),
      ques_tx_cnpj_empresa: this.fb.control('', [  ]),
      ques_tx_logo_empresa: this.fb.control('', [  ]),
      ques_tx_texto_inicio: this.fb.control('', [  ]),
      ques_tx_texto_fim: this.fb.control('', [  ]),
      situ_tx_situacao: this.fb.control('A', [])
    }) */

    this.id_questionario = this.activatedRoute.snapshot.paramMap.get('id')

  }

  getDados() {
    this.servicePergunta
       .getFilter({situ_tx_situacao: 'A', ques_nr_sequencia: this.id_questionario }, 1, 1000)
       .subscribe(retorno => {
         this.lista = retorno.dados

          if (retorno.total_registro) {
            this.ques_tx_logo_empresa = this.lista[0].camposAuxiliares.ques_tx_logo_empresa
            this.textoQuestionario = {
                ques_tx_texto_inicio: this.lista[0].camposAuxiliares.ques_tx_texto_inicio,
                ques_tx_texto_fim: this.lista[0].camposAuxiliares.ques_tx_texto_fim
              }
            this.paginacao.totalItens = retorno.total_registro
          }
       })
   }

  getId( model: Pergunta ): number {
     return model.perg_nr_sequencia
  }
}
