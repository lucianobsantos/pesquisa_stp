import { Routes } from '@angular/router'

// import { CarroListaComponent } from './carro/carrolista.component';
import { QuestionariolistaComponent } from './lista/questionariolista.component'
import { PerguntalistaComponent } from './pergunta/perguntalista.component'

export const QuestionarioRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: QuestionariolistaComponent
      },
      {
        path: 'perguntaquestionario/:id',
        component: PerguntalistaComponent
      },
      {
        path: 'questionario/:id',
        component: QuestionariolistaComponent
      }/*, 
      {
        path: 'revisao/:placa/:revisao',
        component: RevisaoDadosComponent
      }*/
      /*,
      {
        path: 'usuario/:id',
        component: UsuarioTelaComponent,
        children: [
          { path: '', redirectTo: 'detail', pathMatch: 'full' },
          { path: 'detail', component: UsuarioTelaComponent },
          { path: 'edit', component: UsuarioTelaComponent },
          { path: 'new', component: UsuarioTelaComponent }
        ]
      }*/
    ]
  }
];
