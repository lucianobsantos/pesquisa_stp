import { Component, AfterViewInit, EventEmitter, Output } from '@angular/core'
import { Router } from '@angular/router'
import {
  NgbModal,
  ModalDismissReasons,
  NgbCarouselConfig
} from '@ng-bootstrap/ng-bootstrap'
import { FormBuilder, FormGroup } from '@angular/forms'
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar'

import { LoginService } from '../../framework/service/login.service'
import { BIEMPRESAService } from '../../framework/service/biempresa.service'
import { BIDEPARTAMENTOService } from '../../framework/service/bidepartamento.service'
import { User } from '../../framework/model/user.model'
import { BIEMPRESA } from '../../framework/model/biempresa.model'
import { BIDEPARTAMENTO } from '../../framework/model/bidepartamento.model'
import { FilterService } from '../../framework/service/filter.service'

declare var $: any

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements AfterViewInit {
  @Output()
  toggleSidebar = new EventEmitter<void>()

  public listaEmpresa: BIEMPRESA[]
  public listaDepartamento: BIDEPARTAMENTO[]
  public listaSituacao: Array<any> = [
    { situ_tx_situacao: '', tx_situacao: 'Todos' },
    { situ_tx_situacao: 'A', tx_situacao: 'Ativo' },
    { situ_tx_situacao: 'I', tx_situacao: 'Inativo' }
  ]
  filterForm: FormGroup

  public config: PerfectScrollbarConfigInterface = {}
  public nomeEmpresa : String = "Empresa";
  public empresaSelecionada : BIEMPRESA = {};
  public showSearch = false

  constructor(
    private loginService: LoginService,
    private router: Router,
    private filterService: FilterService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private biempresaService: BIEMPRESAService,
    private bidepartamentoService: BIDEPARTAMENTOService
  ) {
    this.filterForm = this.fb.group({
      periodo: this.fb.control(
        this.filterService.getfiltroPadrao().periodo,
        []
      ),
      empresasfiltro: this.fb.control(
        this.filterService.getfiltroPadrao().empresasfiltro,
        []
      ),
      departamentosfiltro: this.fb.control(
        this.filterService.getfiltroPadrao().departamentosfiltro,
        []
      )
    })

  }

  user(): User {
    return this.loginService.user
  }

  logout() {
    this.loginService.logout()
  }

  ngAfterViewInit() {
    // this.getDados()
  }

  onMudaEmpresa( item: BIEMPRESA ) {
    this.empresaSelecionada = item;
    this.nomeEmpresa = item.nome;
  }

  onSelectedNotSelected(value: string, situacao: string) {
    const indpapoex = Array.from(this.listaDepartamento).findIndex(
      (element, index, array) => {
        return element['iD_DEPARTAMENTO'] === value
      }
    )

    const itemSel = this.listaDepartamento[indpapoex]

    if (indpapoex >= 0) {
      const qtdMarcadas = this.listaDepartamento.filter(function(x) {
        return (
          x.camposAuxiliares['marcado'] === 'S' &&
          x.camposAuxiliares['conta'].indexOf('.' + value + '.') >= 0
        )
      }).length

      const qtdDesmarcadas = this.listaDepartamento.filter(function(x) {
        return (
          x.camposAuxiliares['marcado'] !== 'S' &&
          x.camposAuxiliares['conta'].indexOf('.' + value + '.') >= 0
        )
      }).length

      if (qtdDesmarcadas === 0) {
        this.listaDepartamento[indpapoex].camposAuxiliares['marcado'] = 'S'
      } else {
        this.listaDepartamento[indpapoex].camposAuxiliares['marcado'] =
          qtdMarcadas === 0 ? 'N' : 'X'
      }
      if (itemSel['iD_DEPARTAMENTO'] !== itemSel['iD_DEPARTAMENTO_PAI']) {
        this.onSelectedNotSelected(itemSel['iD_DEPARTAMENTO_PAI'], situacao)
      }
    }
  }

  onSelect(item: BIDEPARTAMENTO): void {
    item.camposAuxiliares['marcado'] =
      item.camposAuxiliares['marcado'] === 'S' ? 'N' : 'S'

    this.listaDepartamento.forEach(objeto => {
      if (
        objeto.camposAuxiliares['conta'].indexOf(
          '.' + item['iD_DEPARTAMENTO'] + '.'
        ) >= 0
      ) {
        objeto.camposAuxiliares['marcado'] = item.camposAuxiliares['marcado']
      }
    })
    this.onSelectedNotSelected(
      item['iD_DEPARTAMENTO_PAI'],
      item.camposAuxiliares['marcado']
    )
  }

  onSelectEmpresa(item: BIEMPRESA): void {
    item.camposAuxiliares['marcado'] = item.camposAuxiliares['marcado'] === 'S' ? 'N' : 'S'
  }


  onTodosEmpresa( ): void {
    const qtdMarcadas = this.listaEmpresa.filter(function(x) { return ( x.camposAuxiliares['marcado'] === 'S'    )  }).length
      this.listaEmpresa.forEach(objeto => {
        objeto.camposAuxiliares['marcado'] = ( qtdMarcadas > 0 ? 'N' : 'S' )
      })
  }

  onTodosDepartamento( ): void {
    const qtdMarcadas = this.listaDepartamento.filter(function(x) { return ( x.camposAuxiliares['marcado'] === 'S'    )  }).length
      this.listaDepartamento.forEach(objeto => {
        objeto.camposAuxiliares['marcado'] = ( qtdMarcadas > 0 ? 'N' : 'S' )
      })
  }




  sendMessage(): void {

    let empresasfiltro = []
    this.listaEmpresa.forEach(objeto => {
      if ( objeto.camposAuxiliares['marcado'] === 'S' ) {
         empresasfiltro.push( objeto['iD_EMPRESA'] )
      }
    })
    this.filterForm.controls['empresasfiltro'].setValue(empresasfiltro);
    let departamentofiltro = []
    this.listaDepartamento.forEach(objeto => {
      if ( objeto.camposAuxiliares['marcado'] === 'S' ) {
        departamentofiltro.push( objeto['iD_DEPARTAMENTO'] )
      }
    })
    this.filterForm.controls['departamentosfiltro'].setValue(departamentofiltro);

    this.filterService.setfiltroPadrao( this.filterForm.value )
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false
    }

    let currentUrl = this.router.url + '?'

    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false
      this.router.navigate([this.router.url])
    })
    window.scrollTo(0, 0)
  }

  getDados() {
    this.biempresaService
      .getFilter({ id_usuario: this.user().id_usuario }, 1, 100)
      .subscribe(retorno => {
        this.listaEmpresa = retorno.dados
 
        this.empresaSelecionada = this.listaEmpresa.filter( item => item['iD_EMPRESA']  === 6 )[0];
        this.filterService.setBiEmpresaSelecionada( this.empresaSelecionada );
        this.nomeEmpresa = this.empresaSelecionada.nome;

        if ( this.filterService.getfiltroPadrao().empresasfiltro.length > 0  ) {
        this.listaEmpresa.forEach(objeto => {
          let contagem = this.filterService.getfiltroPadrao().empresasfiltro.filter(function(x) {
            return ( x ===  objeto['iD_EMPRESA'] )
          }).length
          objeto.camposAuxiliares['marcado'] = ( contagem > 0 ? 'S' : 'N' )
        })
      }

      })

    this.bidepartamentoService
      .getFilter({ id_usuario: this.user().id_usuario }, 1, 100)
      .subscribe(retorno => {
        this.listaDepartamento = retorno.dados
        if ( this.filterService.getfiltroPadrao().departamentosfiltro.length > 0  ) {
          this.listaDepartamento.forEach(objeto => {
            let contagem = this.filterService.getfiltroPadrao().departamentosfiltro.filter(function(x) {
              return ( x ===  objeto['iD_DEPARTAMENTO'] )
            }).length
            objeto.camposAuxiliares['marcado'] = ( contagem > 0 ? 'S' : 'N' )
          })
        }
      })
  }
}
