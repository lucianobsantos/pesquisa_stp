import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

import { RevisaoRoutes } from './revisao.routing';

import { InvoiceComponent } from './carro/invoice.component';
import { CarroListaComponent } from './carro/carrolista.component';
import { CarroDadosComponent } from './carro/carrodados.component';
import { RevisaoDadosComponent } from './carro/revisaodados.component';
import { CarroService } from '../framework/service/carro.service';
import { Grupo_ModeloService } from '../framework/service/grupo_modelo.service';
import { Revisao_clienteService } from '../framework/service/revisao_cliente.service';

import { UsuarioTelaComponent } from './usuario/usuariotela.component';
import { UsuariolistaComponent } from './usuario/usuariolista.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RevisaoRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule
  ],
  declarations: [
    CarroListaComponent,
    CarroDadosComponent,
    RevisaoDadosComponent,
    InvoiceComponent,
    UsuarioTelaComponent,
    UsuariolistaComponent
  ],
  providers: [
    CarroService, Grupo_ModeloService, Revisao_clienteService
  ]
})
export class RevisaoModule { }
