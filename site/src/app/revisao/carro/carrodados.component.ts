import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { FilterService } from '../../framework/service/filter.service';
 
import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'
import { LoginService } from '../../framework/service/login.service'
import { CarroService } from '../../framework/service/carro.service';
import { Grupo_ModeloService } from '../../framework/service/grupo_modelo.service';
import { Carro } from '../../framework/model/carro.model';
import { Revisao } from '../../framework/model/revisao.model';
import { Modelo } from '../../framework/model/modelo.model';
import { Grupo_Modelo } from '../../framework/model/grupo_modelo';

@Component({
  selector: 'app-usuariolista',
  templateUrl: './carrodados.component.html',
  styleUrls: ['./carrodados.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CarroDadosComponent implements OnInit   { //extends TabelaGridComponent<Usuario>
   


  localStorage:any;
  
  formDados: FormGroup

  user: Usuario = {}
  listaModelo: Modelo[] = [];
  listaRevisao: Revisao[] = [];
  tituloPagina: string ="";
  modelEdit: Grupo_Modelo = {}
  paginacao = {
    totalItens: 0,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }
  ngOnInit() {


     
   this.getModel( this.modelEdit);
   this.getModeloPorCarro( this.modelEdit);
   // this.tituloPagina = 'Ordem de Serviço'
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public activatedRoute: ActivatedRoute,
    public toastyService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    public carroService: CarroService,
    public filterService: FilterService,
    public grupo_ModeloService:Grupo_ModeloService,
    
    private fb: FormBuilder
  ) {
    this.localStorage = window.localStorage;
    this.activatedRoute.params.subscribe(params => {
      console.log( this.activatedRoute.snapshot.paramMap )
      this.modelEdit.identificador = +this.activatedRoute.snapshot.paramMap.get('id')
   });
    //super(_router, http, toastyService , loginService, service)
    this.formDados = this.fb.group({
      placa: this.fb.control('', [  ]),
      modelo: this.fb.control('', [  ]),
      revisao: this.fb.control('', [  ]),
     // email: this.fb.control('', [  ]),
      //senha: this.fb.control('', [  ]),
      //situacao: this.fb.control('A', [])
    })

  
    
  
  }

  getModel(model: Grupo_Modelo) {
    this.grupo_ModeloService.get(this.getId(model)).subscribe(retorno => {
      this.modelEdit = retorno.dados
    })
  }
  

  

  getModeloPorCarro(model: Carro) {
    this.grupo_ModeloService.getGrupoModelo(model.identificador, 1, 1000).subscribe(retorno => {
      this.listaModelo = retorno.dados
    })
  }


  getRevisaoPorModelo(model: Modelo) {

console.log( model )
this.localStorage.setItem("dadasModel", JSON.stringify(model ));
    var empresaSelecionada = this.filterService.getBiEmpresaSelecionada();
    // console.log( model )
    this.grupo_ModeloService.getRevisao( this.formDados.get('modelo').value, this.filterService.getBiEmpresaSelecionada().cnpj,  1, 1000).subscribe(retorno => {
      this.listaRevisao = retorno.dados
    })
  }

  changeRevisao(value ) {
    this.localStorage.setItem("dadasRevisao", JSON.stringify(value ));
    
  }

  getId( model: Carro ): number {
     return model.identificador
  }

  getUrl() {
    this.localStorage.setItem("dadosarrayRevisao", JSON.stringify(this.formDados.value ));
    return `/revisao/revisao/${this.formDados.get('placa').value}/${this.formDados.get('revisao').value}`
  }
 

  gotoBack() {
    window.history.back()
  }
}
