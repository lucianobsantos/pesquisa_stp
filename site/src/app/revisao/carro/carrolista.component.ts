import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

//import { TabelaGridComponent } from '../../framework/component/tabela-grid/tabela-grid.component'
import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'
import { LoginService } from '../../framework/service/login.service'
import { CarroService } from '../../framework/service/carro.service';
import { Carro } from '../../framework/model/carro.model';
import { IBaseService } from '../../framework/service/IBaseService';
import { Grupo_ModeloService } from '../../framework/service/grupo_modelo.service';
import { Grupo_Modelo } from '../../framework/model/grupo_modelo';

@Component({
  selector: 'app-usuariolista',
  templateUrl: './carrolista.component.html',
  styleUrls: ['./carrolista.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CarroListaComponent implements OnInit   { //extends TabelaGridComponent<Usuario>
   
  user: Usuario = {}
  tituloPagina: string ="";
  lista: Grupo_Modelo[] = []
  paginacao = {
    totalItens: 0,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }
  ngOnInit() {
 

   this.getDados();
   
    this.tituloPagina = 'Selecione um Veículo'
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastyService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    public grupo_modeloservice: Grupo_ModeloService,
    private fb: FormBuilder
  ) {
    //super(_router, http, toastyService , loginService, service)
  /*  this.filterForm = this.fb.group({
      nome: this.fb.control('', [  ]),
      apelido: this.fb.control('', [  ]),
      email: this.fb.control('', [  ]),
      senha: this.fb.control('', [  ]),
      situacao: this.fb.control('A', [])
    })*/
  }


  getDados() {
   this.grupo_modeloservice
      .getFilter({}, 1, 1000)
      .subscribe(retorno => {
        this.lista = retorno.dados
        if (retorno.total_registro) {
          this.paginacao.totalItens = retorno.total_registro
        }
      })
  }


  getId( model: Usuario ): number {
     return model.id_usuario
  }
}
