import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'


import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'
import { LoginService } from '../../framework/service/login.service'
import { CarroService } from '../../framework/service/carro.service';
import { Carro } from '../../framework/model/carro.model';
import { Revisao } from '../../framework/model/revisao.model';
import { Modelo } from '../../framework/model/modelo.model';
import { FilterService } from '../../framework/service/filter.service';
import { Grupo_ModeloService } from '../../framework/service/grupo_modelo.service';
import { Revisao_cliente } from '../../framework/model/revisao_cliente';
import { Revisao_clienteService } from '../../framework/service/revisao_cliente.service';

@Component({
  selector: 'app-usuariolista',
  templateUrl: './revisaodados.component.html',
  styleUrls: ['./revisaodados.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RevisaoDadosComponent implements OnInit { //extends TabelaGridComponent<Usuario>


localStorage : any;
  listaPecasObrigatorio = []
  listaPecasAdicional = []
  totalAdicional =0.00;
  

  dadasModel = null;
  dadasRevisao = null;
  dadosarrayRevisao =  null;



  empresaSelecionada = {};
  formDados: FormGroup
  placa: string = "";
  revisao: string;
  user: Usuario = {}
  listaModelo: Modelo[] = [];
  listaRevisao: Revisao[] = [];
  tituloPagina: string = "";
  modelEdit: Carro = {}
  paginacao = {
    totalItens: 0,
    itensPorPagina: 5,
    paginaCorrente: 1,
    maximoPaginas: 10
  }
  ngOnInit() {



    //  this.getModel( this.modelEdit);
    //  this.getModeloPorCarro( this.modelEdit);
    // super.ngOnInit()
    this.tituloPagina = 'Ordem de Serviço'
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public activatedRoute: ActivatedRoute,
    public toastyService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    public filterService: FilterService,
    public carroService: CarroService,
    public revisao_clienteService: Revisao_clienteService,
    private fb: FormBuilder,
    public grupo_ModeloService:Grupo_ModeloService
  ) {
    this.localStorage = window.localStorage;
    var empresaSelecionada = this.filterService.getBiEmpresaSelecionada();

    this.activatedRoute.params.subscribe(params => {
      console.log(this.activatedRoute.snapshot.paramMap)
      this.placa = this.activatedRoute.snapshot.paramMap.get('placa')
      this.revisao = this.activatedRoute.snapshot.paramMap.get('revisao')
      this.getItensdeRevisao( this.revisao )
    });
  
    this.formDados = this.fb.group({
      placa: this.fb.control('', []),
      modelo: this.fb.control('', []),
      revisao: this.fb.control('', []),
      item_teste: this.fb.control('', [  ]),
      //senha: this.fb.control('', [  ]),
      //situacao: this.fb.control('A', [])
    })



    this.dadasModel = JSON.parse(this.localStorage.getItem("dadasModel")) as {};
    this.dadasRevisao = JSON.parse(this.localStorage.getItem("dadasRevisao")) as {};   
    this.dadosarrayRevisao = JSON.parse(this.localStorage.getItem("dadosarrayRevisao")) as {};   
    console.log(  this.dadasModel  )
    console.log(  this.dadasRevisao  )
    console.log(  this.dadosarrayRevisao  )

  }

  getModel(model: Carro) {
    this.carroService.get(this.getId(model)).subscribe(retorno => {
      this.modelEdit = retorno.dados
    })
  }


  getItensdeRevisao(model: string) {

    var empresaSelecionada = this.filterService.getBiEmpresaSelecionada();
    this.grupo_ModeloService.getItensRevisao(this.revisao, empresaSelecionada.cnpj, 1, 1000).subscribe(retorno => {
      this.listaPecasObrigatorio = retorno.dados.filter( x=> x["obrigatorio"]==='S')
      this.listaPecasAdicional = retorno.dados.filter( x=> x["obrigatorio"]==='N')
    })
  }

 
  getId(model: Carro): number {
    return model.identificador
  }

  gotoBack() {
    window.history.back()
  }


  InsereRevisaoOrcamento( ) {

//this.listaPecasAdicional.filter( x => )

    var model = { 
      cnpj_empresa : this.filterService.getBiEmpresaSelecionada().cnpj,
      placa: this.dadosarrayRevisao.placa,
      situacao:'A',
      statusos:'O'
    }
    this.insert( model )
  }

  InsereRevisaoOS( ) {
    var model = { 
      cnpj_empresa : this.filterService.getBiEmpresaSelecionada().cnpj,
      placa: this.dadosarrayRevisao.placa,
      situacao:'A',
      statusos:'S'
    }
    this.insert( model )
  }


  insert(model: Revisao_cliente) {
    this.revisao_clienteService.post(model).subscribe(retorno => {
      this.modelEdit = retorno.dados
    })
  }



  onSelect( index  ) {
   
  //  console.log( this.listaPecasAdicional[index] )
    this.listaPecasAdicional[index].camposAuxiliares['marcado'] = this.listaPecasAdicional[index].camposAuxiliares['marcado'] === 'S' ? 'N' : 'S'
    this.totalAdicional = this.listaPecasAdicional.filter(item => item.camposAuxiliares["marcado"] === 'S')
    .map(item => +item.valor_venda* item.camposAuxiliares["qtde"])
    .reduce( (previousValue, currentValue) => previousValue + currentValue, 0);

    var lsepardor = '';
    var valorItem = "";
    this.listaPecasAdicional.filter(item => item.camposAuxiliares["marcado"] === 'S').forEach(item => {
      console.log( item )
      valorItem += lsepardor + item.identificador;
      lsepardor = ",";
  })
 this.formDados.get("item_teste").setValue(valorItem)
console.log( valorItem)

 
  }

}
