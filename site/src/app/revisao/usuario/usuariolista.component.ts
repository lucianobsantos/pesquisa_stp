import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

import { TabelaGridComponent } from '../../framework/component/tabela-grid/tabela-grid.component'
import { UsuarioService } from '../../framework/service/usuario.service'
import { Usuario } from '../../framework/model/usuario.model'
import { LoginService } from '../../framework/service/login.service'

@Component({
  selector: 'app-usuariolista',
  templateUrl: './usuariolista.component.html',
  styleUrls: ['./usuariolista.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UsuariolistaComponent extends TabelaGridComponent<Usuario>
  implements OnInit   {

  ngOnInit() {


    this.user = this.loginService.user
    if ( this.user.id_usuario !== 3 ) {
      this._router.navigate(['starter'])
   }


    super.ngOnInit()
    this.tituloPagina = 'Lista de Usuário'
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastyService: ToastrService,
    public loginService: LoginService,
    public service: UsuarioService,
    private fb: FormBuilder
  ) {
    super(_router, http, toastyService , loginService, service)
    this.filterForm = this.fb.group({
      nome: this.fb.control('', [  ]),
      apelido: this.fb.control('', [  ]),
      email: this.fb.control('', [  ]),
      senha: this.fb.control('', [  ]),
      situacao: this.fb.control('A', [])
    })
  }




  getId( model: Usuario ): number {
     return model.id_usuario
  }
}
