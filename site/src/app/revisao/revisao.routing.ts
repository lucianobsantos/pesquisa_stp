import { Routes } from '@angular/router';

import { CarroListaComponent } from './carro/carrolista.component';
import { CarroDadosComponent } from './carro/carrodados.component';
import { RevisaoDadosComponent } from './carro/revisaodados.component';
import { InvoiceComponent } from './carro/invoice.component';
 
export const RevisaoRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CarroListaComponent
      },
      {
        path: 'carro/:id',
        component: CarroDadosComponent
      }, {
        path: 'revisao/:placa/:revisao',
        component: RevisaoDadosComponent
      }, {
        path: 'invoice',
        component: InvoiceComponent
      }
      /*,
      {
        path: 'usuario/:id',
        component: UsuarioTelaComponent,
        children: [
          { path: '', redirectTo: 'detail', pathMatch: 'full' },
          { path: 'detail', component: UsuarioTelaComponent },
          { path: 'edit', component: UsuarioTelaComponent },
          { path: 'new', component: UsuarioTelaComponent }
        ]
      }*/
    ]
  }
];
