import { Component, OnInit, ViewEncapsulation , ViewChild} from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient  } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { LoginService } from '../../framework/service/login.service'


import {InfiniteScrollerDirective} from '../../framework/directive/infinite-scroller.directive'

import { TabelaGridComponent } from '../../framework/component/tabela-grid/tabela-grid.component'
import { Consultor } from '../../framework/model/consultor.model'
import { ConsultorService } from '../../framework/service/consultor.service'



@Component({
  selector: 'app-consultorlista',
  templateUrl: './consultorlista.component.html',
  styleUrls: ['./consultorlista.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConsultorlistaComponent extends TabelaGridComponent<Consultor>
  implements OnInit   {

    @ViewChild(InfiniteScrollerDirective) infiniteScroll;

    scrollCallback;
    // this.scrollCallback = this.getStories.bind(this);
    public listaSexo: Array<any> = [
      { value: 'F', label: 'Feminino' },
      { value: 'M', label: 'Masculino' }
    ]

  ngOnInit() {
    this.callDadosInit = false
    this.paginacao.itensPorPagina = 8;
    super.ngOnInit()
    this.tituloPagina = 'Lista de Consultor'


  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastr: ToastrService,
    public loginService: LoginService,
    public service: ConsultorService,
    private fb: FormBuilder
  ) {
    super(_router, http, toastr , loginService, service)
    this.scrollCallback = this.getDados.bind(this);
    this.filterForm = this.fb.group({
      cons_tx_nome: this.fb.control('', [  ]),
      cons_tx_apelido: this.fb.control('', [  ]),
      cons_tx_cpf: this.fb.control('', [  ]),
      cons_tx_rg: this.fb.control('', [  ]),
      cons_tx_orgao_rg: this.fb.control('', [  ]),
      cons_tx_sexo: this.fb.control('', [  ]),
      cons_tx_telefone1: this.fb.control('', [  ]),
      cons_tx_telefone2: this.fb.control('', [  ]),
      cons_tx_celular: this.fb.control('', [  ]),
      cons_tx_email: this.fb.control('', [  ]),
      cons_tx_cep: this.fb.control('', [  ]),
      cons_tx_logradouro: this.fb.control('', [  ]),
      cons_tx_numero: this.fb.control('', [  ]),
      cons_tx_bairro: this.fb.control('', [  ]),
      cons_tx_complemento: this.fb.control('', [  ]),
      muni_nr_sequencia: this.fb.control('', [  ]),
      cons_tx_curriculo: this.fb.control('', [  ]),
      situ_tx_situacao: this.fb.control('A', []),
      agree: this.fb.control('', []),
    })
  }

  getId( model: Consultor ): number {
     return model.cons_nr_sequencia
  }


  pesquisaDados() {
    this.paginacao.paginaCorrente = 1;
    this.lista =   []
    this.infiniteScroll.zeraPaginacao();
  }

  getDados() {

   return (this.service)
      .getFilter(this.filterForm.value, this.paginacao.paginaCorrente, this.paginacao.itensPorPagina)
      .do(this.processData);

  }



  private processData = (news) => {
    this.paginacao.paginaCorrente++;
    this.lista = this.lista.concat(news.dados);
  }

}
