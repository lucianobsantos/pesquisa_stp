import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

import { InfiniteScrollerDirective } from '../framework/directive/infinite-scroller.directive';
import { ColaboradorRoutes } from './colaborador.routing';
import { ConsultorService } from '../framework/service/consultor.service'
import { ConsultorlistaComponent } from './consultor/consultorlista.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ColaboradorRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule
  ],
  declarations: [
    InfiniteScrollerDirective,
    ConsultorlistaComponent
  ],
  providers: [
    ConsultorService,
  ]
})
export class ColaboradorModule {}
