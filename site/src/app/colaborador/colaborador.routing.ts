import { Routes } from '@angular/router';

import { ConsultorlistaComponent  } from './consultor/consultorlista.component';

export const ColaboradorRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'consultor',
        component: ConsultorlistaComponent
      }
    ]
  }
];
