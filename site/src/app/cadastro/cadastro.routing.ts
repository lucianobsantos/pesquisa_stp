import { Routes } from '@angular/router';

import { ArealistaComponent } from './area/arealista.component';
import { PortelistaComponent } from './porte/portelista.component';
import { TributacaolistaComponent } from './tributacao/tributacaolista.component';
import { GraduacaolistaComponent } from './graduacao/graduacaolista.component';

export const CadastroRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'area',
        component: ArealistaComponent
      },
      {
        path: 'porte',
        component: PortelistaComponent
      },
      {
        path: 'tributacao',
        component: TributacaolistaComponent
      },
      {
        path: 'graduacao',
        component: GraduacaolistaComponent
      }
    ]
  }
];
