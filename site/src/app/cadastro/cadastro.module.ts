import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

import { CadastroRoutes } from './cadastro.routing';

import { ArealistaComponent } from './area/arealista.component';
import { PortelistaComponent } from './porte/portelista.component';
import { TributacaolistaComponent } from './tributacao/tributacaolista.component';
import { GraduacaolistaComponent } from './graduacao/graduacaolista.component';

import { AreaService } from '../framework/service/area.service'
import { PorteService } from '../framework/service/porte.service'
import { TributacaoService } from '../framework/service/tributacao.service'
import { GraduacaoService } from '../framework/service/graduacao.service'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CadastroRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule
  ],
  declarations: [
    ArealistaComponent,
    PortelistaComponent,
    TributacaolistaComponent,
    GraduacaolistaComponent
  ],
  providers: [
    AreaService,
    PorteService,
    TributacaoService,
    GraduacaoService
  ]
})
export class CadastroModule {}
