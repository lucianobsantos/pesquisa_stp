import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient  } from '@angular/common/http'

import { ToastrService } from 'ngx-toastr'

import { LoginService } from '../../framework/service/login.service'
import { TabelaGridInlineComponent } from '../../framework/component/tabela-gridinline/tabela-gridinline.component'
import { TributacaoService } from '../../framework/service/tributacao.service'
import { Tributacao } from '../../framework/model/tributacao.model'


@Component({
  selector: 'app-tributacaolista',
  templateUrl: './tributacaolista.component.html',
  styleUrls: ['./tributacaolista.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TributacaolistaComponent extends TabelaGridInlineComponent<Tributacao>
  implements OnInit   {

  ngOnInit() {
    super.ngOnInit()
    this.tituloPagina = 'Lista de Tributação'
  }

  setNovo() {
    this.modelEdit =  {
      trib_nr_sequencia : -1,
      trib_tx_descricao : null,
      situ_tx_situacao : 'A',
      camposAuxiliares : {'tx_situacao':  'ATIVA'},
      usua_nr_cadastro : this.user.id_usuario,
      usua_nr_edicao   : this.user.id_usuario }

    this.setFormGroup(this.modelEdit)
    this.acao = 'INCLUIR'
  }

  onTratarModelEditPosGet() {
    this.modelEdit.usua_nr_edicao = this.user.id_usuario
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastr: ToastrService,
    public loginService: LoginService,
    public service: TributacaoService,
    private fb: FormBuilder
  ) {
    super(_router, http, toastr , loginService, service)
    this.filterForm = this.fb.group({
      trib_tx_descricao: this.fb.control('', [  ]),
      situ_tx_situacao: this.fb.control('A', [])
    })

    this.filterEdicao = this.fb.group({
      trib_nr_sequencia: this.fb.control('', [  ]),
      trib_tx_descricao: this.fb.control('', [Validators.required, Validators.maxLength(60) ]),
      situ_tx_situacao: this.fb.control('', []),
    })
  }

  getId( model: Tributacao ): number {
     return model.trib_nr_sequencia              
  }
}
