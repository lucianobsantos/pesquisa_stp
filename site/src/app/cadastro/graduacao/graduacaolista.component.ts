import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'

import { ToastrService } from 'ngx-toastr'

import { LoginService } from '../../framework/service/login.service'
import { TabelaGridInlineComponent } from '../../framework/component/tabela-gridinline/tabela-gridinline.component'
import { GraduacaoService } from '../../framework/service/graduacao.service'
import { Graduacao } from '../../framework/model/graduacao.model'


@Component({
  selector: 'app-graduacaolista',
  templateUrl: './graduacaolista.component.html',
  styleUrls: ['./graduacaolista.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GraduacaolistaComponent extends TabelaGridInlineComponent<Graduacao>
  implements OnInit   {

  ngOnInit() {
    super.ngOnInit()
    this.tituloPagina = 'Lista de Graduação'
  }

  setNovo() {
    this.modelEdit =  {
      grad_nr_sequencia : -1,
      grad_tx_descricao : null,
      situ_tx_situacao : 'A',
      camposAuxiliares : {'tx_situacao':  'ATIVA'},
      usua_nr_cadastro : this.user.id_usuario,
      usua_nr_edicao   : this.user.id_usuario }

    this.setFormGroup(this.modelEdit)
    this.acao = 'INCLUIR'
  }

  onTratarModelEditPosGet() {
    this.modelEdit.usua_nr_edicao = this.user.id_usuario
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastr: ToastrService,
    public loginService: LoginService,
    public service: GraduacaoService,
    private fb: FormBuilder
  ) {
    super(_router, http, toastr , loginService, service)
    this.filterForm = this.fb.group({
      grad_tx_descricao: this.fb.control('', [  ]),
      situ_tx_situacao: this.fb.control('A', [])
    })

    this.filterEdicao = this.fb.group({
      grad_nr_sequencia: this.fb.control('', [  ]),
      grad_tx_descricao: this.fb.control('', [Validators.required, Validators.maxLength(100) ]),
      situ_tx_situacao: this.fb.control('', []),
    })
  }

  getId( model: Graduacao ): number {
     return model.grad_nr_sequencia              
  }
}
