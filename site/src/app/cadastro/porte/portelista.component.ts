import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { HttpClient  } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'

import { TabelaGridInlineComponent } from '../../framework/component/tabela-gridinline/tabela-gridinline.component'
import { PorteService } from '../../framework/service/porte.service'
import { Porte } from '../../framework/model/porte.model'
import { LoginService } from '../../framework/service/login.service'


@Component({
  selector: 'app-portelista',
  templateUrl: './portelista.component.html',
  styleUrls: ['./portelista.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PortelistaComponent extends TabelaGridInlineComponent<Porte>
  implements OnInit   {

  ngOnInit() {
    super.ngOnInit()
    this.tituloPagina = 'Lista - Porte da empresa'
    
  }

  setNovo() {
    this.modelEdit =  {
      port_nr_sequencia : -1,
      port_tx_descricao : null,
      situ_tx_situacao : 'A',
      camposAuxiliares : {'tx_situacao':  'ATIVA'},
      usua_nr_cadastro : this.user.id_usuario,
      usua_nr_edicao   : this.user.id_usuario }

    this.setFormGroup(this.modelEdit)
    this.acao = 'INCLUIR'
  }

  onTratarModelEditPosGet() {
    this.modelEdit.usua_nr_edicao = this.user.id_usuario
  }

  constructor(
    public _router: Router,
    public http: HttpClient,
    public toastr: ToastrService,
    public loginService: LoginService,
    public service: PorteService,
    private fb: FormBuilder
  ) {
    super(_router, http, toastr , loginService, service)
    this.filterForm = this.fb.group({
      port_tx_descricao: this.fb.control('', [  ]),
      situ_tx_situacao: this.fb.control('A', [])
    })

    this.filterEdicao = this.fb.group({
      port_nr_sequencia: this.fb.control('', [  ]),
      port_tx_descricao: this.fb.control('', [Validators.required, Validators.maxLength(100) ]),
      situ_tx_situacao: this.fb.control('', []),
    })
  }

  getId( model: Porte ): number {
     return model.port_nr_sequencia
  }
}
