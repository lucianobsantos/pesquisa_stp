
use pesquisa_stp

go

drop table [questionario_pergunta]
drop table [pesquisa_resposta]
drop table [resposta]
drop table [pergunta]
drop table [questionario]

go
-- tabela: pergunta

CREATE TABLE pergunta (
	[perg_nr_sequencia] [int] IDENTITY(1,1) NOT NULL,
	[perg_tx_descricao] [varchar](1000) NULL,
	[perg_tx_tipo] [varchar](1) NULL,
	[perg_tx_situacao] [varchar](1) NOT NULL,
	[usua_nr_cadastro] [int] NOT NULL,
	[usua_nr_edicao] [int] NULL,
	[data_dt_cadastro] [datetime] NOT NULL,
	[data_dt_edicao] [datetime] NULL
PRIMARY KEY CLUSTERED 
(
	[perg_nr_sequencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [pergunta] ADD  DEFAULT ('A') FOR [perg_tx_situacao]
GO

ALTER TABLE [pergunta] ADD  DEFAULT (getdate()) FOR [data_dt_cadastro]
GO

ALTER TABLE [pergunta] ADD  DEFAULT (getdate()) FOR [data_dt_edicao]


-- ################################################################################

-- tabela: resposta

CREATE TABLE [resposta] (
	[resp_nr_sequencia] [int] IDENTITY(1,1) NOT NULL,
	[perg_nr_sequencia] [int] NOT NULL,
	[resp_tx_descricao] [varchar](2000) NOT NULL,
	[resp_nr_ordem] [int] NOT NULL,
	[resp_tx_situacao] [varchar](1) NOT NULL,
	[usua_nr_cadastro] [int] NOT NULL,
	[usua_nr_edicao] [int] NULL,
	[data_dt_cadastro] [datetime] NOT NULL,
	[data_dt_edicao] [datetime] NULL
PRIMARY KEY CLUSTERED 
(
	[resp_nr_sequencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [resposta] ADD  DEFAULT ('A') FOR [resp_tx_situacao]
GO

ALTER TABLE [resposta] ADD  DEFAULT (getdate()) FOR [data_dt_cadastro]
GO

ALTER TABLE [resposta] ADD  DEFAULT (getdate()) FOR [data_dt_edicao]
GO

ALTER TABLE [resposta]  WITH CHECK ADD FOREIGN KEY([perg_nr_sequencia])
REFERENCES [pergunta] ([perg_nr_sequencia])
GO




-- ################################################################################

-- tabela: questionario



CREATE TABLE [questionario](
	[ques_nr_sequencia] [int] IDENTITY(1,1) NOT NULL,
	[ques_tx_descricao] [varchar](100) NOT NULL,
	[ques_tx_cnpj_empresa] [varchar](20) NULL,
	[ques_tx_logo_empresa] [varchar](100) NULL,
	[ques_tx_texto_inicio] [varchar](8000) NULL,
	[ques_tx_texto_fim] [varchar](8000) NULL,
	[ques_tx_situacao] [varchar](1) NOT NULL,
	[usua_nr_cadastro] [int] NOT NULL,
	[usua_nr_edicao] [int] NULL,
	[data_dt_cadastro] [datetime] NOT NULL,
	[data_dt_edicao] [datetime] NULL
PRIMARY KEY CLUSTERED 
(
	[ques_nr_sequencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [questionario] ADD  DEFAULT ('A') FOR [ques_tx_situacao]
GO

ALTER TABLE [questionario] ADD  DEFAULT (getdate()) FOR [data_dt_cadastro]
GO

ALTER TABLE [questionario] ADD  DEFAULT (getdate()) FOR [data_dt_edicao]





-- ################################################################################

-- tabela: questionario


CREATE TABLE [questionario_pergunta](
	[quep_nr_sequencia] [int] IDENTITY(1,1) NOT NULL,
	[ques_nr_sequencia] [int] NOT NULL,
	[perg_nr_sequencia] [int] NOT NULL,
	[quep_nr_ordem] [int] NOT NULL,
	[quep_tx_situacao] [varchar](1) NOT NULL,
	[usua_nr_cadastro] [int] NOT NULL,
	[usua_nr_edicao] [int] NULL,
	[data_dt_cadastro] [datetime] NOT NULL,
	[data_dt_edicao] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[quep_nr_sequencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [questionario_pergunta] ADD  DEFAULT ('A') FOR [quep_tx_situacao]
GO

ALTER TABLE [questionario_pergunta] ADD  DEFAULT (getdate()) FOR [data_dt_cadastro]
GO

ALTER TABLE [questionario_pergunta] ADD  DEFAULT (getdate()) FOR [data_dt_edicao]
GO

ALTER TABLE [questionario_pergunta]  WITH CHECK ADD FOREIGN KEY([perg_nr_sequencia])
REFERENCES [pergunta] ([perg_nr_sequencia])
GO

ALTER TABLE [questionario_pergunta]  WITH CHECK ADD FOREIGN KEY([ques_nr_sequencia])
REFERENCES [questionario] ([ques_nr_sequencia])
GO




-- ################################################################################

-- tabela: questionario


CREATE TABLE [pesquisa_resposta](
	[pere_nr_sequencia] [int] IDENTITY(1,1) NOT NULL,
	[ques_nr_sequencia] [int] NOT NULL,
	[resp_nr_sequencia] [int] NOT NULL,
	[pere_dt_resposta] [datetime] NULL,
	[pere_tx_situacao] [varchar](1) NOT NULL,
	[usua_nr_cadastro] [int] NOT NULL,
	[usua_nr_edicao] [int] NULL,
	[data_dt_cadastro] [datetime] NOT NULL,
	[data_dt_edicao] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[pere_nr_sequencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [pesquisa_resposta] ADD  DEFAULT (getdate()) FOR [pere_dt_resposta]
GO

ALTER TABLE [pesquisa_resposta] ADD  DEFAULT ('A') FOR [pere_tx_situacao]
GO

ALTER TABLE [pesquisa_resposta] ADD  DEFAULT (getdate()) FOR [data_dt_cadastro]
GO

ALTER TABLE [pesquisa_resposta] ADD  DEFAULT (getdate()) FOR [data_dt_edicao]
GO

ALTER TABLE [pesquisa_resposta] WITH CHECK ADD FOREIGN KEY([ques_nr_sequencia])
REFERENCES [questionario] ([ques_nr_sequencia])
GO

ALTER TABLE [pesquisa_resposta] WITH CHECK ADD FOREIGN KEY([resp_nr_sequencia])
REFERENCES [resposta] ([resp_nr_sequencia])

